#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import torch
import torch.nn.functional as F
from torch import nn,Tensor,LongTensor
from torch.autograd import Variable
from torch.autograd import Function

class LossUnet(nn.Module):
    
    def __init__(self,w0,sigma):
        self.w0 = w0
        self.sigma = sigma
        self.loss = nn.CrossEntropyLoss(size_average = False,reduction='none')
        
    def distance_to_nearests(self,pixels,image,n):
        """retrieves connected components of the image, and compute the distance to the
        border of the n nearest component for each pixel in pixels"""
        list_info = dict()
        distance_to_n_connected = dict()
        num_connected,connected_components = cv2.connectedComponents(image)
        
        # for each pixel, computes the distance of the pixel to each pixel within each 
        # connected component
        for pixel in pixels:
            list_info_pixel = list()
            for (i,j),element in np.ndenumerate(connected_components):
                if element>0:
                    distance_to_pixel = np.sqrt((i-pixel[0])**2+(j-pixel[1])**2)
                    list_info_pixel.append([i,j,element,distance_to_pixel])
            #list_info_pixel = np.array(list_info_pixel)
            
            # retrieves the closest connected component
            distance_to_connected = list()
            for connected in range(1,num_connected):
                list_connected = list(filter(lambda x: x[2] == connected, list_info_pixel))
                distance_to_connected.append(np.min(np.array(list_connected)[:,3]))
            distance_to_n_connected[pixel] = np.sort(distance_to_connected)[:n]
        
        return distance_to_n_connected
                
    
    def weight_map(self,image,n=2):
        """computes the weight map of a masked image"""
    
        pixels = [(i,j) for (i,j),_ in np.ndenumerate(image)]
        distance_to_n_connected = self.distance_to_nearests(pixels,image,n)
        
        weight_map = np.zeros(image.shape)
        for (i,j) in pixels :
            weight_map[i,j] = sum(distance_to_n_connected[(i,j)])
        weight_map = self.w0*np.exp(-weight_map**2/(2*self.sigma**2))
        
        return weight_map
        
    def forward(self,y_pred,y_true):
        """computes the loss associated to UNet"""
        weight_map = torch.flatten(torch.from_numpy(self.weight_map(y_true.numpy())))
        y_pred_flat = torch.flatten(y_pred)
        y_true_flat = torch.flatten(y_true)
        entropy_loss = self.loss(y_pred_flat,y_true_flat)
        return torch.sum(entropy_loss*weight_map,0)
      
        size,stride,level,type_object,mode = 'train'
        
        
class DiceLoss(nn.Module):
    """Dice coeff for individual examples"""
    
    def __init__(self, gamma=0.0001, weights=None, size_average=True):
        super(DiceLoss, self).__init__()
        
        self.gamma = gamma
        
        #weights of the focal loss
        if isinstance(weights,(float,int)): self.weights = torch.Tensor([weights,1-weights])
        if isinstance(weights,list): self.weights = torch.Tensor(weights)
        if weights is None : self.weights = weights

        self.size_average = size_average

    def forward(self, input, target):

        #reshaping input and target
        if input.dim() > 2:
            input = input.view(input.size(0),input.size(1), -1)  # N,C,H,W => N,C,H*W
            input = input.transpose(1, 2)    # N,C,H*W => N,H*W,C
            target = target.view(input.size(0), -1)
            #input = input.contiguous().view(-1,input.size(2))   # N,H*W,C => N*H*W,C
        #target = target.view(-1,1)
        
        #converting the target into one-hot vector
        target_one_hot = torch.zeros(input.size(0), input.size(1), input.size(2))
        batches = torch.arange(target.size(0))
        positions = torch.arange(target.size(1))
        batches, positions = torch.meshgrid(batches, positions)
        target_one_hot[batches, positions, target] = 1
        target_one_hot = target_one_hot.cuda()
        
        #computes probabilities of input
        pt = F.softmax(input)
        
        # computes the dice coefficients of each class computed
        self.inter = (pt * target_one_hot).sum(1)
        self.union = (pt + target_one_hot).sum(1)
        
        #computs the loss
        loss = (self.inter + self.gamma) / (self.union + self.gamma)
        
        #makes the weights
        if self.weights is not None:
            
            #checks cuda !
            if self.weights.type()!=input.data.type():
                self.weights = self.weights.type_as(input.data)
            
            loss_weights = (self.weights * loss).sum(1)
            
        #averaging the loss along classes
        if self.size_average: return loss.mean()
        else: return loss.sum()


class FocalLoss(nn.Module):
    """This class is used to compute the Focal Loss
    
    Attributes :
    
        gamma : float, the ponderation of hard elements
        weights : list of floats, the ponderation of each class in the computation of the loss
        size_average : bool, performs the average on a batch if True, else the Sum
        crop_size : int, if only the center part of the image has to be taken into account in the computation of the loss. Choose the size.
        skip_class : list of int, if some classes are to be skipped in the computation of the loss, input the class which are to be skipped
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
            __repr__ : displays information about the class
        
        Special :
            forward : used to compute the forward pass
    
    """
    
    def __init__(self, gamma=0, weights=None, size_average=True, crop_size=None, skip_class = None):
        
        """initialises the class
        
        Inputs : 
            gamma : float, the decay factor of the loss. Default : 0.
            weights : float or list of floats, the importance of each class in the computation of the loss. If None give uniform weightening. Default : None.
            size_average : bool, if True the loss is averaged over the batch, else summed. Default : True.
            crop_size : int, if only the center part of the image has to be taken into account in the computation of the loss. Choose the size.
            skip_class : list of int, if some classes are to be skipped in the computation of the loss, input the class which are to be skipped
        
        """
        
        super(FocalLoss, self).__init__()
        
        self.gamma = gamma
        
        #weights of the focal loss
        if isinstance(weights,(float,int)): self.weights = torch.Tensor([weights,1-weights])
        if isinstance(weights,list): self.weights = torch.Tensor(weights)
        else : self.weights = weights
        
        self.size_average = size_average
        
        self.crop_size = crop_size
        
        if isinstance(skip_class,(float,int)): self.skip_class = [skip_class]
        elif isinstance(skip_class,list): self.skip_class = skip_class
        elif skip_class is None : self.skip_class = skip_class
           
        #modifies weights if some classes are to be skept
        if skip_class is not None :
            if self.weights is not None :
                self.weights[np.array(skip_class).astype(bool)] = 0.
                self.weights = self.weights / self.weights.sum()
            else :
                self.weights = 1 - torch.Tensor(self.skip_class)
                self.weights = self.weights / self.weights.sum()
            
    def forward(self, input, target):
        
        """Forward pass through the loss
        
        Inputs :
            input : the predicted vector to be compared to the target
            target : the reference to compute the loss
        """
        
        #reshaping input and target
        if input.dim()>2:
            
            #performs cropping
            if self.crop_size is not None :
                input = input[:,:, (input.size(2)-self.crop_size)//2:(input.size(2)-self.crop_size)//2+self.crop_size, (input.size(3)-self.crop_size)//2:(input.size(3)-self.crop_size)//2+self.crop_size]
            input = input.reshape(input.size(0),input.size(1),-1)  # N,C,H,W => N,C,H*W
            input = input.transpose(1,2)    # N,C,H*W => N,H*W,C
            input = input.contiguous().view(-1,input.size(2))   # N,H*W,C => N*H*W,C
        
        #performs cropping
        if self.crop_size is not None :
            target = target[:, (target.size(1)-self.crop_size)//2:(target.size(1)-self.crop_size)//2+self.crop_size, (target.size(2)-self.crop_size)//2:(target.size(2)-self.crop_size)//2+self.crop_size]
        target = target.reshape(-1, 1)
        

        #computes the output log-probabilities
        logpt = F.log_softmax(input)

        #makes the product between labels and outputs
        logpt = logpt.gather(1,target)
        
        logpt = logpt.view(-1)
        pt = Variable(logpt.data.exp())

        #makes the weights
        if self.weights is not None:
            
            #checks cuda !
            if self.weights.type()!=input.data.type():
                self.weights = self.weights.type_as(input.data)
                
            #assigns each weight to the corresponding value
            at = self.weights.gather(0,target.data.view(-1))
            logpt = logpt * Variable(at)

        loss = -1 * (1-pt)**self.gamma * logpt
        
        #averaging the loss along classes
        if self.size_average: return loss.mean()
        else: return loss.sum()
        
    def __repr__(self):
        """Displays information about the class"""
        return 'focal'
     