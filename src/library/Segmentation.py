#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 14:00:21 2020

@author: valentinabadie
"""

import matplotlib.pyplot as plt
import cv2
import numpy as np
import torch
import torch.nn as nn
from shapely.geometry import Point,Polygon
from shapely.ops import cascaded_union as union
from tqdm import tqdm
from scipy.ndimage import gaussian_filter
from library.SlidesHandling import DataSetSlides
from library.Morphology import MorphologyStudy


class SegmentationImplementation():
    
    """This class is used to perform segmentation of each sample of the input dataset, and then to merge them by using the coordinates of thre processed regions to make them match
    
    Attributes :
    
        dataset : DataSetGenerator object or instance of it, input dataset for the model to be tested on it
        region_indices : list of int, indices of the region to select among the dataset (the are pre-selected by the classifier
        region_coords : list of 4-tuple, coordinates of the regions passed as input to the model
        model : nn.Module object, model to be tested
        labels : list of list of 2D tuples coordinates, corresponding to the contours of the ground-truth labels
        threshold : float, the decision cutoff to be applied to the probability predictions
        ROI : int, the ROI on which to evaluate segmentation
        size : int, size of the images in the dataset
        
        segmentations : list, list of all segmented objects (in meters)
        segmentations_union : geom object, union of all the segmented objects
        
        tp : int, number of true positive detected
        fp : int, number of false positive detected
        fn :int, number of false negative detected
        
        f1_score : float, f1 score performed by the model on the dataset
        precision : float, precision performed by the model on the dataset
        recall : float, recall performed by the model on the dataset
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            convert_to_meter : uses the boundaries of the region to compute the real-world segmentation positions
            segmentation : performs segmentation of objects of interest on every sample region, and merge them
            evaluation : computes scores on the performed segmentation
            display : displaying an example of result of segmentation
            display_several : displaying some examples of result of segmentation
            postprocessing : applying postprocessing based on the surface of the detected objects
    
    """
    
    def __init__(self, region_indices, region_coords, dataset, model, labels = None, threshold = 0.5, ROI = 2):
        
        """initialises the class
        
        Inputs :
            dataset : DataSetGenerator object or instance of it, input dataset for the model to be tested on it
            region_indices : list of int, indices of the region to select among the dataset (the are pre-selected by the classifier
            region_coords : list of 4-tuple, coordinates of the regions passed as input to the model
            model : nn.Module object, model to be tested
            labels : list of list of 2D tuples coordinates, corresponding to the contours of the ground-truth labels
            threshold : float, the decision cutoff to be applied to the probability predictions
            ROI : int, the ROI on which to evaluate segmentation

        """
        
        self.region_indices = region_indices
        self.region_coords = region_coords
        self.dataset = dataset
        self.model = model
        self.labels = labels
        self.threshold = threshold
        self.ROI = ROI
        
        self.size = max(dataset[0]['Images'].shape)
        
    def convert_to_meter(self, positions, coords):
        """uses the boundaries of the region to compute the real-world segmentation positions
        
        Inputs :
            positions : N-by-2 array, containing positions in terms of pixels
            coords : 4-tuple, real-world coordinates of the regions, given in meters
        """
        
        x = coords[0] + positions[:,0]/self.size *(coords[2] - coords[0])
        y = coords[1] + (self.size - positions[:,1])/self.size *(coords[3] - coords[1])
        
        return np.vstack((x,y))
    
    def segmentation(self):
        """performs the segmentation task on all the dataset"""
    
        list_predictions = list()
        for i,index in tqdm(enumerate(self.region_indices)): 

            sample = self.dataset[index]
            image = sample['Images'].type(torch.FloatTensor)
            
            #processes the sample to go through the neural network
            image = image.reshape([1] + list(image.shape)).permute(0,3,1,2)
            output = self.model(image).permute(0,2,3,1).detach().cpu()

            #computes the prediction be done on the sample
            prob_output = np.argmax(np.array(nn.Softmax(-1)(output)), -1)
            prediction = (prob_output>=self.threshold).astype(np.uint8).reshape(prob_output.shape[1:])

            if np.max(np.array(prediction))==0: #if nothing is retrieved on the current sample
                continue
            x = np.arange(max(image.shape))
            y = np.arange(max(image.shape))

            plt.ioff() #prevents matplotlib to display unuseful graphs
            contours = plt.contour(x,y,prediction, [0.5]).collections[0].get_paths() #retrieves the object contours on the image
            for contour in contours :
                if len(contour.vertices) >= 3: #some contours are simply lines, it is not relevant to use it
                    list_predictions.append(self.convert_to_meter(contour.vertices, self.region_coords[index])) #computes every contour to be found in the dataset, and make it match with real world measures

        #retrieves the contour of the image 
        self.segmentations_union = union([Polygon(list(zip(*coordinates))) for coordinates in list_predictions])
        self.segmentations = [np.array(list(geom.exterior.coords)) for geom in list(self.segmentations_union.geoms)]

    def evaluation(self):
        """performs evaluation of the dataset (precision, recall, f1-score)"""
        
        #computes union of all ground-truth objects
        labels = [coords for obj, coords in list(filter(lambda x : x[0] == self.ROI, self.labels))]
        labels_union = union([Polygon(list(zip(*coordinates.T))) for coordinates in labels])
        
        #computes intersection with this union
        pred_label_inter = self.segmentations_union.intersection(labels_union)

        #computes true/false pos/neg
        self.tp = sum([seg.intersects(labels_union) for seg in list(self.segmentations_union.geoms)]) 
        self.fp = len(self.segmentations_union) - self.tp
        self.fn = len(labels_union) - len(pred_label_inter)
        
        #computes scores
        self.precision = self.tp/(self.tp + self.fp)
        self.recall = self.tp/(self.tp + self.fn)
        self.f1_score = 2*self.precision*self.recall / (self.precision + self.recall)
        
    def display(self, index, slide = 'A1702114', magnification = 'x20'):
        
        """Displays a visual example of segmentation results
        
        
        Inputs :
            index : int, index of the segmented object to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        #retrieves the object to display
        polygon_to_disp = self.segmentations_union[index]
        
        #computes limits of the circumscribed rectangle of the object
        coords_region = list(polygon_to_disp.bounds)
        
        #extents borders of the regions for better visual results
        coords_region[0], coords_region[2] = (coords_region[0] + coords_region[2])/2 - 10, (coords_region[0] + coords_region[2])/2 + 10
        coords_region[1], coords_region[3] = (coords_region[1] + coords_region[3])/2 - 10, (coords_region[1] + coords_region[3])/2 + 10

        #retrieves the corresponding region
        region = DataSetSlides()[slide][magnification](*coords_region)
        
        fig, ax = plt.subplots(1, 2, figsize = (10,20))
        ax[0].imshow(region.array_region, extent = np.array(coords_region)[np.array([0,2,1,3])])
        ax[0].plot(*list(map(list, zip(*list(polygon_to_disp.exterior.coords)))))
        ax[1].imshow(region.array_region, extent = np.array(coords_region)[np.array([0,2,1,3])])
        for obj, coords in region.in_labels :
            if obj == self.ROI :
                ax[1].plot(coords[:,0], coords[:,1])
        plt.show()
        
    def display_several(self, indices, slide = 'A1702114', magnification = 'x20'):
        
        """Displays several example of results
        
        Inputs :
            indices : list of int, indices of the segmented objects to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        for index in indices :
            self.display(index, slide = slide, magnification = magnification)
            
    def postprocessing(self, threshold = 0.1, slide = 'A1702114', magnification = 'x20'):
        
        """Displays several example of results
        
        Inputs :
            indices : list of int, indices of the segmented objects to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        #computes the surface of each retrieved object
        morphology = MorphologyStudy(self.segmentations, DataSetSlides()[slide][magnification])
        morphology.surface()
        
        #performs another segmentation
        self.segmentations_union = union([seg for i,seg in enumerate(list(self.segmentations_union.geoms)) if morphology.metrics['Surface'][i]>= max(morphology.metrics['Surface'])*threshold])
        self.segmentations = [np.array(list(geom.exterior.coords)) for geom in list(self.segmentations_union.geoms)]
        
    
class ImageProcessing():
    
    def __init__(self,image):
        
        self.image_BGR = image
        self.image_HSV = cv2.cvtColor(image[:,:,:3],cv2.COLOR_BGR2HSV)
        self.image_grayscale = np.mean(image[:,:,:3], axis = 2)
        
        #self.surface_connected_components()
        
    def find_color_objects(self, sigma=4):
        
        #computes the hue histogram of the image
        histogram_H, values_H = np.histogram(self.image_HSV[:,:,0].flatten(), bins = np.arange(181))
        values_H = values_H[:-1]
        
        #smoothes the histogram in order to perform local maxima detection
        smooth_histogram_H = gaussian_filter(histogram_H.astype(float), sigma, order = 0, mode = 'wrap')
        
        #plt.plot(smooth_histogram_H)
        #plt.figure()
        
        #retrieves the local maxima of the histogram to extract important colors
        histo_M = np.r_[False, smooth_histogram_H[1:]>=smooth_histogram_H[:-1]]*np.r_[smooth_histogram_H[1:]<=smooth_histogram_H[:-1],False]
        values_H_max, histogram_H_max = values_H[histo_M], smooth_histogram_H[histo_M]
        
        #retrieves the 2 maximums, corresponding to the start of blue and brown color respectively
        vals_max = sorted(sorted(zip(values_H_max,histogram_H_max), key = lambda x: x[1])[-2:], key = lambda x:x[0])
        #print(vals_max)
        
        #retrieves the local minima of the histogram to extract important colors
        histo_m = np.r_[False, smooth_histogram_H[1:]<=smooth_histogram_H[:-1]]*np.r_[smooth_histogram_H[1:]>=smooth_histogram_H[:-1],False]
        
        #retrieve minima corresponding respectively to brown (br) and blue (bl) up boundary
        histo_m_bl = histo_m*(values_H <=vals_max[1][0])*(values_H >= vals_max[0][0])
        histo_m_br = histo_m*((values_H >=vals_max[1][0])+(values_H <=vals_max[0][0]))
        values_H_min_bl, histogram_H_min_bl = values_H[histo_m_bl], smooth_histogram_H[histo_m_bl]
        values_H_min_br, histogram_H_min_br = values_H[histo_m_br], smooth_histogram_H[histo_m_br]
        
        #sorts the retrieved values
        vals_min_bl = sorted(zip(values_H_min_bl,histogram_H_min_bl), key = lambda x: -x[1])[-1]
        vals_min_br = sorted(zip(values_H_min_br,histogram_H_min_br), key = lambda x: -x[1])[-1]

        #retrieves the bounds of brown objects
        bounds_brown = (vals_max[1][0], vals_min_br[0])
        bounds_blue = (vals_max[0][0], vals_min_bl[0])
        
        return bounds_brown, bounds_blue
        
    def find_grayscale_threshold_neurites(self, n_scales = 100):
        
        #retrieves the pixels of the right color (brown)
        bounds_brown, _ = self.find_color_objects(sigma = 4)
        selected_color_image = (self.image_HSV[:,:,0] >= bounds_brown[0])*(self.image_HSV[:,:,0] <= bounds_brown[1]).astype(np.uint8)
        #plt.imshow(selected_color_image, cmap = 'gray')
        
        #adjusts the grayscale threshold to find the one which generates the most connected components
        grayscale_list = np.linspace(np.min(self.image_grayscale), np.max(self.image_grayscale), n_scales)
        self.nb_connected_components = list()
        for grayscale in grayscale_list:
            #print(grayscale)
            #performs a dilation to merge close objects
            grayscale_thresholded_image = cv2.dilate((self.image_grayscale<=grayscale).astype(np.uint8)*selected_color_image, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)), iterations = 1)
            #plt.figure()
            #plt.imshow(grayscale_thresholded_image)
            
            #retrieves the number of objects
            self.nb_connected_components.append(cv2.connectedComponents(grayscale_thresholded_image)[0])
            
        smooth_nb_connected_components = gaussian_filter(self.nb_connected_components, 3, order = 0)
        #plt.figure()
        #plt.plot(grayscale_list, smooth_nb_connected_components)
        #the optimal gray scale is computed
        grayscale_opt = grayscale_list[np.argmax(smooth_nb_connected_components)]
        
        return grayscale_opt, (self.image_grayscale<=grayscale_opt).astype(np.uint8)*selected_color_image#cv2.dilate((self.image_grayscale<=grayscale_opt).astype(np.uint8)*selected_color_image, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)), iterations = 1)
            
    def find_grayscale_threshold_tangles(self, n_scales = 100):   
        #retrieves the pixels of the right color (brown)
        bounds_brown, _ = self.find_color_objects(sigma = 4)
        grayscale_neurites, _ = self.find_grayscale_threshold_neurites(n_scales = 100)
        
        selected_color_image = (self.image_HSV[:,:,0] >= bounds_brown[0])*(self.image_HSV[:,:,0] <= bounds_brown[1]).astype(np.uint8)
        smooth_image_grayscale = gaussian_filter(self.image_grayscale, 3, order = 0)
        
        #positions on the image, used in the loop
        X = np.arange(self.image_grayscale.shape[0])
        Y = np.arange(self.image_grayscale.shape[1])
        [X,Y] = np.meshgrid(X,Y)
        
        #adjusts the grayscale threshold to find the one which generates the connected components with a great surface
        grayscale_list = np.linspace(np.min(smooth_image_grayscale), grayscale_neurites, n_scales)
        nb_connected_components = list()
        surf_connected_components = list()
        for grayscale in grayscale_list:
            #performs a dilation to merge close objects
            grayscale_thresholded_image = cv2.morphologyEx((smooth_image_grayscale<=grayscale).astype(np.uint8)*selected_color_image, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)))
            
            #retrieves the number of objects
            n_connected, connecteds = cv2.connectedComponents(grayscale_thresholded_image)
            nb_connected_components.append(n_connected)
            
            #computes the mean surface of the connected components of the image
            splitted_connected_components = np.zeros(self.image_HSV.shape[:2] + tuple([nb_connected_components[-1]]))
            splitted_connected_components[X, Y, connecteds] = 1
            
            surf_connected_components.append(np.mean(np.sum(np.sum(splitted_connected_components[:,:,1:], 0), 0)))
        
        surf_connected_components = gaussian_filter(self.nb_connected_components, 3, order = 0)
        #the grayscale generating the most connected components is computed
        #max_connected = np.argmax(nb_connected_components)
        
        #the optimal grayscale is the one maxmizing the mean surface while being under the grayscale generating the most connected components
        grayscale_list, surf_connected_components = list(zip(*filter(lambda x:not np.isnan(x[1]), zip(grayscale_list, surf_connected_components))))
        grayscale_list = list(grayscale_list)
        surf_connected_components = list(surf_connected_components)
        
        #i = np.argmax(surf_connected_components)
        #grayscale_opt = grayscale_list[i]
        #del grayscale_list[i]
        #del surf_connected_components[i]
        
        i = np.argmax(surf_connected_components)
        grayscale_opt = grayscale_list[i]
        
        #plt.plot(grayscale_list, surf_connected_components)
        #print(grayscale_opt)
        
        im_th = cv2.morphologyEx((smooth_image_grayscale<=grayscale_opt).astype(np.uint8)*selected_color_image, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)))
        
        # Copy the thresholded image.
        im_floodfill = im_th.copy()

        # Notice the size needs to be 2 pixels than the image.
        h, w = im_th.shape[:2]
        mask = np.zeros((h+2, w+2), np.uint8)

        # Foodfill from point (0, 0)
        cv2.floodFill(im_floodfill, mask, (0,0), 255);

        # Invert floodfilled image
        im_floodfill_inv = cv2.bitwise_not(im_floodfill)

        # Combine the two images to get the foreground.
        im_out = im_th | im_floodfill_inv
        
        contours, hierarchy = cv2.findContours(im_out, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        # create hull array for convex hull points
        hull = []

        # calculate points for each contour
        for i in range(len(contours)):
            # creating convex hull object for each contour
            hull.append(cv2.convexHull(contours[i], False))
            
        # create an empty black image
        #drawing = np.zeros((im_out.shape[0], im_out.shape[1], 3), np.uint8)
        self.image_BGR = self.image_BGR.astype(np.uint8)
        
        # draw contours and hull points
        for i in range(len(contours)):
            color_contours = (0, 255, 0) # green - color for contours
            color = (255, 0, 0) # blue - color for convex hull
            # draw ith contour
            #cv2.drawContours(self.image_BGR, contours, i, color_contours, 1, 8, hierarchy)
            # draw ith convex hull object
            cv2.drawContours(self.image_BGR, hull, i, color, 1, 8)
        
        return surf_connected_components, im_out, contours

    def surface_connected_components(self,nb_th = 10, kernel_size = 5):
        
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(kernel_size,kernel_size))
        region_h = cv2.morphologyEx(self.imageHSV[:,:,0], cv2.MORPH_OPEN, kernel)

        self.thresholds = np.linspace(np.min(region_h), np.max(region_h), nb_th)
        
        self.nb_connecteds = list()
        self.thresholded_images_HSV = list()
        self.surface_connected_components = list()
        for t in thresholds: 
            image_th = cv2.threshold(region_h,t,np.max(region_h),cv2.THRESH_BINARY)[1]
            
            self.thresholded_images_HSV.append(image_th)
            
            num_connected,connected = cv2.connectedComponents(thresh.astype(np.uint8))
            
            unique, surface = np.unique(connected, return_counts=True)
            if len(counts)>1:
                self.surface_connected_components.append(np.mean(surface[1:]))
            else :
                self.surface_connected_components.append(0)
            
            self.nb_connecteds.append(num_connected)
    
    def contour_manual(self,img,contour,color=0):
        contour= contour/np.max(contour)
        img = np.array(img)
        for i in range(img.shape[2]):
            if i==color :
                img[:,:,i] = img[:,:,i]*(1-contour) + 255*contour
            elif i ==3:
                pass
            else :
                img[:,:,i] = img[:,:,i]*(1-contour)
        return img
            
    def closing(self,slide,level,tab,x1,x2,y1,y2,size,it=1):
        closing = self.to_hsv_region(slide,level,tab,x1,x2,y1,y2)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size,size))
        for _ in range(it):
            closing = cv2.morphologyEx(closing, cv2.MORPH_CLOSE, kernel)
        
        return closing
    
    def opening(self,slide,level,tab,x1,x2,y1,y2,size):
        region_hsv = self.to_hsv_region(slide,level,tab,x1,x2,y1,y2)
        kernel = cv2.getStructuringElement(cv2.MORPH_RECT,(size,size))
        opening = cv2.morphologyEx(region_hsv, cv2.MORPH_OPEN, kernel)
        
        return opening
    
    def get_in_region_histograms(self,slide,level,tab,x1,x2,y1,y2,object_kind,scale,separation=0.8):
        """makes an histogram for each object of interest in the region"""
        _,region,points_containing = super().get_in_region_pixels(slide,level,tab,x1,x2,y1,y2,object_kind)
        print('OK')
        hist_list = list()
        hues_list =[]
        if object_kind == 'none':
            hue_values = list()
            for x in range(region.shape[0]):
                for y in range(region.shape[1]):
                    pixel = region[x,y]
                    h = self.rgb_to_hsv(pixel[0],pixel[1],pixel[2])[0]
                    hue_values.append(h)
            hues_list.append((hue_values,'none'))
            scaled_hue = list((scale*np.array(hue_values)).astype(int))
            hist_list.append([scaled_hue.count(i) for i in range(scale+1)])
            return scaled_hue,hist_list
                    
        for i,val in tqdm(enumerate(points_containing)):
            hue_object = []
            object_type = val[0]
            coordinates = val[1]
            
            if object_type == object_kind:
                for coor in coordinates :
                    pixel = region[coor[1],coor[0]]
                    h = rgb_to_hsv(pixel[0],pixel[1],pixel[2])[0]
                    if h>separation :
                        h = h-separation
                    else :
                        h = h+1-separation
                    hue_object.append(h)
                hues_list.append((hue_object,object_type))
                
                scaled_hue = list((scale*np.array(hue_object)).astype(int))
                hist_list.append([scaled_hue.count(i) for i in range(scale+1)])
            
        return hues_list,hist_list
    
    def plot_region_h_contour(self,slide,level,tab,x1,x2,y1,y2,object_kind,separation=0.8,n_points = 10):
        tabs_to_plot,region,points_containing = super().get_in_region_pixels(slide,level,tab,x1,x2,y1,y2,object_kind)
        size = region.shape
        h_region = np.zeros((size[0],size[1]))
        
        for x in range(region.shape[0]):
            for y in range(region.shape[1]):
                pixel = region[x,y]
                h = rgb_to_hsv(pixel[0],pixel[1],pixel[2])[0]
                if h>separation :
                    h = h-separation
                else :
                    h = h+1-separation
                h_region[x,y] = h
                
        fig,ax = plt.subplots(1,2)
        ax[0].imshow(h_region,vmin=0,vmax=1)
        ax[1].imshow(region,vmin=0,vmax=255)
        
        size = region.shape
        X = np.linspace(0,size[0],size[0])
        Y = np.linspace(0,size[1],size[1])
        [X,Y] = np.meshgrid(Y,X)
        ax[1].contour(X,Y,h_region,[0.3,0.5],colors=['red','blue'])
    
    def plot_average_histogram(self,slide,level,tab,x1,x2,y1,y2,object_type,scale):
        """plots the average histogram of a given class of objects in the region"""
        _,hist_list = self.get_in_region_histograms(slide,level,tab,x1,x2,y1,y2,object_type,scale)
        histograms_concerned = np.array(hist_list)
        average_hist = np.mean(histograms_concerned,axis=0)
        positions = np.linspace(0,100,average_hist.shape[0])
        colors = ['blue','green','red','blue']
        plt.bar(positions,average_hist,color = 'red')
        
        
    def plot_region_h(self,slide,level,tab,x1,x2,y1,y2,object_kind,separation=0.8):
        tabs_to_plot,region,points_containing = super().get_in_region_pixels(slide,level,tab,x1,x2,y1,y2,object_kind)
        size = region.shape
        h_region = np.zeros((size[0],size[1]))
        
        for x in range(region.shape[0]):
            for y in range(region.shape[1]):
                pixel = region[x,y]
                h = rgb_to_hsv(pixel[0],pixel[1],pixel[2])[0]
                if h>separation :
                    h = h-separation
                else :
                    h = h+1-separation
                h_region[p[1],p[0]] = np.array(h)
                
        fig,ax = plt.subplots(1,2)
        ax[0].imshow(h_region,vmin=0,vmax=1)
        ax[1].imshow(region,vmin=0,vmax=255)
        
        for i,val in enumerate(tabs_to_plot):
            object_type = val[0]
            coordinates = val[1]
            ax[0].plot(coordinates[:,0],coordinates[:,1],color =colors[object_type])
            ax[1].plot(coordinates[:,0],coordinates[:,1],color =colors[object_type])