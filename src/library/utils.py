#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:06:22 2020

@author: valentinabadie
"""

import pickle
import numpy as np

class SaveFiles(object):
    
    """Class used to manage file storage
    
    Methods :
        
        Class :
            save : saves an object in a file
            load : loads an object from a file
    
    """
    
    @classmethod
    def save(self, path, obj):
        
        """saves an object in a file
        
        Inputs :
            path : str, path to file where to store the object
            obj : python object
        """
        
        with open(path,'wb') as file :
            pickle.Pickler(file).dump(obj)
    
    @classmethod
    def load(self, path):
        
        """loads an object from a file
        
        Inputs :
            path : str, path to file where to load the object
        """
        
        with open(path,'rb') as file :
            obj = pickle.Unpickler(file).load()
        return obj
    
class Displayer:
    
    @classmethod
    def print_info(self, to_display):
        print('\r', end ='', flush = True)
        print(' '*len(to_display), end='',flush=True)
        print('\r', end ='', flush = True)
        print(to_display, end = '', flush = True)

class PrintFiles(object):

    @classmethod
    def print(self,path,*args,opt='a'):
        with open(path,opt) as f:
            print(*args, file=f)
    
class ColorTransforms(object):
    
    
    rbgMxyz = np.array([[0.5141, 0.3239, 0.1604],[0.2651, 0.6702, 0.0641],[0.0241, 0.1228, 0.8444]]).T

    xyzMLMS = np.array([[0.3897, 0.6890, 1.1834], [-0.2298, 1.1834, 0.0464], [0., 0., 1.0000]]).T
        
    LMSMrgb = np.linalg.inv(np.dot(rbgMxyz,xyzMLMS))
        
    logLMSMlab = np.dot(np.diag([1/np.sqrt(3),1/np.sqrt(6),1/np.sqrt(2)]),np.array([[1,1,1],[1,1,-2],[1,-1,0]])).T
    labMlogLMS = np.linalg.inv(logLMSMlab)
    
    @classmethod
    def rgb2LMS(self, _array):
        array = np.copy(_array).astype(float)
        array[:,:,:3] = np.dot(np.dot(array[:,:,:3],self.rbgMxyz),self.xyzMLMS)
        return array
        
    @classmethod
    def rgb2logLMS(self, _array):
        array = np.copy(_array)
        array = self.rgb2LMS(array)
        array[:,:,:3] = np.log10(array[:,:,:3])
        return array
    
    @classmethod
    def rgb2lab(self, _array):
        array = np.copy(_array)
        array = self.rgb2logLMS(array)
        array[:,:,:3] = np.dot(array[:,:,:3],self.logLMSMlab)
        return array
    
    @classmethod
    def lab2logLMS(self, _array):
        array = np.copy(_array).astype(float)
        array[:,:,:3] = np.dot(array[:,:,:3],self.labMlogLMS)
        return array
    
    @classmethod
    def lab2LMS(self, _array):
        array = np.copy(_array)
        array = self.lab2logLMS(array)
        array[:,:,:3] = np.exp(np.log(10)*array[:,:,:3])
        return array
    
    @classmethod
    def lab2rgb(self, _array):
        array = np.copy(_array)
        array = self.lab2LMS(array)
        array[:,:,:3] = np.dot(array[:,:,:3],self.LMSMrgb)
        return array
