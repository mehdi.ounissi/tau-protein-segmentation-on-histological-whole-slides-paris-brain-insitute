#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 22 14:00:21 2020

@author: valentinabadie
"""

#import matplotlib.pyplot as plt
import cv2
import numpy as np
from shapely.geometry import Point,Polygon, MultiPoint
from tqdm import tqdm
from scipy.ndimage import gaussian_filter
from library.SlidesHandling import DataSetSlides
from shapely.ops import nearest_points
import pandas as pd
from collections.abc import Iterable


class MorphologyStudy():
    
    """This is about 
    
    Attributes :
        labels : list of 2D lists, contours delimiting the retrieved objects
        gray_matter : list, gray matter contours
        metrics : dict, containing the different variables computed
    
    Methods :
    
        Built-in :
            __init__ : initializes the class
        
        Special :
            surface : computes the surface of all the objects of the slide
            perimeter : computes the perimeter of all the objects of the slide
            density : computes the density of objects of the slide (N objects/ square meter)
            circularity : computes the circularity of all the objects of the slide
            convex_hull_surface : computes the convex hull surface of all the objects of the slide
            convex_hull_perimeter : computes the convex hull perimeter of all the objects of the slide
            convexity : computes the convexity of all the objects of the slide
            roughness : computes the roughness of all the objects of the slide
            load : computes the density of objects of the slide (surface objects/ square meter)
            form_factor : computes the form factor of all the objects of the slide
            intensity : computes the intensity of all the objects of the slide
            integrated_density : computes the integrated density of all the objects of the slide
            proximity : computes the proximity of all the objects of the slide
            axes : computes the equivalent axes of all the objects of the slide
        
        Backend :
            _convert_to_pixel : uses the boundaries of a region to compute the region segmentation positions on a pixel basis
    
    """
    
    def __init__(self, labels, slide, object_group = 2):
        
        """initializes the class
        
        Inputs :
            labels : list of 2D lists, contours delimiting the retrieved objects
            slide : Slide object, the slide on which segmentation has been performed
        
        """
        self.slide = slide
        self.labels = labels
        self.object_group = object_group
        
        self.gray_matter = [label for obj, label in list(filter(lambda x: x[0] == 1, slide.labels))]
        self.metrics = dict()
        
    def surface(self):
        """computes the surface of all the objects of the slide"""
        
        surfaces = list()
        for label in self.labels :
            surfaces.append(Polygon(list(zip(*label.T))).area)
        
        self.metrics['Surface'] = surfaces
        
    def perimeter(self):
        """computes the perimeter of all the objects of the slide"""
        
        perimeters = list()
        for label in self.labels :
            perimeters.append(Polygon(list(zip(*label.T))).length)
        
        self.metrics['Perimeter'] = perimeters
        
    def density(self):
        """computes the density of objects of the slide (N objects/ square meter)"""
        
        surfaces_gray_matter = list()
        for label in self.gray_matter :
            surfaces_gray_matter.append(Polygon(list(zip(*label.T))).area)
        
        self.metrics['Density'] = [len(self.labels)/sum(surfaces_gray_matter)]*len(self.labels)
    
    def circularity(self):
        """computes the circularity of all the objects of the slide"""
        
        if 'Surface' in self.metrics:
            surfaces = self.metrics['Surface']
        else :
            self.surface()
            surfaces = self.metrics['Surface']
            
        if 'Perimeter' in self.metrics:
            perimeters = self.metrics['Perimeter']
        else :
            self.perimeter()
            perimeters = self.metrics['Perimeter']
        
        self.metrics['Circularity'] = [4*np.pi*x/y**2 for x,y in list(zip(surfaces, perimeters))]
        
    def convex_hull_surface(self):
        """computes the convex hull surface of all the objects of the slide"""
        
        surfaces = list()
        for label in self.labels :
            surfaces.append(Polygon(list(zip(*label.T))).convex_hull.area)
        
        self.metrics['Convex hull surface'] = surfaces
        
    def convex_hull_perimeter(self):
        """computes the convex hull perimeter of all the objects of the slide"""
        
        perimeters = list()
        for label in self.labels :
            perimeters.append(Polygon(list(zip(*label.T))).convex_hull.length)
        
        self.metrics['Convex hull perimeter'] = perimeters
        
    def convexity(self):
        """computes the convexity of all the objects of the slide"""
        
        if 'Surface' in self.metrics:
            surfaces = self.metrics['Surface']
        else :
            self.surface()
            surfaces = self.metrics['Surface']
            
        if 'Convex hull surface' in self.metrics:
            convex_hull_surfaces = self.metrics['Convex hull surface']
        else :
            self.convex_hull_surface()
            convex_hull_surfaces = self.metrics['Convex hull surface']
        
        self.metrics['Convexity'] = [x/y for x,y in list(zip(surfaces, convex_hull_surfaces))]
        
    def roughness(self):
        """computes the roughness of all the objects of the slide"""
        
        if 'Perimeter' in self.metrics:
            perimeters = self.metrics['Perimeter']
        else :
            self.perimeter()
            perimeters = self.metrics['Perimeter']
            
        if 'Convex hull perimeter' in self.metrics:
            convex_hull_perimeters = self.metrics['Convex hull perimeter']
        else :
            self.convex_hull_perimeter()
            convex_hull_perimeters = self.metrics['Convex hull perimeter']
                  
        self.metrics['Roughness'] = [x/y for x,y in list(zip(perimeters, convex_hull_perimeters))]
        
    def load(self):
        """computes the density of objects of the slide (surface objects/ square meter)"""
        
        if 'Surface' in self.metrics:
            surfaces = self.metrics['Surface']
        else :
            self.surface()
            surfaces = self.metrics['Surface']
            
        if 'Density' in self.metrics:
            density = self.metrics['Density']
        else :
            self.density()
            density = self.metrics['Density']
        
        print(np.mean(surfaces))
        print(density)
        
        self.metrics['Load'] = np.mean(surfaces)*np.array(density)
        
    def form_factor(self):
        """computes the form factor of all the objects of the slide"""
        
        form_factors = list()
        for label in self.labels :
            poly = Polygon(list(zip(*label.T)))
            centroid = np.array(poly.centroid.coords)
            form_factors.append(np.sqrt(np.sum((centroid - label)**2)))
            
        self.metrics['Form factor'] = form_factors
        
    def intensity(self):
        """computes the intensity of all the objects of the slide"""
        
        intensities = list()
        for label in self.labels :
            
            bounds = Polygon(list(zip(*label.T))).bounds
            
            #retrieves the region which contains the detected object and converts to grayscale
            array = self.slide(*bounds, read_labels = False, read_mask = False).array_region
            grayscale_array = np.mean(array[:,:,:3], 2)
            
            #make a correspondance between pixels of the region and coordinates of the label
            coordinates_on_image = self._convert_to_pixel(label, bounds, array.shape[:2]) #converts coordinates to pixel position
            polygon_mask = Polygon(list(zip(*coordinates_on_image.T))) #computes the polygon from the label
            X,Y = np.meshgrid(np.arange(array.shape[0]), np.arange(array.shape[1])) #computes the corresponding mask
            coords_mask = np.vstack((X.flatten(),Y.flatten()))
            mask = np.array([polygon_mask.contains(point) for point in MultiPoint(list(zip(*coords_mask)))]).reshape(*array.shape[:2])
            
            #computes the mean intensity of the surface
            intensities.append(np.mean(grayscale_array[mask]))  
                                    
        self.metrics['Intensity'] = intensities
                                    
    def integrated_density(self):
        """computes the integrated density of all the objects of the slide"""
                                    
        if 'Surface' in self.metrics:
            surfaces = self.metrics['Surface']
        else :
            self.surface()
            surfaces = self.metrics['Surface']
        
        if 'Intensity' in self.metrics:
            intensities = self.metrics['Intensity']
        else :
            self.intensity()
            intensities = self.metrics['Intensity']
                                    
        self.metrics['Integrated density'] = [x*y for x,y in list(zip(surfaces, intensities))]
                                    
    def proximity(self):
        """computes the proximity of all the objects of the slide"""
                                    
        distances = list()
        centroids = [list(Polygon(list(zip(*label.T))).centroid.coords)[0] for label in self.labels]       
                                    
        for i, centroid in enumerate(centroids):
            other_centroids = MultiPoint(centroids[:i]+centroids[i+1:])
            nearest_neighbor = list(nearest_points(other_centroids, Point(centroid))[0].coords)[0]
            distances.append(np.sqrt(np.sum((np.array(nearest_neighbor) - np.array(centroid))**2)))
                                    
        self.metrics['Proximity'] = distances
        
        
    #add proximity nearest pixel
                                    
    def axes(self):
        """computes the equivalent axes of all the objects of the slide"""
        axes_long = list()
        axes_little = list()
        for label in self.labels :
            bounds = Polygon(list(zip(*label.T))).bounds
            
            #retrieves the region which contains the detected object
            array = self.slide(*bounds, read_labels = False, read_mask = False).array_region
            
            #make a correspondance between pixels of the region and coordinates of the label
            coordinates_on_image = self._convert_to_pixel(label, bounds, array.shape[:2]) #converts coordinates to pixel position
            polygon_mask = Polygon(list(zip(*coordinates_on_image.T))) #computes the polygon from the label
            X,Y = np.meshgrid(np.arange(array.shape[0]), np.arange(array.shape[1])) #computes the corresponding mask
            coords_mask = np.vstack((X.flatten(),Y.flatten()))
            mask = np.array([polygon_mask.contains(point) for point in MultiPoint(list(zip(*coords_mask)))]).reshape(*array.shape[:2]).astype(int)
            
            mean = np.array([np.mean(X[mask]), np.mean(Y[mask])])
            Ixx = np.sum((X[mask] - mean[0])**2)
            Ixy = np.sum((X[mask] - mean[0])*(Y[mask] - mean[1]))
            Iyy = np.sum((Y[mask] - mean[1])**2)
            
            min_x, max_x = 0.5 * ((Ixx + Iyy) + np.sqrt((Ixx - Iyy)**2 + Ixy**2)), 0.5 * ((Ixx + Iyy) - np.sqrt((Ixx - Iyy)**2 + Ixy**2))
            axes_long.append(max_x)
            axes_little.append(min_x)
                                    
        self.metrics['Long ax'] = axes_long
        self.metrics['Little ax'] = axes_little

    def _convert_to_pixel(self, positions, coords, size):
        """uses the boundaries of the region to compute the region segmentation positions"""
        
        if not isinstance(size, Iterable):
            size = [size, size]
            
        i =  (positions[:,0] - coords[0])/(coords[2] - coords[0])*size[0]
        j =  size[1] - (positions[:,1] - coords[1])/(coords[3] - coords[1])*size[1]
        
        return np.vstack((i,j)).T  
        
    def output_all_variables(self, path_to_save=None):
        
        """Computes all the morphological variables and creates a frame out of them, and save it in a table
        
        Inputs :
            path_to_save : path to the file where the output table is to be saved
        """   
        
        #reinitialises the metrics dict
        self.metrics = dict()
        
        #adds slide, objects information
        self.metrics['Slide ID'] = [self.slide.id] * len(self.labels)
        self.metrics['Object ID'] = list(np.arange(1,len(self.labels)+1))
        self.metrics['Object Group'] = ['Tangles' if self.object_group == 2 else 'Neuritic plaques'] * len(self.labels)
        
        #computes all morphological variables
        
        #variables of size
        self.surface()
        self.perimeter()
        self.convex_hull_surface()
        self.convex_hull_perimeter()
        
        #comparison with a circle
        self.circularity()
        self.form_factor()
        self.axes()
        
        #comparison with a convex object
        self.convexity()
        self.roughness()
        
        #signal intensity variables
        self.intensity()
        self.integrated_density()
        
        #glabal variable
        self.density()
        self.load()
        
        #semi-global variables
        self.proximity()
        
        
        #creates the sheet
        table = pd.DataFrame.from_dict(self.metrics)
        
        #saves it in an excel file
        table.to_excel(path_to_save, sheet_name = self.slide.id)
  
