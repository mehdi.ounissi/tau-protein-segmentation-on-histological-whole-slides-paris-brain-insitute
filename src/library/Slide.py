#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:59 2020

@author: valentinabadie
"""

import numpy as np
from openslide import OpenSlide
import pickle
import numpy
from shapely.geometry import Polygon

path_slides = '/data/slides/'
path_labels = '/data/labels/'
path_infos = '/data/info_slides/'
            
class Slide():
    
    """A class to handle a whole slide ndpi image
    
    Attributes :
        path_slide : str, path to the file containing the slide (.ndpi)
        path_label : str, path to the file containing the labels (.p)
        path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
        slide : openslide object
        level_dims : 2-tuple, size of the slide in pixels, at the thinner magnification level
        labels : list of 2 tuple containing the label of the associated object and its segmentation coordinates
        id : str, ID of the slide
        center : 2-tuple, coordinates of the center of the slide (in micrometers)
        step : 2-tuple, convertion rate between pixels and micrometers (in pixels/micrometers)
        limits : 2-tuple, coordinates of the limits of the slide (in micrometers)
        magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
        
    Methods :
    
        Built-in :
        
            __init__ : initialises the class
            __getitem__ : returns the selected magnification level of the slide
            __len__ : returns the number of magnification levels of the slide
            __repr__ : displays information about the slide
            __str__ : displays a macroscopic view of the slide
            
        Special :
        
            reshape_meter : sets all positions in micrometer outside of the slide to be inside instead
            data_balance : performs an evaluation of the relative balance of different labels in the slide
        
    """
    
    def __init__(self, path_slide, path_label = None, path_info = None, magnification_levels=None):
        
        """initialises the class
        
        Inputs :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p). If there is no labels for this slide, just pass None. Default : None.
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching. If there is no info for this slide, just pass None. Default : None.
            magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
        """

        self.path_slide = path_slide
        self.path_label = path_label
        self.path_info = path_info
        
        #by default, assumes that consecutive magnification levels have a x2 difference up to x40
        if magnification_levels is None :
            self.magnification_levels = [''.join(('x',str(40/2**i))) for i in range(8)]
        else :
            #user can specify magnification level if they are not standard
            self.magnification_levels = magnification_levels

        self.id = self.path_slide.split('/')[-1].split('.')[0]
        self.slide = OpenSlide(path_slide)
        self.level_dims = self.slide.level_dimensions
        
        if path_label is not None :
            with open(path_label,'rb') as file:
                self.labels = pickle.Unpickler(file).load()
        
        if path_info is not None :
            with open(path_info,'rb') as file:
                infos = pickle.Unpickler(file).load()
                self.center = infos['Center']
                self.step = infos['Step']
                self.limits = infos['Limits']

    def __getitem__(self, magnification):
        
        """returns the selected magnification level of the slide
        
        Inputs :
            magnification : int or str, if int returns the magnification-th magnification level, starting from the greatest down to the smaller, if str return the corresponding magnification level
        """
        
        #imports the class LevelSlide
        from library.LevelSlide import LevelSlide
        
        #if magnification is str, converts it to corresponding int
        if type(magnification) is str :
            magnification = 'x' + str(float(magnification[1:]))
            magnification = int(np.where(np.array(self.magnification_levels)==magnification)[0])
            
        return LevelSlide(self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = magnification)
    
    def __len__(self):
        """returns the number of magnification levels of the slide"""
        return len(self.level_dims)
    
    def __repr__(self):
        """displays information about the slide"""
        return str(self.slide)
    
    def __str__(self, magnification=None):
        """displays a macroscopic view of the slide"""
        if magnification == None :
            magnification = self.level_dims[-1]
        
        print(LevelSlide(self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = magnification))
        
    def reshape_meter(self, *args):
        """Sets all positions in micrometer outside of the slide to be inside instead
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - x1,y1,x2,y2,... : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        def rsp_en_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""
            
            bool_array_x_min = (array[:,0] >= 0).astype(float)
            bool_array_y_min = (array[:,1] >= 0).astype(float)
            bool_array_x_max = (array[:,0] <= self.limits[0]).astype(float)
            bool_array_y_max = (array[:,1] <= self.limits[1]).astype(float)
            
            array[:,0] = bool_array_x_min * bool_array_x_max * array[:,0] + (1 - bool_array_x_max) * self.limits[0]
            array[:,1] = bool_array_y_min * bool_array_y_max * array[:,1] + (1 - bool_array_y_max) * self.limits[1]

            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1,1)
            converted = rsp_en_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if (type(args[0]) is float) or (type(args[0]) is int):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.array([args[x_index], args[y_index]]).reshape(-1, 2)
            
            #converts the values
            converted_array =  rsp_en_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)
            
            return tuple(args_out)
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in rsp_en_arr(np.array(args[0]))])
            else :
                return tuple([tuple(line) for line in rsp_en_arr(np.array(args))])
            
        #case 4
        if type(args[0]) is numpy.ndarray :
            return rsp_en_arr(args[0])
        
    def data_balance(self):
        """computes the surface of each label of the slide, and evaluates the portion of tangles and plaques in the gray matter
        """
        
        #stores sum of objects areas for every type of object on the slide
        ROI_sizes = np.zeros(4)
        for obj,coordinates in self.labels:
            polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
            ROI_sizes[obj] += polygon_label.area

        return ROI_sizes[2]/ROI_sizes[1], ROI_sizes[3]/ROI_sizes[1]