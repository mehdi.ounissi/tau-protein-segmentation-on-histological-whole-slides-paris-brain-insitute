#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:59 2020

@author: valentinabadie
"""

import os
import numpy as np
from openslide import OpenSlide
import pickle
import numpy
from shapely.geometry import Polygon, Point, LinearRing
from library.utils import SaveFiles as sf
from collections.abc import Iterable
import random
from shapely.ops import cascaded_union as union
from tqdm import tqdm

path_slides = '../data/slides/'
path_labels = '../data/labels/'
path_infos = '../data/info_slides/'

#################################################################################################################################

class DataSetSlides(object):
    
    """ Class to handle the the dataset of histopathology hole slides and their labels
   
    Attributes :
        path_slides : str, path to the folder containing the slides (.ndpi)
        path_labels : str, path to the folder containing the labels (.p)
        path_infos : list of str, path to the folder containing the info files (.p), which are used to make slides and labels matching
        slide_files : list of str, containing the names of the slide files
        label_files : list of str, containing the names of the label files
        info_files : list of str, containing the names of the info files
        ids : list of str, containing the id of the slides
        
    Methods :
    
        Built-in :
            __init__ : initialises the class with the necessary attributes
            __getitem__ : permits to access to a slide of the dataset by specifying its index or id
            __len__ : returns the number of slides in the dataset
            __repr__ : returns ids of the slides in the dataset
            __iter__ : iterates across the slides in the dataset
    """
    
    def __init__(self, path_slides, path_labels = None, path_infos = None):
        
        """initialises the class
        
        Inputs :
            path_slides : str, path to the folder containing the slides (.ndpi)
            path_labels : str, path to the folder containing the labels (.p). If there is no labels for these slides, just pass None. Default : None.
            path_infos : list of str, path to the folder containing the info files (.p), which are used to make slides and labels matching. If there is no labels for these slides, just pass None. Default : None.
        """
        
        self.path_slides = path_slides
        self.path_labels = path_labels
        self.path_infos = path_infos
        
        self.slide_files = sorted(list(filter(lambda x: x[0] != '.', os.listdir(self.path_slides))))
        
        if self.path_labels is not None:
            self.label_files = sorted(list(filter(lambda x: x[0] != '.', os.listdir(self.path_labels))))
            
        if self.path_infos is not None:
            self.info_files = sorted(list(filter(lambda x: x[0] != '.', os.listdir(self.path_infos))))
        
        self.ids = [item.split('.')[0] for item in self.slide_files]
    
    def __getitem__(self, key):
        
        """returns the chosen slide
        
        Inputs :
            key : int or str, if int returns the slide at position key, if str return the slide of id key.
        
        """
        
        #case key is int
        if type(key) is int :
            return Slide(self.path_slides + self.slide_files[key], None if self.path_labels is None else self.path_labels + self.label_files[key], None if self.path_infos is None else self.path_infos + self.info_files[key])
        
        #case key is str
        if type(key) is str :
            
            #function designed to retrieve files associated to slide id passed as input
            def retrieve_str_in_list(name, list_of_names):
                """returns the first element to be found in a list, containing the string passed as argument"""
                return next(filter(lambda x : name in x, list_of_names))
            
            return Slide(self.path_slides + retrieve_str_in_list(key, self.slide_files), None if self.path_labels is None else self.path_labels + retrieve_str_in_list(key, self.label_files), None if self.path_infos is None else self.path_infos + retrieve_str_in_list(key, self.info_files))
        
    def __len__(self):
        """return the number of slides in the dataset"""
        return len(self.slide_files)
    
    def __repr__(self):
        """displays the file names of the slides"""
        return str(self.ids)
    
    def __iter__(self):
        """Creates iterator object to go through the files of the dataset"""
        return self.ids
    
#################################################################################################################################

class Slide():
    
    """A class to handle a whole slide ndpi image
    
    Attributes :
        path_slide : str, path to the file containing the slide (.ndpi)
        path_label : str, path to the file containing the labels (.p)
        path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
        slide : openslide object
        level_dims : 2-tuple, size of the slide in pixels, at the thinner magnification level
        labels : list of 2 tuple containing the label of the associated object and its segmentation coordinates
        id : str, ID of the slide
        center : 2-tuple, coordinates of the center of the slide (in micrometers)
        step : 2-tuple, convertion rate between pixels and micrometers (in pixels/micrometers)
        limits : 2-tuple, coordinates of the limits of the slide (in micrometers)
        magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
        
    Methods :
    
        Built-in :
        
            __init__ : initialises the class
            __getitem__ : returns the selected magnification level of the slide
            __len__ : returns the number of magnification levels of the slide
            __repr__ : displays information about the slide
            __str__ : displays a macroscopic view of the slide
            
        Special :
        
            reshape_meter : sets all positions in micrometer outside of the slide to be inside instead
            data_balance : performs an evaluation of the relative balance of different labels in the slide
        
    """
    
    def __init__(self, path_slide, path_label = None, path_info = None, magnification_levels=None):
        
        """initialises the class
        
        Inputs :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p). If there is no labels for this slide, just pass None. Default : None.
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching. If there is no info for this slide, just pass None. Default : None.
            magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
        """

        self.path_slide = path_slide
        self.path_label = path_label
        self.path_info = path_info
        
        #by default, assumes that consecutive magnification levels have a x2 difference up to x40
        if magnification_levels is None :
            self.magnification_levels = [''.join(('x',str(40/2**i))) for i in range(8)]
        else :
            #user can specify magnification level if they are not standard
            self.magnification_levels = magnification_levels

        self.id = self.path_slide.split('/')[-1].split('.')[0]
        self.slide = OpenSlide(path_slide)
        self.level_dims = self.slide.level_dimensions
        
        if path_label is not None :
            with open(path_label,'rb') as file:
                self.labels = pickle.Unpickler(file).load()
        
        if path_info is not None :
            with open(path_info,'rb') as file:
                infos = pickle.Unpickler(file).load()
                self.center = infos['Center']
                self.step = infos['Step']
                self.limits = infos['Limits']

    def __getitem__(self, magnification):
        
        """returns the selected magnification level of the slide
        
        Inputs :
            magnification : int or str, if int returns the magnification-th magnification level, starting from the greatest down to the smaller, if str return the corresponding magnification level
        """
        
        #if magnification is str, converts it to corresponding int
        if type(magnification) is str :
            magnification = 'x' + str(float(magnification[1:]))
            magnification = int(np.where(np.array(self.magnification_levels)==magnification)[0])
            
        return LevelSlide(self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = magnification)
    
    def __len__(self):
        """returns the number of magnification levels of the slide"""
        return len(self.level_dims)
    
    def __repr__(self):
        """displays information about the slide"""
        return str(self.slide)
    
    def __str__(self, magnification=None):
        """displays a macroscopic view of the slide"""
        if magnification == None :
            magnification = self.level_dims[-1]
        
        print(LevelSlide(self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = magnification))
        
    def reshape_meter(self, *args):
        """Sets all positions in micrometer outside of the slide to be inside instead
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - x1,y1,x2,y2,... : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        def rsp_en_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""
            
            bool_array_x_min = (array[:,0] >= 0).astype(float)
            bool_array_y_min = (array[:,1] >= 0).astype(float)
            bool_array_x_max = (array[:,0] <= self.limits[0]).astype(float)
            bool_array_y_max = (array[:,1] <= self.limits[1]).astype(float)
            
            array[:,0] = bool_array_x_min * bool_array_x_max * array[:,0] + (1 - bool_array_x_max) * self.limits[0]
            array[:,1] = bool_array_y_min * bool_array_y_max * array[:,1] + (1 - bool_array_y_max) * self.limits[1]

            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1,1)
            converted = rsp_en_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if (type(args[0]) is float) or (type(args[0]) is int):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.array([args[x_index], args[y_index]]).reshape(-1, 2)
            
            #converts the values
            converted_array =  rsp_en_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)
            
            return tuple(args_out)
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in rsp_en_arr(np.array(args[0]))])
            else :
                return tuple([tuple(line) for line in rsp_en_arr(np.array(args))])
            
        #case 4
        if type(args[0]) is numpy.ndarray :
            return rsp_en_arr(args[0])
        
    def data_balance(self):
        """computes the surface of each label of the slide, and evaluates the portion of tangles and plaques in the gray matter
        """
        
        #stores sum of objects areas for every type of object on the slide
        ROI_sizes = np.zeros(4)
        for obj,coordinates in self.labels:
            polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
            ROI_sizes[obj] += polygon_label.area

        return ROI_sizes[2]/ROI_sizes[1], ROI_sizes[3]/ROI_sizes[1]

##################################################################################################################################

class LevelSlide(Slide):
    
    """A class to handle a specified scale of a slide
    
    Attributes :
    
        Attributes coming from Slide class (class with super()) :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            slide : openslide object
            level_dims : 2-tuple, size of the slide in pixels, at the thinner magnification level
            labels : list of 2 tuple containing the label of the associated object and its segmentation coordinates
            id : str, ID of the slide
            center : 2-tuple, coordinates of the center of the slide (in micrometers)
            step : 2-tuple, convertion rate between pixels and micrometers (in pixels/micrometers)
            limits : 2-tuple, coordinates of the limits of the slide (in micrometers)
            magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
        
        magnification : scaling level of the slide
        dims : dimensions of the level concerned
        _sampler_params : parameters of the sampler (size, stride, offset, borders, ROI, ROIp)
        sampler : an iterable wich samples regions from the level, according to the parameters settled in _sampler_params
        
    Methods :
    
        Built-in :
        
            __init__ : initialises the class
            __call__ : allows to call a rectangular region of this magnification level of the slide
            __str__ : displays a visual form of this magnification level
            __getitem__ : allows to get a region of the magnification level, according to the sampler parameters
            __len__ : number of regions which can be generated by the sampler

        Special : 
        
            update_sampler : changes the sampler, according the parameters set via _update_sampler_params
            meter_to_pixel : converts coordinates given in meters to coordinates in pixels for this magnification level
            pixel_to_meter : converts coordinates given in pixels to coordinates in meters for this magnification level
            reshape_pixel : sets all positions in pixel outside of this magnification level of the slide to be inside instead
            save : saves all regions of this magnification level, according to the sampler parameters

        Backend :
        
            _update_sampler_params : updates the sampler parameters
            _make_sampler : creates a sampler for this magnification level
    
    """
    
    def __init__(self, path_slide, path_label=None, path_info=None, magnification=-1):
        
        """initilises the class
        
        Inputs :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            magnification : int or str, if int returns the magnification-th magnification level, starting from the greatest down to the smaller, if str return the corresponding magnification level. Default : -1.

        """
        
        super().__init__(path_slide, path_label = path_label, path_info = path_info)
        
        self.magnification = magnification
        
        if magnification == -1:
            self.magnification = len(self.level_dims) -1
            
        self.dims = self.level_dims[magnification]
        
        
        
        self._sampler_params = dict()
        
    def __call__(self, *coordinates, type_coord='meter', read_labels=True, read_mask=True):
        """Returns the region selected by the input coordinates.
        
        Inputs :
            coordinates : 4-tuple, the coordinates of the 4 points delimiting the rectangular region to be called.
            type_coord : str. If 'meter', the coordinates passed as input will be considered as real life meter coordinates, if 'pixel' it will be considered as pixel values on the magnification level. Default : 'meter'.
            read_labels : bool, if True retrieves the labels intersecting the region to be returned. Default : True.
            read_mask : bool, if True creates a mask associated with labels of the region. Default : True.
        
        """
        
        return Region(coordinates, self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = self.magnification, type_coord=type_coord, read_labels=read_labels, read_mask=read_mask)
    
    def __str__(self, read_labels = True):
        
        """Displays the whole level as a single region. WATCH OUT : it can induce RAM crashes for high resolution levels
        
        Inputs :
            read_labels : bool. If True, displays the labels of teh magnification level as well. Default : True.
        """
        
        from library.Region import Region
        print(Region((self.limits[0],self.limits[2],self.limits[1],self.limits[3]), self.path_slide, path_label = self.path_label, path_info = self.path_info, magnification = self.magnification, read_labels = read_labels, read_mask = False))
        return ''
    
    def __len__(self):
        """returns the number region which can be selected by the sampler"""
        return len(self.sampler)

    def __getitem__(self,index):
        
        """returns the region corresponding to the index passed as input
        
        Inputs :
            index : int, the index of the wanted region in the sampler.
        
        """
        
        region = self.__call__(*self.sampler[index],type_coord ='pixel')
        return region
        
        
    def meter_to_pixel(self, *args):
        
        """Converts an information given in micrometers to an information in pixel position
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - x1,y1,x2,y2,... : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        
        def mtr_px_arr(array):
            """this function is the restriction of meter_to_pixel to the 4th type of input data (array)"""
            
            #performs the transformation into pixels
            center_pixels = (np.array(self.dims)/2).reshape((-1, 2)).astype(float)
            array = center_pixels + (((array-self.center)*self.step)/2**self.magnification)
            array[:,1] = self.dims[1]-array[:,1] #flips coordinates on y axis
            
            return array.round().astype(int)
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1, 1)
            converted = mtr_px_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if not isinstance(args[0], Iterable):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
            
            #converts the values
            converted_array =  mtr_px_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(-1)
            args_out[y_index] = converted_array[:,1].reshape(-1)
            
            return args_out
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in mtr_px_arr(np.array(args[0]))])
            else :
                if len(args[0])==2:
                    return tuple([tuple(line) for line in mtr_px_arr(np.array(args))])
                else :
                    args = np.array(args[0])
                    x_index = [x for x in range(0, len(args), 2)]
                    y_index = [y for y in range(1, len(args), 2)]
                    array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
                    
                    #converts the values
                    converted_array =  mtr_px_arr(array_args)
        
                    #reshapes the result to have an input-like output
                    args_out = np.zeros(len(args)).astype(int)
                    args_out[x_index] = converted_array[:,0].reshape(-1)
                    args_out[y_index] = converted_array[:,1].reshape(-1)
                    
                    return args_out
            
        #case 4
        if type(args[0]) is numpy.ndarray :
            return mtr_px_arr(args[0])
        
        return 'Input not in a proper format'
    
    def pixel_to_meter(self, *args):
        
        """Converts an information given in pixel position to an information in micrometers
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - x1,y1,x2,y2,... : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        
        def px_mtr_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""

            #performs transformation into micrometers
            center = (np.array(self.dims)/2).reshape(-1, 2).astype(float)
            array[:,1] = self.dims[1] - array[:,1] #flips the y axis
            array = ((array.astype(float) - center)*2**self.magnification)/self.step + self.center
            
            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1, 1)
            converted = px_mtr_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if not isinstance(args[0], Iterable):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
            
            #converts the values
            converted_array =  px_mtr_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args))
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)
            
            return args_out
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in px_mtr_arr(np.array(args[0]))])
            else :
                if len(args[0])==2:
                    return tuple([tuple(line) for line in px_mtr_arr(np.array(args))])
                else :
                    args = np.array(args[0])
                    x_index = [x for x in range(0, len(args), 2)]
                    y_index = [y for y in range(1, len(args), 2)]
                    array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
                    
                    #converts the values
                    converted_array =  px_mtr_arr(array_args)
        
                    #reshapes the result to have an input-like output
                    args_out = np.zeros(len(args))
                    args_out[x_index] = converted_array[:,0].reshape(1, -1)
                    args_out[y_index] = converted_array[:,1].reshape(1, -1)
                    
                    return args_out
            
        #case 4
        if type(args[0]) is numpy.ndarray :
            return px_mtr_arr(args[0])
        
        return 'Input not in a proper format'
    
    def reshape_pixel(self, *args):
        """Sets all pixel coordinates outside of the image to be inside instead
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - (x1,y1,x2,y2,...) : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        
        def rsp_en_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""
            
            bool_array_x_min = (array[:,0] >= 0).astype(int)
            bool_array_y_min = (array[:,1] >= 0).astype(int)
            bool_array_x_max = (array[:,0] <= self.dims[0]).astype(int)
            bool_array_y_max = (array[:,1] <= self.dims[1]).astype(int)
            
            array[:,0] = bool_array_x_min * bool_array_x_max * array[:,0] + (1 - bool_array_x_max) * (self.dims[0]-1)
            array[:,1] = bool_array_y_min * bool_array_y_max * array[:,1] + (1 - bool_array_y_max) * (self.dims[1]-1)

            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1,1)
            converted = rsp_en_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if not isinstance(args[0], Iterable):
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
            
            #converts the values
            converted_array =  rsp_en_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)

            return args_out
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in rsp_en_arr(np.array(args[0]))])
            else :
                if len(args[0])==2:
                    return tuple([tuple([int(i) for i in line]) for line in rsp_en_arr(np.array(args))])
                else :
                    args = np.array(args[0])
                    x_index = [x for x in range(0, len(args), 2)]
                    y_index = [y for y in range(1, len(args), 2)]
                    array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
                    
                    #converts the values
                    converted_array =  rsp_en_arr(array_args)
        
                    #reshapes the result to have an input-like output
                    args_out = np.zeros(len(args)).astype(int)
                    args_out[x_index] = converted_array[:,0].reshape(1, -1)
                    args_out[y_index] = converted_array[:,1].reshape(1, -1)
                    
                    return args_out
        #case 4
        if type(args[0]) is numpy.ndarray :
            return rsp_en_arr(args[0])
    
    
    
    def _update_sampler_params(self, size=None, stride=None, offset=None, borders=None, ROI=None,ROIp=None,balance=None,tolerance=None,include_gray_matter=None, random_pos = None):
        
        """sets the parameters of the sampler. Some parameters may not be coherent as tolerance and stride, but they are used in different cases. Report to the method _make_sampler for more details.
        
        Inputs :
            size : 2-tuple, dimensions of each generated region
            stride : 2-tuple, the stride between 2 consecutive regions
            offset : 2-tuple, distance from the top-left of the slide to start picking regions
            border : bool, includes or not areas which are partly out of the slide
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
            ROIp : float in [0,1], if>0 only selects the region of which the fraction intersecting the ROI is at least above                   ROIp. Default 0.
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            include_gray_matter : bool. If True, only regions in gray matter will be sampled; Defualt : True.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
        """
        
        if size is not None :
            self._sampler_params['size'] = size
        
        if stride is not None :
            self._sampler_params['stride'] = stride
        
        if offset is not None :
            self._sampler_params['offset'] = offset
        
        if borders is not None :
            self._sampler_params['borders'] = borders
        
        if ROI is not None :
            self._sampler_params['ROI'] = ROI
            
        if ROIp is not None :
            self._sampler_params['ROIp'] = ROIp
            
        if balance is not None:
            self._sampler_params['balance'] = balance
        else :
            self._sampler_params['balance'] = 0.5
            
        if tolerance is not None :
            self._sampler_params['tolerance'] = tolerance
        
        if include_gray_matter is not None :
            self._sampler_params['include_gray_matter'] = include_gray_matter
        else :
            self._sampler_params['include_gray_matter'] = True
            
        if random_pos is not None :
            self._sampler_params['random_pos'] = random_pos
        else :
            self._sampler_params['random_pos'] = False
            
    def _make_sampler(self, random = True):
        
        """This function creates a sampler for the magnification level
        
        Inputs :
            random : bool. If True, creates a sampler which generate positive samples by targetting regions containing objects, and negative by randomization. The only parameters to be taken into account in this case are : size, ROI, balance, tolerance, include_gray_matter and random_pos. If False, generates samples one after the other, by simple 2-D constant stride sampling. The only parameters to be taken into account in this case are : size, ROI, ROIp, offset, stride, borders. Default : True.
        
        """
        
        #if random = True, takes RandomSampler as default sampler
        if random :
            self.sampler = Samplers.RandomSampler(self, self._sampler_params['size'], ROI = self._sampler_params['ROI'],balance = self._sampler_params['balance'], tolerance = self._sampler_params['tolerance'], include_gray_matter = self._sampler_params['include_gray_matter'], random_pos = self._sampler_params['random_pos'])
            
        #else, creates a Determinist one
        else :
            self.sampler = Samplers.DeterministSampler(self, self._sampler_params['size'], self._sampler_params['stride'], self._sampler_params['offset'], self._sampler_params['borders'],self._sampler_params['ROI'],self._sampler_params['ROIp'])
            
    def update_sampler(self, size=None, stride=None, offset=None, borders=None, ROI=None, ROIp=None,balance=None,tolerance=None,include_gray_matter=None, random = True, random_pos = True):
        
        """This function makes a pipeline between _update_sampler_params and _make_sampler.
        
        Inputs :
            size : 2-tuple, dimensions of each generated region
            stride : 2-tuple, the stride between 2 consecutive regions
            offset : 2-tuple, distance from the top-left of the slide to start picking regions
            border : bool, includes or not areas which are partly out of the slide
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default : 1.
            ROIp : float in [0,1], if>0 only selects the region of which the fraction intersecting the ROI is at least above                   ROIp. Default : 0.
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            include_gray_matter : bool. If True, only regions in gray matter will be sampled; Defualt : True.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
            
            random : bool. If True, creates a sampler which generate positive samples by targetting regions containing objects, and negative by randomization. The only parameters to be taken into account in this case are : size, ROI, balance, tolerance, include_gray_matter and random_pos. If False, generates samples one after the other, by simple 2-D constant stride sampling. The only parameters to be taken into account in this case are : size, ROI, ROIp, offset, stride, borders. Default : True.
            
        
        """
        self._update_sampler_params(size=size,stride=stride,offset=offset,borders=borders,ROI=ROI,ROIp=ROIp,balance=balance,tolerance=tolerance,include_gray_matter=include_gray_matter, random_pos = random_pos)
        
        self._make_sampler(random = random)
    
    
    def save(self,path=None):
        
        """Saves all regions to be generated by the sampler of the selected magnification level
        
        Inputs :
            path : str. Path to the folder where regions have to be saved. Default : None.
        """
        
        if path is None :
            path = self.path_labels
        
        name_folder = 'level_{}_{}_{}_{}_{}_{}_{}.p'.format(self.id,self.level,self._sampler_params['size'],self._sampler_params['stride'],self._sampler_params['offset'],'b' if self._sampler_params['borders'] else 'n',self._sampler_params['ROI'],self._sampler_params['ROIp'])
        
        try :
            os.mkdir(path+name_folder)
        except :
            pass
        
        for region in self:
            region.save(path+name_folder)

#################################################################################################################################

class Region(LevelSlide):
    """A class to handle a rectangular region of a slide, for a specific scale
    
    Attributes :
    
        Attributes coming from LevelSlide class (class with super()) :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            slide : openslide object
            level_dims : 2-tuple, size of the slide in pixels, at the thinner magnification level
            labels : list of 2 tuple containing the label of the associated object and its segmentation coordinates
            id : str, ID of the slide
            center : 2-tuple, coordinates of the center of the slide (in micrometers)
            step : 2-tuple, convertion rate between pixels and micrometers (in pixels/micrometers)
            limits : 2-tuple, coordinates of the limits of the slide (in micrometers)
            magnification_levels : list, levels of magnification available for the slide. If None, assumes that consecutive magnification levels have a x2 difference up to x40. Default : None.
            magnification : scaling level of the slide
            dims : dimensions of the level concerned
            _sampler_params : parameters of the sampler (size, stride, offset, borders, ROI, ROIp)
            sampler : an iterable wich samples regions from the level, according to the parameters settled in _sampler_params
            
        array_region : array containing the value of the pixels of the region
        coordinates : 4-tuple of ints  containing the x1,y1,x2,y2 coordinates of the region
        in_labels : list, labels intersecting the region
        shape : 3-tuple, shape of the region array
        size : 2-tuple, size on the region
        colors : list, list of colors used to display labels properly.
        read_labels : bool, if True reads the intersecting labels. Default : True.
        read_mask : bool, if True makes a mask out of intersecting labels. Default : True.
        
    Methods :
    
        Built-in :
            __init__ : initialises the class.
            __str__ : displkays a visual aspect of the region
            
        Special :
            reshape_meter : takes input coordinates in meters, and places it inside the region if outside.
            save : saves the regions with its labels and mask
        
        Backend :
            _read_region : retrieves and read the wanted rectangular region.
            _read_labels : retrieves slide labels intersecting the region.
            _read_mask : computes the mask of the region out of its labels.
            
        
        
    To read a region, you must input the path, the level of the slide and the 4 coordinates
    delimiting the region x1,y1,x2,y2. You specify to what kind of coordinates that corresponds,
    via the parameter type_coord which can be set either to 'meter' or 'pixel'. Default to 'meter'.
    
    """
    
    def __init__(self, coordinates, path_slide, path_label = None, path_info = None, magnification = -1, type_coord='meter',read_labels = True, read_mask = True):
        
        """Initialises the class
        
        Inputs :
            path_slide : str, path to the file containing the slide (.ndpi)
            path_label : str, path to the file containing the labels (.p)
            path_info : list of str, path to the file containing the info file (.p), which is used to make slide and labels matching
            magnification : int or str, if int returns the magnification-th magnification level, starting from the greatest down to the smaller, if str return the corresponding magnification level. Default : -1.
            coordinates : 4-tuple of ints  containing the x1,y1,x2,y2 coordinates of the region
            type_coord : str. If 'meter', the coordinates passed as input will be considered as real life meter coordinates, if 'pixel' it will be considered as pixel values on the magnification level. Default : 'meter'.
            read_labels : bool, if True reads the intersecting labels. Default : True.
            read_mask : bool, if True makes a mask out of intersecting labels. Default : True.
            
        """
        
        
        super().__init__(path_slide,path_label = path_label, path_info = path_info, magnification = magnification)
        
        coordinates = sorted([coordinates[0],coordinates[2]])+sorted([coordinates[1],coordinates[3]])
        self.coordinates = [coordinates[0],coordinates[2], coordinates[1],coordinates[3]]
        
        self._read_region(type_coord)

        
        self.colors = ['blue','green','red','white']
        
        self.shape = self.array_region.shape
        self.size = (self.shape[1],self.shape[0])
        
        if read_labels and (path_label is not None) and  (path_info is not None):
            self._read_labels()
            if read_mask :
                self._read_mask()
    
    def _read_region(self,type_coord):
        """reads the region specified as input
       
        Inputs :
            type_coord : str. If 'meter', the coordinates passed as input will be considered as real life meter coordinates, if 'pixel' it will be considered as pixel values on the magnification level. Default : 'meter'.
        
        """
        
        if type_coord == 'meter':
            xp1,yp1,xp2,yp2 = self.meter_to_pixel(self.coordinates)

            xp1,yp1,xp2,yp2 = self.reshape_pixel(xp1,yp1,xp2,yp2)

            #permutation of coordinates
            xp1,xp2 = sorted([xp1,xp2])
            yp1,yp2 = sorted([yp1,yp2])
            
            #retrieves th corresponding poisitions in meters
            self.positions = np.meshgrid(np.linspace(self.coordinates[0],self.coordinates[2],xp2-xp1),np.linspace(self.coordinates[1],self.coordinates[3],yp2-yp1))
            
            top_left = (xp1*2**self.magnification,yp1*2**self.magnification)
            size = (xp2-xp1,yp2-yp1)
            
            self.array_region = np.array(self.slide.read_region(top_left,self.magnification,size))
        
        if type_coord == 'pixel' :
            xp1,yp1,xp2,yp2 = self.coordinates
            
            coordinates = self.pixel_to_meter(self.coordinates)
            coordinates = sorted([coordinates[0],coordinates[2]])+sorted([coordinates[1],coordinates[3]])
            self.coordinates = [coordinates[0],coordinates[2], coordinates[1],coordinates[3]]
            
            xp1,yp1,xp2,yp2 = self.reshape_pixel(xp1,yp1,xp2,yp2)
            
            #permutation of coordinates
            xp1,xp2 = sorted([xp1,xp2])
            yp1,yp2 = sorted([yp1,yp2])
            
            #retrieves th corresponding poisitions in meters
            self.positions = np.meshgrid(np.linspace(self.coordinates[0],self.coordinates[2],xp2-xp1),np.linspace(self.coordinates[1],self.coordinates[3],yp2-yp1))
            
            top_left = (xp1*2**self.magnification,yp1*2**self.magnification)
            size = (xp2-xp1,yp2-yp1)
            
            self.array_region = np.array(self.slide.read_region(top_left,self.magnification,size))
    
    def _read_labels(self):
        """gets the labels that intersect the region, and modify them to display properly"""
        
        self.in_labels = list()
        
        #creates a polygon object corresponding to the region 
        x1,y1,x2,y2 = self.coordinates
        polygon_region = Polygon(((x1,y1),(x2,y1),(x2,y2),(x1,y2)))

        #check, for every labelled object, if it intersects the region
        for object_type,coordinates in self.labels :
            #creates a polygon object
            polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
            
            #checking intersection
            if polygon_label.intersects(polygon_region) :
                try :
                    in_coordinates = np.array(list((polygon_label.intersection(polygon_region)).exterior.coords))
                    self.in_labels.append((object_type,in_coordinates))
                except :
                    pass
        
    
    def reshape_meter(self, *args):
        """Sets all pixel coordinates outside of the image to be inside instead
        
        Arguments can be : 
            1 - type of coordinate,x1,x2,...: type_of_coordinate being a string ('x' for x axis, 'y' for y axis)
            2 - (x1,y1,x2,y2,...) : a tuple of paired coordinates
            3 - (x1,y1),(x2,y2),... : a list of 2-tuple pairs of coordinates
            4 - array([[x1,y1],[x2,y2],...]) : a n-by-2 array of coordinates
        """
        
        x_m,x_M = sorted([self.coordinates[0],self.coordinates[2]])
        y_m,y_M = sorted([self.coordinates[1],self.coordinates[3]])
        
        def rsm_en_arr(array):
            """this function is the restriction of pixel_to_meter to the 4th type of input data (array)"""

            bool_array_x_min = (array[:,0] >= x_m).astype(float)
            bool_array_y_min = (array[:,1] >= y_m).astype(float)
            bool_array_x_max = (array[:,0] <= x_M).astype(float)
            bool_array_y_max = (array[:,1] <= y_M).astype(float)
            
            array[:,0] = bool_array_x_min * bool_array_x_max * array[:,0] + ((1 - bool_array_x_min) * x_m) + ((1 - bool_array_x_max) * x_M)
            array[:,1] = bool_array_y_min * bool_array_y_max * array[:,1] + ((1 - bool_array_y_min) * y_m) + ((1 - bool_array_y_max) * y_M)

            return array
        
        # case 1
        if type(args[0]) is str :
            
            #since the first arg is to tell the type, the rest is only taken into account
            to_convert = np.array(args[1:]).reshape(-1,1)
            converted = rsm_en_arr(np.concatenate((to_convert, to_convert), 1))
            
            #the output depends on the type of input
            if args[0] == 'x':
                return tuple(converted[:,0])
            if args[0] == 'y':
                return tuple(converted[:,1])
         
        #case 2
        if (type(args[0]) is float) or (type(args[0]) is int):
            
            #sets the 2n sized input to a n*2 array
            args = np.array(args)
            x_index = [x for x in range(0, len(args), 2)]
            y_index = [y for y in range(1, len(args), 2)]
            array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
            
            #converts the values
            converted_array =  rsm_en_arr(array_args)

            #reshapes the result to have an input-like output
            args_out = np.zeros(len(args)).astype(int)
            args_out[x_index] = converted_array[:,0].reshape(1, -1)
            args_out[y_index] = converted_array[:,1].reshape(1, -1)
            
            return tuple(args_out)
        
        #case 3
        if (type(args[0]) is tuple) or (type(args[0]) is list) :
            if (type(args[0][0]) is tuple) or (type(args[0][0]) is list) :
                return tuple([tuple(line) for line in rsm_en_arr(np.array(args[0]))])
            else :
                if len(args[0])==2:
                    return tuple([tuple(line) for line in rsm_en_arr(np.array(args))])
                else :
                    args = np.array(args[0])
                    x_index = [x for x in range(0, len(args), 2)]
                    y_index = [y for y in range(1, len(args), 2)]
                    array_args = np.concatenate([args[x_index].reshape(-1,1), args[y_index].reshape(-1,1)], 1)
                    
                    #converts the values
                    converted_array =  rsm_en_arr(array_args)
        
                    #reshapes the result to have an input-like output
                    args_out = np.zeros(len(args)).astype(int)
                    args_out[x_index] = converted_array[:,0].reshape(1, -1)
                    args_out[y_index] = converted_array[:,1].reshape(1, -1)
                    
                    return tuple(args_out)
        #case 4
        if type(args[0]) is numpy.ndarray :
            return rsm_en_arr(args[0])
    
    def _read_mask(self,priority=[1,0,2,3]):
        
        """ Computes the mask of the region. It outputs an array of the same size as the region, containing
        intergers which indicate whether a pixel belongs or not to a given object class (0 for nor class,
        and 1, 2, 3... according to the classes passed as argument)
        
        Inputs :
            - priority : list or tuple of integers. if several classes overlap, only one can be represented
            in the final result. Hence, a priority order has to be given. The priority argument is a list
            containing the classes in the inverse order of priority : [3,0,1,2] indicates 2 class to has prio-
            rity on class 1, which has priority on class 0 which has priority on class 3. If priority is passed
            as 'default', the order is [0,1,2,3,...]
        
        """
        
        mask = np.zeros(np.flip(self.size))
        
        if self.in_labels == []:
            self.mask = mask
            return
        
        if priority == 'default':
            priority = list(sorted(np.unique(list(list(zip(*self.in_labels))[0]))))
        
        #for each label in priority order, retrieves pixels belonging to the corresponding class
        for label in priority :
            coordinates_label = list(zip(*list(filter(lambda x:x[0] == label,self.in_labels))))
            #print(coordinates_label)
            if coordinates_label:
                for coordinates in list(coordinates_label[1]) :
                    
                    #creates a Polygon from the coordinates detouring the object
                    polygon_label = Polygon([(point[0],point[1]) for point in coordinates])
                    edges_label = LinearRing([(point[0],point[1]) for point in coordinates])
                    
                    #creating a frame of pixel positions including the mask to retrieve
                    X,Y = self.positions
                    mask_contour = (X>=np.min(coordinates[:,0]))* (X<=np.max(coordinates[:,0]))*(Y>=np.min(coordinates[:,1]))* (Y<=np.max(coordinates[:,1]))
                    X_in,Y_in = X[mask_contour],Y[mask_contour]
                    pixels = np.vstack((X_in.flatten(),Y_in.flatten())).T
                    
                    #computing the mask of the concerned object
                    mask_contour[mask_contour] = np.array([polygon_label.contains(Point(pixel[0],pixel[1])) or edges_label.contains(Point(pixel[0],pixel[1])) for pixel in pixels])
                    mask[mask_contour] = label*(mask_contour[mask_contour].astype(float))
                
        self.mask = np.flip(mask,0)
         
    def __str__(self):
        
        """ Displays the region, with its labels, and the corresponding mask"""
        
        from matplotlib.pyplot import subplots
        fig,ax = subplots(1,int('mask' in self.__dict__)+1,figsize=(10,10*(int('mask' in self.__dict__)+1)))
        
        if 'mask' in self.__dict__ :
            ax[0].imshow(self.array_region, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
            
            for val in self.in_labels :
                ax[0].plot(val[1][:,0], val[1][:,1],color = self.colors[val[0]])
                
            ax[1].imshow(self.mask, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
                
        else :
            ax.imshow(self.array_region, cmap='gray', extent = [self.coordinates[0],self.coordinates[2],self.coordinates[1],self.coordinates[3]])
            if 'in_labels' in self.__dict__ :
                for val in self.in_labels :
                    ax.plot(val[1][:,0], val[1][:,1],color = self.colors[val[0]])
        
        return ''
    
    def save(self,path=None):
        """saves the region object in a file
        
        Inputs :
            path : the path folder where to save the region.
        
        """
        
        if path is None :
            path = self.path_regions
        if 'in_labels' not in self.__dict__ :
            self._read_labels()
        if 'mask' not in self.__dict__ :
            self._read_mask()
            
        infos_to_save = {'image' : self.array_region, 'mask' : self.mask, 'labels' : self.in_labels, 'magnification': self.magnification, 'coordinates': self.coordinates,'id': self.id}
        
        name = 'region_{}_{}_{}_{}_{}_{}'+'.p'
        name = name.format(self.id,self.magnification,self.coordinates[0],self.coordinates[1],self.coordinates[2],self.coordinates[3])
        
        if path[-1] == '/':
            sf.save(path + name, infos_to_save)
        else :
            sf.save(path +'/'+ name, infos_to_save)

#################################################################################################################################

class Samplers :
    
    """This class contains the 2 types of samplers as described in the report
    
    Attributes :
        DeterministSampler : class, determinist sampler
        RandomSampler : class, random sampler
    
    """
    
    ############################################################################################################################
    
    class DeterministSampler():
        """ A class to handle the generation of several sub-regions of a slide

        Attributes :
            levelSlide : obj, the LevelSlide object to which the sampler is attached
            size : 2-tuple, dimensions of each generated region
            stride : 2-tuple, the stride between 2 consecutive regions
            offset : 2-tuple, distance from the top-left of the slide to start picking regions
            borders : bool, includes or not areas which are partly out of the slide
            ROI : int, selects only the region which interset ROI of the specified class. Set to 0 for no selection (all regions are                 taken into account)
            ROIp : float in [0,1], the minimal amount of ROI which has to interset the region for it to be taken into account. Set                   to 0 to take any region intersecting the ROI.

        Methods :

            Built-in :
                __init__ : initialises the class
                __getitem__ : gets the regions corresponding to the index passed as input
                __len__ : returns the number of regions to be generated by the sampler
                __repr__ : displays information about the sampler

            Backend :
                _set_list_coordinates : makes the list of coordinates of the recangular regions to be retrivied by the sampler

        """

        def __init__(self, levelSlide, size, stride, offset, borders, ROI, ROIp):

            """initialises the class

            Inputs :
                levelSlide : obj, the LevelSlide object to which the sampler is attached
                size : 2-tuple, dimensions of each generated region
                stride : 2-tuple, the stride between 2 consecutive regions
                offset : 2-tuple, distance from the top-left of the slide to start picking regions
                borders : bool, includes or not areas which are partly out of the slide
                ROI : int, selects only the region which interset ROI of the specified class. Set to 0 for no selection (all regions are                 taken into account)
                ROIp : float in [0,1], the minimal amount of ROI which has to interset the region for it to be taken into account. Set                   to 0 to take any region intersecting the ROI.

            """

            self.levelSlide = levelSlide

            if type(size) is int :
                self.size = (size, size)
            else :
                self.size = size

            if type(stride) is int :
                self.stride = (stride, stride)
            else :
                self.stride = stride

            if type(offset) is int :
                self.offset = (offset, offset)
            else :
                self.offset = offset

            self.borders = borders

            self.ROI = ROI

            self.ROIp = ROIp

            self._set_list_coordinates()

        def _set_list_coordinates(self):
            """Creates a list containing all the positions to be used to compute the iterated regions"""

            #creates a grid of region positions
            positions = [[i for i in range(self.offset[k], self.levelSlide.dims[k], self.stride[k])] for k in [0,1]]
            positions_1 = [[i for i in range(self.offset[k] + self.size[k], self.levelSlide.dims[k] + self.size[k] * int(self.borders), self.stride[k])] for k in [0,1]]

            positions_x,positions_x_1 = tuple(zip(*list(zip(positions[0],positions_1[0]))))
            positions_y,positions_y_1 = tuple(zip(*list(zip(positions[1],positions_1[1]))))

            positions_x,positions_y = np.meshgrid(positions_x,positions_y)
            positions_x_1,positions_y_1 = np.meshgrid(positions_x_1,positions_y_1)

            list_coordinates = list(zip(list(positions_x.flatten()),list(positions_y.flatten()),list(positions_x_1.flatten()),list(positions_y_1.flatten())))

            #checks whether the regions are instersecting the chosen ROI (if required)
            if self.ROI != 0 :
                self.list_coordinates = []

                for index,coordinates in enumerate(list_coordinates):

                    coordinates_x = self.levelSlide.pixel_to_meter(*coordinates)
                    coordinates_x = sorted([coordinates_x[0],coordinates_x[2]])+sorted([coordinates_x[1],coordinates_x[3]])
                    x1,y1,x2,y2 = coordinates_x[0],coordinates_x[2], coordinates_x[1],coordinates_x[3]

                    polygon_region = Polygon([(x1,y1),(x2,y1),(x2,y2),(x1,y2)])

                     #check, for every labelled object as the selected ROI, if it intersects the region
                    for object_type,coords in list(filter(lambda x:x[0] == self.ROI,self.levelSlide.labels)):              

                        #creates a polygon object
                        polygon_label = Polygon([(point[0],point[1]) for point in coords])

                        #checking intersection
                        if polygon_label.intersects(polygon_region):
                            if (polygon_label.intersection(polygon_region)).length >= (polygon_region.length*self.ROIp):
                                self.list_coordinates.append(coordinates)
                                break
            else :
                self.list_coordinates = list_coordinates

        def __getitem__(self,index):

            """Gets the region with inptu index

            Inputs :

                index : the position of the region as defined by the sampler
            """

            return self.list_coordinates[index]

        def __len__(self):
            """returns the number of regions to be generated by the sampler"""
            return len(self.list_coordinates)

        def __repr__(self):
            """Returns the list region coordinates generated by the sampler"""
            return str(self.list_coordinates)
        
    ############################################################################################################################

    class RandomSampler():
        """ A class to handle the generation of several sub-regions of a slide 

        Attributes :

            levelSlide : obj, the LevelSlide object to which the sampler is attached
            size : 2-tuple, dimensions of each generated region
            ROI : int, selects only the region which interset ROI of the specified class. Set to 0 for no selection (all regions are                 taken into account)
            balance : float, between 0 and 1, the ratio of positive samples in the dataset
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            include_gray_matter : bool. If True, only regions in gray matter will be sampled; Defualt : True.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.


        Methods :

            Built-in :
                __init__ : initialises the class
                __getitem__ : gets a region as defined by the sampler
                __len__ : returns the number of regions to be generated by the sampler
                __repr__ : returns the list region coordinates generated by the sampler

            Backend :
                _positive_sampler : sampler for generating positive examples
                _negative_sampler : sampler for generating negative examples
                _set_sampler : associates _positive_sampler and _negative_sampler to make an overall sampler
        """

        def __init__(self, levelSlide, size, ROI = 2, balance = 0.5, tolerance = None, include_gray_matter = True, random_pos = True):

            """initialises the class

            Inputs :

                levelSlide : obj, the LevelSlide object to which the sampler is attached
                size : 2-tuple, dimensions of each generated region
                ROI : int, selects only the region which interset ROI of the specified class. Set to 0 for no selection (all regions are                 taken into account)
                balance : float, between 0 and 1, the ratio of positive samples in the dataset
                tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
                include_gray_matter : bool. If True, only regions in gray matter will be sampled; Defualt : True.
                random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.

            """

            self.levelSlide = levelSlide

            if type(size) is int :
                self.size = (size, size)
            else :
                self.size = size

            if tolerance is None :
                self.tolerance = size
            elif type(size) is int :
                self.tolerance = (tolerance, tolerance)
            else :
                self.tolerance = tolerance

            self.random_pos = random_pos

            self.ROI = ROI

            self.balance = balance

            self.include_gray_matter = include_gray_matter

            self._set_sampler()

        def _positive_sampler(self):

            """Creates a list containing all the positions of the regions containing positive samples"""

            #generate a list of all rois under the Polygon format, easy to process with
            self.list_ROI = [Polygon([(point[0], point[1]) for point in coord]) for _,coord in filter(lambda x: x[0] == self.ROI, self.levelSlide.labels)]


            #compute the centroids of each roi
            list_roi_positions = [self.levelSlide.meter_to_pixel(*roi.centroid.coords[:][0]) for roi in self.list_ROI]

            if not self.random_pos :
                #retrieves the coordinates of all regions corresponding to the positive samples
                self.list_coordinates_positives = [(x-self.size[0]//2, y-self.size[1]//2, x+self.size[0]//2, y+self.size[1]//2) for x,y in list_roi_positions]

            #random pos is True, places the ROI at a random position on the region
            else :
                list_roi_bounds = [self.levelSlide.meter_to_pixel(*roi.bounds) for roi in self.list_ROI]

                #defines a rectangle in which the centroid of the region can be chosen while keeping the whole ROI in it
                random_squares = [(max(x1,x2) - self.size[0]//2, max(y1,y2) - self.size[1]//2, min(x1,x2) + self.size[0]//2, min(y1,y2) + self.size[1]//2) for x1,y1,x2,y2 in list_roi_bounds]

                #make a random sample within each rectangle
                list_roi_positions = list()
                for square in random_squares :
                    list_roi_x = np.random.randint(square[0], square[2], size = 1)
                    list_roi_y = np.random.randint(square[1], square[3], size = 1)
                    list_roi_positions += list(zip(list_roi_x, list_roi_y))

                #retrieves the regions boundaries of the randomly generated samples
                self.list_coordinates_positives = [(x-self.size[0]//2, y-self.size[1]//2, x+self.size[0]//2, y+self.size[1]//2) for x,y in list_roi_positions]

        def _negative_sampler(self, num_random_samples = None, nb_iterations = 40):

            """Randomly samples negative samples, with flexible definition of negative : it can be rid of any ROI, but the user can choose to have partly ROIs via the parameter tolerance
            The algorithm iteratively randomly selects num_random_samples regions and keeps the one which corresponds to the definition, up to having same num_neg_samples negative samples, or having performed nb_iterations iterations. In the second case, the remaining samples are just given by copying already retrieved ones.

            Inputs :
                num_random_samples : int, number of negative sampels to be generated.
                nb_iterations : int, maximum number of iterations before the sampler stop trying to get negative samples.
            """

            num_neg_samples = int(len(self.list_coordinates_positives)*(1-self.balance)/self.balance)

            #creates a union of all rois to be avoided
            union_roi = union(self.list_ROI)

            #creates a polygon corresponding to the whole gray matter
            if self.include_gray_matter :
                union_gray_matter = union([Polygon([(point[0], point[1]) for point in coord]) for _,coord in filter(lambda x: x[0] == 1, self.levelSlide.labels)])

            #defines the number of random samples to be generated at each iteration
            num_random_samples = num_neg_samples if num_random_samples is None else num_random_samples

            self.list_coordinates_negatives = list()
            break_loop = False
            for it in tqdm(range(nb_iterations)):

                if break_loop :
                    break

                #creates num_random_samples random samples of positions on the slide
                bounds = self.levelSlide.pixel_to_meter(self.size[0]//2, self.size[1]//2, self.levelSlide.dims[0] - self.size[0]//2, self.levelSlide.dims[1] - self.size[1]//2)
                bounds = [min(bounds[0],bounds[2]),min(bounds[1],bounds[3]),max(bounds[0],bounds[2]),max(bounds[1],bounds[3])]
                x = (bounds[2] - bounds[0])*np.random.random((num_random_samples,)) + bounds[0]
                y = (bounds[3] - bounds[1])*np.random.random((num_random_samples,)) + bounds[1]
                random_positions = np.vstack((x, y)).T

                #only keeps positions in gray matter         
                if self.include_gray_matter :
                    random_positions = np.array([[x,y] for x,y in random_positions if Point(x,y).within(union_gray_matter)])

                #computes the coordinates of the corresponding region
                random_positions_pixel = [self.levelSlide.meter_to_pixel(x,y) for x,y in random_positions]
                random_regions = [(x-self.size[0]//2, y-self.size[1]//2, x+self.size[0]//2, y+self.size[1]//2) for x,y in random_positions_pixel]

                #computes the coordinates of the tolerance zone
                random_regions_tolerance_zone = [self.levelSlide.pixel_to_meter(*(x-self.tolerance[0]//2, y-self.tolerance[1]//2, x+self.tolerance[0]//2, y+self.tolerance[1]//2)) for x,y in random_positions_pixel]

                #appends valid regions to the dataset
                for i, region in enumerate(random_regions):

                    #checks if there is no roi in the tolerance zone
                    x1,y1,x2,y2 = random_regions_tolerance_zone[i]
                    if not Polygon([(x1,y1),(x1,y2),(x2,y2),(x2,y1)]).intersects(union_roi):
                        self.list_coordinates_negatives.append(region)

                    #checks if the list of samples has its expected size
                    if len(self.list_coordinates_negatives) >= num_neg_samples :
                        break_loop = True
                        break

            if len(self.list_coordinates_negatives) < num_neg_samples :
                self.list_coordinates_negatives += self.list_coordinates_negatives[:(num_neg_samples - len(self.list_coordinates_negatives))]

        def _set_sampler(self, nb_iterations = 40):

            """Associates _positive_sampler and _negative_sampler to make an overall sampler

            Inputs :
                nb_iterations : int, maximum number of iterations before the negative sampler stop trying to get negative samples.
            """

            self._positive_sampler()
            self._negative_sampler(nb_iterations = nb_iterations)
            self.list_coordinates = self.list_coordinates_positives + self.list_coordinates_negatives
            random.shuffle(self.list_coordinates)

        def __getitem__(self, index):

            """Gets the region with input index

            Inputs :

                index : the position of the region as defined by the sampler
            """

            return self.list_coordinates[index]

        def __len__(self):
            """returns the number of regions to be generated by the sampler"""
            return len(self.list_coordinates)

        def __repr__(self):
            """returns the list region coordinates generated by the sampler"""
            return str(self.list_coordinates)
    
#################################################################################################################################
