#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import torch
import os
#import torch.nn.functional as F
from torch import nn,Tensor,LongTensor
from torch.autograd import Variable
import numpy as np
import cv2
#import matplotlib.pyplot as plt
from tqdm import tqdm
from library.utils import SaveFiles as sf
#from library.DataSetSlides import DataSetSlides

def train_classifier(model, train_loader, test_loader, path_save, criterion=torch.nn.CrossEntropyLoss(), optimizer=torch.optim.Adam, lr=0.01, betas=(0.9, 0.999), epochs = 10):
    
    """This function is designed to train a neural network designed for classification of object on regions of the whole slides images
    
    Inputs :
    
        model : has to be an instance of torch.nn.Module, is the model to be trained. It can be raw or pretrained.
        train_loader : data to be passed as examples to the model to be trained. Has to be an instance of torch.Dataloader
        test_loader : data to be passed as examples to the model have it performances tested at each epoch. Has to be an instance of torch.Dataloader
        path_save : str, the path where the model and its performances along epochs has to be stored
        criterion : instance of torch.nn.Module, is the criterion/loss along which the model is to be trained. Default : torch.nn.CrossEntropyLoss
        optimizer : instance of torch.optim, is the optimizer with which the model is to be trained. Default : torch.optim.Adam
        lr : float, learning rate of the optimizer. Default : 0.01.
        betas : tuple of floats, momentums of the optimizer. Default : (0.9, 0.99).
        epochs : int, number of epochs to be passed to the model when training. Default : 10.
    
    """
    print('Starting point.')
    
    # displays availability of GPU
    cuda = True if torch.cuda.is_available() else False
    print("cuda %s"%(cuda))  
    
    #optimizer is chosen according to inputs parameters
    optimizer = optimizer(model.parameters(), lr=lr, betas=betas)
    
    #initializes directories where to store models and performances if necessary
    try :
        os.mkdir(path_save)
    except :
        pass
    try :
        os.mkdir(path_save + '/classifiers')
    except :
        pass
    
    #initializes the trainer
    trainer = Trainer(model, path_save, criterion, optimizer)
    
    #initializes the list of performances of the models along epochs
    list_loss_train = list()
    list_loss_test = list()
    list_f1_train = list()
    list_f1_test = list()
    
    print('Performing first evaluation of loss.')
    
    #computes performances of the model before training to have ground basis for it to be compared once trained
    list_loss_train.append(trainer.mean_loss(train_loader)) #performances on loss
    list_loss_test.append(trainer.mean_loss(test_loader))
    
    print('Performing first evaluation of F1-score.')
    
    list_f1_train.append(max(trainer.test(train_loader)[2]))#performances on f1-score
    list_f1_test.append(max(trainer.test(test_loader)[2]))
    
    #passes the dataset through the network for a determined number of epochs
    for epoch in range(epochs):
        
        #displays progression of the training
        print('\n'.join(('Epoch : {}'.format(epoch+1), 'Best loss on train: {}, Best loss on val: {}, Best F1 on val: {}'.format(min(list_loss_train),min(list_loss_test), max(list_f1_test)))))
        
        #performs training and evaluation of the loss on train set and validation set
        list_loss_train.append(trainer.train(train_loader)) #performances on loss + train
        list_loss_test.append(trainer.mean_loss(test_loader))
        
        #performs evaluation of the f1-score on train set and validation set
        list_f1_train.append(max(trainer.test(train_loader)[2]))#performances on f1-score
        list_f1_test.append(max(trainer.test(test_loader)[2]))
        
        #displaying model performances at epoch
        print('Epoch: {}, Loss on train: {}, Loss on val: {}, F1 on val: {}'.format(epoch,list_loss_train[-1], list_loss_test[-1], list_f1_test[-1]))
        
        #saving model at concerned epoch
        torch.save(trainer.model, path_save + '/classifiers' + '/cnn_epoch={}_batch={}_loss_{}_dropconv_{}_dropfully_{}.pt'.format(epoch, train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_fully))
        
        #saving the losses computed from the beginning (on train and validation)
        sf.save(path_save + '/classifiers' + '/loss_train_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_fully), list_loss_train)
        sf.save(path_save + '/classifiers' + '/loss_valid_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_fully), list_loss_test)
        
        #saving the f1 scores computed from the beginning (on train and validation)
        sf.save(path_save + '/classifiers' + '/f1_train_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_fully), list_f1_train)
        sf.save(path_save + '/classifiers' + '/f1_valid_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_fully), list_f1_test)
        
    return list_loss_train, list_f1_train, list_loss_test, list_f1_test

def train_segmenter(model, train_loader, test_loader, path_save, criterion=torch.nn.CrossEntropyLoss(), optimizer=torch.optim.Adam, lr=0.01, betas=(0.9, 0.999), epochs = 10):
    
    """This function is designed to train a neural network designed for segmentation of object on regions of the whole slides images
    
    Inputs :
    
        model : has to be an instance of torch.nn.Module, is the model to be trained. It can be raw or pretrained.
        train_loader : data to be passed as examples to the model to be trained. Has to be an instance of torch.Dataloader
        test_loader : data to be passed as examples to the model have it performances tested at each epoch. Has to be an instance of torch.Dataloader
        path_save : str, the path where the model and its performances along epochs has to be stored
        criterion : instance of torch.nn.Module, is the criterion/loss along which the model is to be trained. Default : torch.nn.CrossEntropyLoss
        optimizer : instance of torch.optim, is the optimizer with which the model is to be trained. Default : torch.optim.Adam
        lr : float, learning rate of the optimizer. Default : 0.01.
        betas : tuple of floats, momentums of the optimizer. Default : (0.9, 0.99).
        epochs : int, number of epochs to be passed to the model when training. Default : 10.
    
    """
    
    print('Starting point.')
    
    # displays availability of GPU
    cuda = True if torch.cuda.is_available() else False
    print("cuda %s"%(cuda))  
    
    #optimizer is chosen according to inputs parameters
    optimizer = optimizer(model.parameters(), lr=lr, betas=betas)
    
    #initializes directories where to store models and performances if necessary
    try :
        os.mkdir(path_save)
    except :
        pass
    try :
        os.mkdir(path_save + '/unets')
    except :
        pass
    
    #initializes the trainer
    trainer = Trainer(model, path_save, criterion, optimizer)
    
    #initializes the list of performances of the models along epochs
    list_loss_train = list()
    list_loss_test = list()
    
    print('Performing first evaluation of loss.')
    
    #computes performances of the model before training to have ground basis for it to be compared once trained
    list_loss_train.append(trainer.mean_loss(train_loader)) #performances on loss
    list_loss_test.append(trainer.mean_loss(test_loader))
    
    for epoch in range(epochs):
        
        #displays progression of the training
        print('\n'.join(('Epoch : {}'.format(epoch+1), 'Best loss on train: {}, Best loss on val: {}'.format(min(list_loss_train),min(list_loss_test)))))
        
        #performs training and evaluation of the loss on train set and validation set
        list_loss_train.append(trainer.train(train_loader)) #performances on loss + train
        list_loss_test.append(trainer.mean_loss(test_loader))
        
        #displaying model performances at epoch
        print('Epoch: {}, Loss on train: {}, Loss on val: {}'.format(epoch,list_loss_train[-1], list_loss_test[-1]))
        
        #saving model at concerned epoch
        torch.save(trainer.model, path_save + '/unets' + '/unet_epoch={}_batch={}_loss_{}_dropconv_{}_dropupconv_{}.pt'.format(epoch, train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv))
        
        #saving the losses computed from the beginning (on train and validation)
        sf.save(path_save + '/unets' + '/loss_train_batch_{}_loss_{}_dropconv_{}_dropupconv_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv), list_loss_train)
        sf.save(path_save + '/unets' + '/loss_valid_batch_{}_loss_{}_dropconv_{}_dropupconv_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv), list_loss_test)
        
    return list_loss_train,list_loss_test

def test_real_implementation(data_set, model, path_save):

    output_data = list()
    labels_data = list()
    coords_data = list()
    with torch.no_grad():
        for i, sample in enumerate(data_set):
            with open('../log/'+ path_save.split('/')[-1]+'_real_implementation', 'w') as file:
                file.write('Fraction of the data already generated : ' + str(i) + '/' + str(len(data_set)))
            
            #puts sample in right format
            images, labels = Util.shape_sample(sample)
            coords = sample['Coords']
            
            #performs forward pass
            outputs = Util.forward_pass(images, model)
            
            for out in output :
                output_data.append(float(out.cpu()[1]))
            labels_data.append(float(label.cpu()))
            coords_data.append(coords)
    return output_data, labels_data, coords_data

def display_results(path_model, path_data, path_outputs, path_labels, cutoff=0.5, nb_disp = 16, type_res = 'tp'):
    
    results = sf.load(path_outputs)
    labels = sf.load(path_labels)
    data_set = sf.load(path_data + '/Dataset.p')
    data_set.load = True
    data_set.set_path(path_data)
    
    if type_res == 'tp':
        to_disp = np.argwhere((np.array(results)>= cutoff)*labels)[:nb_disp]
    elif type_res == 'fp':
        to_disp = np.argwhere((np.array(results)>= cutoff)*(1-np.array(labels)))[:nb_disp]
    else :
        to_disp = np.argwhere((1-(np.array(results)>= cutoff))*labels)[:nb_disp]
        
    import matplotlib.pyplot as plt
    size = int(np.ceil((np.sqrt(nb_disp))))
    _, ax = plt.subplots(size, size, figsize = (15,15))
    
    for i in range(size):
        for j in range(size):
            sample = data_set[to_disp.flatten()[size*i + j]]
            image = np.array(sample['Images'])
            mask = np.array(sample['Labels'])

            ax[i, j].imshow(image)
            try :
                ax[i, j].contour(mask, [(np.min(mask)+np.max(mask))/2])
            except :
                pass
            
class Util():
    
    """A class containg useful functions when training
    
    Methods :
        Special :
            forward_pass : 
            shape_sample : 
        
    """
    
    @classmethod
    def forward_pass(self, images, model, probability = False):
        
        #reshaping image in the right size if necessary
        if len(images.shape) < 4:
            images = images.reshape(1, *images.shape).type(Tensor)

        #computes forward pass
        outputs = model(images.permute(0,3,1,2))
        
        #if a probability is wanted, ensures to give it
        outputs = nn.Softmax(dim = -1)(outputs) if probability else outputs
        
        return outputs

    @classmethod
    def shape_sample(self, sample):
        # checks if GPU is available
        cuda = True if torch.cuda.is_available() else False

        # Input tensors are given a suitable type, and put on the GPU if possible
        Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor 
        LongTensor = torch.cuda.LongTensor if cuda else torch.LongTensor
        
        #giving the right type to the sample
        images = Variable(sample["Images"].type(Tensor))
        labels = Variable(sample["Labels"].type(LongTensor))
        
        return images, labels

class Trainer():
    
    """A class to train a deep learning model
    
    Attributes :
        model : torch.nn.Module and instances, deep learning model to be trained/evaluated
        path_save : str, path where to save the trained/evaluated model
        criterion : torch.nn.Module and instances, the loss on which the model is to be trained
        optimizer : torch.optim, optimizer used to train the model
        
    Methods :
        Built-in :
            __init__ : initialises the class
        
        Special :
            train : Training the model on a hole dataset, for 1 epoch
            test : Computes performances of the model on input dataset
            mean_loss : Computes mean loss of the model on input dataset
            
    """
    
    def __init__(self, model, path_save, criterion, optimizer):
        
        """initialises the class
        
        Inputs :
            model : torch.nn.Module and instances, deep learning model to be trained/evaluated
            path_save : str, path where to save the trained/evaluated model
            criterion : torch.nn.Module and instances, the loss on which the model is to be trained
            optimizer : torch.optim, optimizer used to train the model
        
        """
        
        cuda = True if torch.cuda.is_available() else False
        
        self.model = model.cuda() if cuda else model.cpu()
        self.path_save = path_save
        self.criterion = criterion.cuda() if cuda else criterion.cpu()
        self.optimizer = optimizer
        
    def train(self, data_loader):
        """Training the model on a hole dataset, for 1 epoch
        
        Inputs :
            data_loader : torch DataLoader object, dataset on which the loss is to be trained
        """
        
        print('Performing training of the model on the overall dataset')
        
        #puts model in training mode
        self.model.train()
        
        #list of losses to be computed
        losses = list()
        
        #passes all data through the model
        for i,batch in enumerate(data_loader) :
            
            print("Batch n°{}/{}".format(i, len(data_loader)))
            
            #preparing inputs
            images, labels = Util.shape_sample(batch)
           
            #zeroes gradients in the optimizer
            self.optimizer.zero_grad()
            
            #computes forward pass
            outputs = Util.forward_pass(images, self.model)
            
            #computing loss
            loss = self.criterion(outputs, labels)
            
            #memorises the loss updates
            losses.append(loss.cpu().detach())
            
            #backward pass
            loss.backward()
            self.optimizer.step()
            
            #clears memory
            torch.cuda.empty_cache()
            
        return sum(losses)/len(losses)
        
    def test(self, data_loader, nb_thresh = 20):
        
        """Computes performances of the model on input dataset
        
        Inputs :
            data_loader : torch DataLoader object, dataset on which the loss is to be evaluated 
            nb_thresh : int, number of decision cutoffs on which the model is to be evaluated, linearly spanned between 0 and 1
        
        """
        
        print('Performing the computation of performances on the overall dataset')
        
        #puts model in evaluation mode
        self.model.eval()
        
        threshs = np.linspace(0,1,nb_thresh)
        
        #initializes true positives, false positives and false negatives counters
        true_pos = np.zeros(nb_thresh)
        false_pos = np.zeros(nb_thresh)
        false_neg = np.zeros(nb_thresh)
        
        #prevents any gradient to be computed
        with torch.no_grad():
            
            #performs evaluation across all the dataset
            for i, batch in enumerate(data_loader):

                print("Batch n°{}/{}".format(i, len(data_loader)))
                
                #preparing inputs
                images, labels = Util.shape_sample(batch)

                #computes forward pass
                outputs = Util.forward_pass(images, self.model, probability = True)

                for j, (output,label) in enumerate(zip(outputs,labels)):
                    
                    #puts output and label in right type to be computed properly
                    output = np.array(output.cpu().detach())
                    label = np.array(label.cpu().detach())
                
                #compares outputs to several thresholds to assess predicitions
                out_th = (threshs <= output[1]).astype(int)

                #update number of true positives, false positives and false negatives
                true_pos += out_th*label
                false_pos += out_th*(1-label)
                false_neg += (1-out_th)*label

        #computes performance indicators
        precision = true_pos/(true_pos + false_pos)
        recall = true_pos/(true_pos + false_neg)
        f1 = 2*precision*recall / (precision + recall)
        
        return precision, recall, f1
        
    def mean_loss(self, data_loader):
        
        """Computes the mean loss on the overall dataset
        
        Inputs :
        
            data_loader : torch DataLoader object, dataset on which the loss is to be evaluated 
        """
        
        
        print('Performing the computation of the average loss on the overall dataset')

        #puts model in evaluation mode
        self.model = self.model.eval()
        
        #prevents any gradient to be computed
        with torch.no_grad():

            #list of losses to be computed
            losses = list()
            for i,batch in enumerate(data_loader) :
                
                print("Batch n°{}/{}".format(i, len(data_loader)))
                
                #preparing inputs
                images, labels = Util.shape_sample(batch)
                
                #computes forward pass
                outputs = Util.forward_pass(images, self.model)

                #computing loss
                loss = self.criterion(outputs, labels)

                #memorises the loss updates
                losses.append(loss.cpu().detach())

                #clears memory
                torch.cuda.empty_cache()
               
        #returns average loss
        return sum(losses)/len(losses)
