#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 30 14:30:59 2020

@author: valentinabadie
"""

import numpy as np
import cv2
from scipy.ndimage import gaussian_filter
import scipy.io as io
from library.utils import SaveFiles as sf



class ManualColorLabellization():
    
    """A class to easily labellize pixels of different colors on an image. Only brown and blue colors are to be labellized, since the rest in done by substraction
    
    Attributes :
        image : array, the image to manually labellize
        image_hue : array, the hue component of the image
        image_grayscale : array, the grayscale component of the image
        
        hue_thresholds_brown, hue_thresholds_blue : list of 2-tuple, which are the bounds of segements of hue value we want to include as brown/blue pixels
        grayscale_thresholds_brown, grayscale_thresholds_blue : list of 2-tuple, which are the bounds of segements of grayscale value we want to include as brown/blue pixels
        
    Methods :
        
        Built-in : 
            __init__ : initializes the class
            
        Special :
            display_histograms : displays the histograms of hue and grayscale value to help the user to set hue and grayscale thresholds
            set_hue : a function to set easily hue thresholds
            set_grayscale : a function to set easily grayscale thresholds
    """
    
    def __init__(self, image):
        
        """initializes the class
        
        Inputs: 
            image : Region object, the image to manually labellize
        """
        self.id = image.id
        self.magnification = image.magnification
        self.coords = image.coordinates
        
        self.image = image.array_region
        self.image_hue = cv2.cvtColor(self.image.astype(np.uint8)[:,:,:3],cv2.COLOR_BGR2HSV)[:,:,0]
        self.image_grayscale = np.mean(self.image[:,:,:3], 2)
        
        self.hue_thresholds_brown = [[0,0]]
        self.grayscale_thresholds_brown = [[0,0]]
        
        self.hue_thresholds_blue = [[0,0]]
        self.grayscale_thresholds_blue = [[0,0]]
        
    def display_histograms(self, bins = 200):
        
        """displays the histograms of hue and grayscale value to help the user to set hue and grayscale thresholds"""
        
        import matplotlib.pyplot as plt
        _, ax = plt.subplots(2, 2, figsize = (10,10))
        
        #displays histograms of hue value
        histo_h, h_values,_ = ax[0,0].hist(self.image_hue.flatten(), bins = bins)
        ax[0,0].set_xlabel('Hue value')
        ax[0,0].set_ylabel('Histogram hight')
        ax[0,0].set_title('Histogram of hue value')
        h_values = h_values[:-1]
        ax[1,0].plot(h_values, gaussian_filter(histo_h, 4, order = 0, mode = 'wrap'))
        ax[1,0].set_xlabel('Hue value')
        ax[1,0].set_ylabel('Histogram hight')
        ax[1,0].set_title('Smoothed histogram of hue value')
        
        #displays histograms of hue value
        histo_g, g_values,_ = ax[0,1].hist(self.image_grayscale.flatten(), bins = bins)
        ax[0,1].set_xlabel('Gray scale value')
        ax[0,1].set_ylabel('Histogram hight')
        ax[0,1].set_title('Histogram of gray scale value')
        g_values = g_values[:-1]
        ax[1,1].plot(g_values, gaussian_filter(histo_g, 4, order = 0, mode = 'wrap'))
        ax[1,1].set_xlabel('Gray scale value')
        ax[1,1].set_ylabel('Histogram hight')
        ax[1,1].set_title('Smoothed histogram of gray scale value')
        
    def set_hue(self, *thresholds, color = 'brown', display = False):
        
        """a function to set easily hue thresholds
        
        Inputs :
            thresholds : 2-tuples, defines the segments of hue value to be set as labels
            color : str, 'blue' or 'brown', the stain for which the given thresholds have to act
        """
        
        setattr(self, 'hue_thresholds_' + color, thresholds)
        
        #displays the contours corresponding to each boundary
        if display :
            import matplotlib.pyplot as plt
            plt.contour(self.image_hue,  levels=np.array(getattr(self, 'hue_thresholds_' + color)).flatten().tolist())
            plt.imshow(self.image)
        
    def set_grayscale(self, *thresholds, color = 'brown', display = False):
    
        """a function to set easily grayscale thresholds
        
        Inputs :
            thresholds : 2-tuples, defines the segments of grayscale value to be set as labels
            color : str, 'blue' or 'brown', the stain for which the given thresholds have to act
        """
    
        setattr(self, 'grayscale_thresholds_' + color, thresholds)
        
        #displays the contours corresponding to each boundary
        if display :
            import matplotlib.pyplot as plt
            plt.contour(self.image_grayscale, levels=np.array(getattr(self, 'grayscale_thresholds_' + color)).flatten().tolist())
            plt.imshow(self.image)
        
        #self.display
        
    def display_labels(self):
        
        """this function displays the segmentation due to pixel color labellization"""
        
        self.compute_mask()
        
        import matplotlib.pyplot as plt
        plt.contour(self.mask,  levels = [0.5,1.5], colors = ['red', 'blue'])
        plt.imshow(self.image)
    
    def compute_mask(self):
        
        """Computes the mask corresponding to the thresholds"""
        
        self.mask = np.zeros(self.image.shape[:2])
        
        #for every color, retrieves which pixels have been manually classified
        for index, color in enumerate(['brown','blue']):
            
            #retrieves pixels selected by their hue
            valid_hue = np.sum(np.dstack([(self.image_hue > threshold[0]) * (self.image_hue < threshold[1]) for threshold in getattr(self, 'hue_thresholds_' + color)]), 2).astype(bool)
            
            #retrieves pixels selected by their grayscale
            valid_grayscale = np.sum(np.dstack([(self.image_grayscale > threshold[0]) * (self.image_grayscale < threshold[1]) for threshold in getattr(self, 'grayscale_thresholds_' + color)]), 2).astype(bool)
            
            valid_pixels = (valid_hue * valid_grayscale).astype(bool)

            self.mask[valid_pixels] = index + 1 #valid_pixels[valid_pixels].astype(float)#*(index+1)
            
        self.mask = cv2.dilate(self.mask.astype(np.uint8),cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(7,7)),iterations = 1)
        
        self.mask = cv2.erode(self.mask.astype(np.uint8),cv2.getStructuringElement(cv2.MORPH_ELLIPSE,(3,3)),iterations = 2)
            
    def save(self, path):
        """Saves the manually labelled image at in the folder set as path
        
        Inputs :
            path : str, path to the folder where to store the manually labelled images
        """
        
        self.compute_mask()
        io.savemat('/'.join((path,'manually_labelled_{}_{}_{}_{}_{}_{}.mat'.format(self.id,self.magnification, *self.coords))), {'sample' : {'image' : self.image, 'label': self.mask}})
                     
class MakeClassifier():
                     
    """A class to train and save the color classifier
    
    Attributes :
        path : str, path to the folder where training data is stored
        name : str, name of the classifier file to be generated (with .mat)
        
    Methods :
        Built-in : 
            __init__ : initialises the class
            
        Special :
            train : the method to train the classifier
    """
                     
                     
    def __init__(self, path, name):
        
        """initialises the class
        
        Inputs :
            path : str, path to the folder where training data is stored
            name : str, name of the classifier file to be generated (with .mat)
        
        """
        
        self.path = path
        self.name = name
        
    def train(self, eng, matlab = None):
        """trains the classifier
        
        Inputs :
            eng : matlab.engine object, the MATLAB environment on which the training is to be performed
            matlab : matlab object, the matlab-python interface
        """
        
        eng.trainingClassifier(self.path, self.name)
        