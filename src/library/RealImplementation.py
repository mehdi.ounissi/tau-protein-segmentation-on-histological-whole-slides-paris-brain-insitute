#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import cv2
import numpy as np
#import matplotlib.pyplot as plt
from tqdm import tqdm
from shapely.geometry import Polygon
from shapely.ops import cascaded_union as union
from library.Training import Util
import torch
from torch import nn
#import matplotlib.pyplot as plt
from library.Morphology import MorphologyStudy
from library.SlidesHandling import DataSetSlides

class ClassificationImplementation():
    
    """This class is used to compute the Focal Loss
    
    Attributes :
    
        predictions : list of float, the predicted probabilies of belonging to a given label
        labels : list of int, the classes that should have been predicted
        coords : list of 4-tuples, containing the coordinates of the regions being evaluated
        threshs : list of float, the list of cutoffs value for which the model is to be evaluated
        way : way of evaluating the performances of the model. If 0, count naively the true/false positives/negatives, if 1, computes consecutive positively predicted components as one prediction, and evaluates model on them.
        predictions_th : list of bool, the predictions compared to every decision cutoff
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
            __repr__ : displays f1 score, precision and recall, for best f1_score achieved
            __len__ : number of predictions passed as input to compute the scores
        
        Special :
            disp : displays visually the hole analysis of the scores
            classification : performs detection of objects of interest
            
        Backend :
            _count : counts the number of true positives, false postitives and false negatives
            _f1_score : computes the f1 score, precision and recall
            
    """
    
    def __init__(self, dataset, model, labels=None, coords=None, nb_points=20, way = 0):
        
        """initialises the class
        
        Inputs : 
            dataset : DataSetGenerator object or intsnace, the dataset on which to perform detection of objects of interest
            labels : list of int, the classes that should have been predicted
            coords : list of 4-tuples, containing the coordinates of the regions being evaluated
            nb_points : int, the number of cutoff to be computed linearly
            way : way of evaluating the performances of the model. If 0, count naively the true/false positives/negatives, if 1, computes consecutive positively predicted components as one prediction, and evaluates model on them.
        
        """
        
        self.dataset = dataset
        self.model = model
        self.threshs = np.linspace(0, 1, nb_points)
        self.way = way

        self.classification()

        th, pred = np.meshgrid(self.threshs, self.predictions)
        
        self.predictions_th = (th <= pred).astype(int)
        self._count()
        self._f1_score()
         
    def classification(self):
        
        """computes the output of the model for every sample of the dataset"""

        self.predictions = list()
        self.labels = list()
        self.coords = list()
        with torch.no_grad():
            for i, sample in enumerate(self.dataset):
                #with open('../log/'+ path_save.split('/')[-1]+'_real_implementation', 'w') as file:
                #    file.write('Fraction of the data already generated : ' + str(i) + '/' + str(len(data_set)))

                #puts sample in right format
                images, labels = Util.shape_sample(sample)
                #coords = sample['Coords']

                #performs forward pass
                outputs = Util.forward_pass(images, self.model)
                
                for out in outputs :
                    self.predictions.append(float(out.cpu()[1]))
                self.labels.append(float(labels.cpu()))
                 
                #self.coords.append(coords)

    def _count(self):
        
        """counts the number of true positives, false postitives and false negatives"""
        
        #makes a 2D grid with label, and the different decision cutoff on which to evaluate the model
        _, labs = np.meshgrid(self.threshs, self.labels)
        
        #way 0, naive computation of true/false positives/negatives
        if self.way == 0:
            self.true_positives_p = np.sum(self.predictions_th*labs, 0) #true positives used to compute precision
            self.true_positives_r = self.true_positives_p #true positives used to compute recall (here the definition is the same)
            self.false_positives = np.sum(self.predictions_th*(1-labs), 0)
            self.false_negatives = np.sum((1-self.predictions_th)*labs, 0)
            
        #way 1
        else :
            
            #initialising true/false positives/negatives counters
            self.true_positives_r = np.zeros(len(self.threshs))
            self.false_negatives = np.zeros(len(self.threshs))
            self.true_positives_p = np.zeros(len(self.threshs))
            self.false_positives = np.zeros(len(self.threshs))
            
            #makes a list of connected component among labels
            polygon_labels = list()
            for i, (label, coord) in enumerate(zip(self.labels,self.coords)):
                if label == 1 :
                    x1, x2, y1, y2 = coord
                    polygon = Polygon([(x1,y1),(x1,y2), (x2,y2), (x2,y1)])
                    polygon_labels.append(polygon)
            polygon_tot_l = union(polygon_labels)
            
            #going through thresholds
            for i, th in tqdm(enumerate(self.threshs)):
                
                #
                args_positives = self.predictions_th[:,i].astype(bool).flatten()
                coords_true = np.array(self.coords)[args_positives]
                
                #makes a list of connected components among predictions
                polygon_predictions = list()
                for x1,y1,x2,y2 in coords_true:
                    polygon_predictions.append(Polygon(((x1,y1),(x2,y1),(x2,y2),(x1,y2))))
                polygon_tot_p = union(polygon_predictions)  
                
                #for each connected component in labels, checks if it intersects  any of the predictions
                for poly in polygon_labels:
                    if poly.intersects(polygon_tot_p): #if yes, counts as true positive for recall computation
                        self.true_positives_r[i] +=1
                    else :
                        self.false_negatives[i] += 1 #else, false negative
                        
                #for each connected component in predictions, checks if it intersects  any of the labels
                for poly in polygon_predictions:
                    if poly.intersects(polygon_tot_l): #if yes, counts as true positive for prediction computation
                        self.true_positives_p[i] +=1
                    else :
                        self.false_positives[i] += 1 #else, false positive
            
    def _f1_score(self):
        
        """computes the f1 score, precision and recall"""
        
        self.precision = self.true_positives_p / (self.true_positives_p + self.false_positives)
        self.recall = self.true_positives_r / (self.true_positives_r + self.false_negatives)
        self.f1_score = 2*self.precision*self.recall/(self.precision+self.recall)
        
    def __len__(self):
        """number of predictions passed as input to compute the scores"""
        return len(self.predictions)
    
    def __repr__(self):
        """displays f1 score, precision and recall, for best f1_score achieved"""
        arg_b = np.argmax(self.f1_score)
        return ''.join(('Precision : ', str(self.precision[arg_b]),', Recall : ', str(self.recall[arg_b]),', F1 score : ', str(self.f1_score[arg_b]),'.'))
    
    def disp(self):
        """displays visually the hole analysis of the scores"""
        
        import matplotlib.pyplot as plt
        _, ax = plt.subplots(figsize = (8, 8))
        ax.plot(self.threshs, self.precision, color = 'blue', label = 'Precision', marker = 'o', linestyle = '-', linewidth = 3)
        ax.plot(self.threshs, self.recall, color = 'green', label = 'Recall', marker = 'o', linestyle = '-', linewidth = 3)
        ax.plot(self.threshs, self.f1_score, color = 'red', label = 'F1 score', marker = 'o', linestyle = '-', linewidth = 3)
        ax.grid()
        ax.set_xlabel('Decision cutoff')
        ax.set_ylabel('Metric')
        plt.legend()
        
class SegmentationImplementation():
    
    """This class is used to perform segmentation of each sample of the input dataset, and then to merge them by using the coordinates of thre processed regions to make them match
    
    Attributes :
    
        dataset : DataSetGenerator object or instance of it, input dataset for the model to be tested on it
        region_indices : list of int, indices of the region to select among the dataset (the are pre-selected by the classifier
        region_coords : list of 4-tuple, coordinates of the regions passed as input to the model
        model : nn.Module object, model to be tested
        labels : list of list of 2D tuples coordinates, corresponding to the contours of the ground-truth labels
        threshold : float, the decision cutoff to be applied to the probability predictions
        ROI : int, the ROI on which to evaluate segmentation
        size : int, size of the images in the dataset
        
        segmentations : list, list of all segmented objects (in meters)
        segmentations_union : geom object, union of all the segmented objects
        
        tp : int, number of true positive detected
        fp : int, number of false positive detected
        fn :int, number of false negative detected
        
        f1_score : float, f1 score performed by the model on the dataset
        precision : float, precision performed by the model on the dataset
        recall : float, recall performed by the model on the dataset
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            convert_to_meter : uses the boundaries of the region to compute the real-world segmentation positions
            segmentation : performs segmentation of objects of interest on every sample region, and merge them
            evaluation : computes scores on the performed segmentation
            display : displaying an example of result of segmentation
            display_several : displaying some examples of result of segmentation
            postprocessing : applying postprocessing based on the surface of the detected objects
    
    """
    
    def __init__(self, region_indices, region_coords, dataset, model, labels = None, threshold = 0.5, ROI = 2):
        
        """initialises the class
        
        Inputs :
            dataset : DataSetGenerator object or instance of it, input dataset for the model to be tested on it
            region_indices : list of int, indices of the region to select among the dataset (the are pre-selected by the classifier
            region_coords : list of 4-tuple, coordinates of the regions passed as input to the model
            model : nn.Module object, model to be tested
            labels : list of list of 2D tuples coordinates, corresponding to the contours of the ground-truth labels
            threshold : float, the decision cutoff to be applied to the probability predictions
            ROI : int, the ROI on which to evaluate segmentation

        """
        
        self.region_indices = region_indices
        self.region_coords = region_coords
        self.dataset = dataset
        self.model = model
        self.labels = labels
        self.threshold = threshold
        self.ROI = ROI
        
        self.size = max(dataset[0]['Images'].shape)
        
    def convert_to_meter(self, positions, coords):
        """uses the boundaries of the region to compute the real-world segmentation positions
        
        Inputs :
            positions : N-by-2 array, containing positions in terms of pixels
            coords : 4-tuple, real-world coordinates of the regions, given in meters
        """
        
        x = coords[0] + positions[:,0]/self.size *(coords[2] - coords[0])
        y = coords[1] + (self.size - positions[:,1])/self.size *(coords[3] - coords[1])
        
        return np.vstack((x,y))
    
    def segmentation(self):
        """performs the segmentation task on all the dataset"""
    
        list_predictions = list()
        for i,index in tqdm(enumerate(self.region_indices)): 

            sample = self.dataset[index]
            image = sample['Images'].type(torch.FloatTensor)
            
            #processes the sample to go through the neural network
            image = image.reshape([1] + list(image.shape)).permute(0,3,1,2)
            output = self.model(image).permute(0,2,3,1).detach().cpu()

            #computes the prediction be done on the sample
            prob_output = np.argmax(np.array(nn.Softmax(-1)(output)), -1)
            prediction = (prob_output>=self.threshold).astype(np.uint8).reshape(prob_output.shape[1:])

            if np.max(np.array(prediction))==0: #if nothing is retrieved on the current sample
                continue
            x = np.arange(max(image.shape))
            y = np.arange(max(image.shape))

            plt.ioff() #prevents matplotlib to display unuseful graphs
            contours = plt.contour(x,y,prediction, [0.5]).collections[0].get_paths() #retrieves the object contours on the image
            for contour in contours :
                if len(contour.vertices) >= 3: #some contours are simply lines, it is not relevant to use it
                    list_predictions.append(self.convert_to_meter(contour.vertices, self.region_coords[index])) #computes every contour to be found in the dataset, and make it match with real world measures

        #retrieves the contour of the image 
        self.segmentations_union = union([Polygon(list(zip(*coordinates))) for coordinates in list_predictions])
        self.segmentations = [np.array(list(geom.exterior.coords)) for geom in list(self.segmentations_union.geoms)]

    def evaluation(self):
        """performs evaluation of the dataset (precision, recall, f1-score)"""
        
        #computes union of all ground-truth objects
        labels = [coords for obj, coords in list(filter(lambda x : x[0] == self.ROI, self.labels))]
        labels_union = union([Polygon(list(zip(*coordinates.T))) for coordinates in labels])
        
        #computes intersection with this union
        pred_label_inter = self.segmentations_union.intersection(labels_union)

        #computes true/false pos/neg
        self.tp = sum([seg.intersects(labels_union) for seg in list(self.segmentations_union.geoms)]) 
        self.fp = len(self.segmentations_union) - self.tp
        self.fn = len(labels_union) - len(pred_label_inter)
        
        #computes scores
        self.precision = self.tp/(self.tp + self.fp)
        self.recall = self.tp/(self.tp + self.fn)
        self.f1_score = 2*self.precision*self.recall / (self.precision + self.recall)
        
    def display(self, index, slide = 'A1702114', magnification = 'x20'):
        
        """Displays a visual example of segmentation results
        
        
        Inputs :
            index : int, index of the segmented object to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        #retrieves the object to display
        polygon_to_disp = self.segmentations_union[index]
        
        #computes limits of the circumscribed rectangle of the object
        coords_region = list(polygon_to_disp.bounds)
        
        #extents borders of the regions for better visual results
        coords_region[0], coords_region[2] = (coords_region[0] + coords_region[2])/2 - 10, (coords_region[0] + coords_region[2])/2 + 10
        coords_region[1], coords_region[3] = (coords_region[1] + coords_region[3])/2 - 10, (coords_region[1] + coords_region[3])/2 + 10

        #retrieves the corresponding region
        region = DataSetSlides()[slide][magnification](*coords_region)
        
        fig, ax = plt.subplots(1, 2, figsize = (10,20))
        ax[0].imshow(region.array_region, extent = np.array(coords_region)[np.array([0,2,1,3])])
        ax[0].plot(*list(map(list, zip(*list(polygon_to_disp.exterior.coords)))))
        ax[1].imshow(region.array_region, extent = np.array(coords_region)[np.array([0,2,1,3])])
        for obj, coords in region.in_labels :
            if obj == self.ROI :
                ax[1].plot(coords[:,0], coords[:,1])
        plt.show()
        
    def display_several(self, indices, slide = 'A1702114', magnification = 'x20'):
        
        """Displays several example of results
        
        Inputs :
            indices : list of int, indices of the segmented objects to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        for index in indices :
            self.display(index, slide = slide, magnification = magnification)
            
    def postprocessing(self, threshold = 0.1, slide = 'A1702114', magnification = 'x20'):
        
        """Displays several example of results
        
        Inputs :
            indices : list of int, indices of the segmented objects to be displayed
            slide : int/str, slide key or index
            magnification : int/str, magnification level at which example is going to be displayed
        """
        
        #computes the surface of each retrieved object
        morphology = MorphologyStudy(self.segmentations, DataSetSlides('../data/slides/', path_labels='../data/labels/', path_infos='../data/info_slides/')[slide][magnification])
        morphology.surface()
        
        #performs another segmentation
        self.segmentations_union = union([seg for i,seg in enumerate(list(self.segmentations_union.geoms)) if morphology.metrics['Surface'][i]>= max(morphology.metrics['Surface'])*threshold])
        self.segmentations = [np.array(list(geom.exterior.coords)) for geom in list(self.segmentations_union.geoms)]
