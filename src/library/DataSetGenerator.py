#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

from library.utils import SaveFiles as sf
from library.utils import PrintFiles as pf
import numpy as np
import os
from shutil import rmtree
from pathlib import Path
from tqdm import tqdm
from collections.abc import Iterable
from matlab import mlarray

import torch
from torch.utils.data import Dataset
import copy
import cv2
from library.FileMakers import FileMakers as fm

path_slides = '/network/lustre/iss01/home/valentin.abadie/Project/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/data/slides/'
path_labels = '/network/lustre/iss01/home/valentin.abadie/Project/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/data/labels/'
path_infos = '/network/lustre/iss01/home/valentin.abadie/Project/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/data/info_slides/'

#################################################################################################################################

class DataSetGenerator(Dataset):
    
    """Makes a suitable dataset to be passed as input to Dataloader
    
    Attributes :
        path_slides : str, path to the folder containing the slides (.ndpi)
        path_labels : str, path to the folder containing the labels (.p)
        path_infos : list of str, path to the folder containing the info files (.p), which are used to make slides and labels matching
        
        slide : int or list, the IDs or indexes of the slides to be included in the dataset
        magnification : int or list, the magnification levels to be included in the dataset
        
        size : 2-tuple, dimensions of each generated region
        stride : 2-tuple, the stride between 2 consecutive regions
        offset : 2-tuple, distance from the top-left of the slide to start picking regions
        border : bool, includes or not areas which are partly out of the slide
        ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
        ROIp : float in [0,1], if>0 only selects the region of which the fraction intersecting the ROI is at least above                   ROIp. Default 0.
        tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
        include_gray_matter : bool. If True, only regions in gray matter will be sampled; Default : True.
        random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
        balance : float. The balance between the number of positive and negative samples to be generated.
        
        random : bool. If True, creates a sampler which generate positive samples by targetting regions containing objects, and negative by randomization. The only parameters to be taken into account in this case are : size, ROI, balance, tolerance, include_gray_matter and random_pos. If False, generates samples one after the other, by simple 2-D constant stride sampling. The only parameters to be taken into account in this case are : size, ROI, ROIp, offset, stride, borders. Default : True.
        
        load : bool. If True, the dataset is considered to be in "already saved" mode. To get its data, it is going to search directly on a folder where samples are supposed to be stored.
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        level_slide_[key]_[zoom] : LevelSlide obj, with [key] and [zoom] being int or str corresponding to the slide and magnification levels to be in the dataset.
        
        region_indices : list of 3-tuples. Each tuple of this list contains the slide key, the magnification level and the index of the sample in the sampler of the LevelSlide object computed from this slide key and magnification level. It is used to retrieve any sample of the dataset.
        regions : list of str, name of the files containing the samples of the dataset when it has been stored.
        
    Methods :
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset
        
        Special :
            make : creates the list of regions in the dataset 
            save : saves all regions of the dataset into a folder, as well as the dataset object itself
            set_path : sets the path to the folder were data has to be stored
            set_show_coord : sets whether the region coordinates have to be returned when calling __getitem__
        
        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
        
    """
    
    def __init__(self, path_slides = path_slides, path_labels = path_labels, path_infos=path_infos, slides=None, magnification = 'x40', size=100, stride=100, offset=0, borders=False, ROI=1, ROIp=0, balance = 0.5, include_gray_matter = True, tolerance = 100, random_pos = True, random = True, load = False):
        
        """initialises the class
        
        Inputs :
            path_slides : str, path to the folder containing the slides (.ndpi)
            path_labels : str, path to the folder containing the labels (.p)
            path_infos : list of str, path to the folder containing the info files (.p), which are used to make slides and labels matching
            slides : list of str or int, ids or indices of the slides to be included in the dataset
            magnification : list of int or str, values or indices of the magnification levels to be included in the dataset
            
            size : 2-tuple, dimensions of each generated region
            stride : 2-tuple, the stride between 2 consecutive regions
            offset : 2-tuple, distance from the top-left of the slide to start picking regions
            border : bool, includes or not areas which are partly out of the slide
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
            ROIp : float in [0,1], if>0 only selects the region of which the fraction intersecting the ROI is at least above                   ROIp. Default 0.
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            include_gray_matter : bool. If True, only regions in gray matter will be sampled; Defualt : True.
            data_aug : int. When generating a positive samples from an objects, says how many positive samples to generate from the same object. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
            
            random : bool. If True, creates a sampler which generate positive samples by targetting regions containing objects, and negative by randomization. The only parameters to be taken into account in this case are : size, ROI, balance, tolerance, include_gray_matter and data_aug. If False, generates samples one after the other, by simple 2-D constant stride sampling. The only parameters to be taken into account in this case are : size, ROI, ROIp, offset, stride, borders. Default : True.
            
            load : bool. If True, considers the same dataset is already store somewhere
        
        """
        
        
        self.path_slides = path_slides
        self.path_labels = path_labels
        self.path_infos = path_infos
        
        #retrieves sorted list of slides ids
        slide_files = sorted(list(filter(lambda x: x[0] != '.', os.listdir(self.path_slides))))
        ids = [item.split('.')[0] for item in slide_files]
        
        if slides is None:
            self.slides = ids
        else :
            if isinstance(slides, list):
                self.slides = slides
            else :
                self.slides = [slides]
        
        if isinstance(magnification, list):
            self.magnification = magnification
        else :
            self.magnification = [magnification]
            
        self.size = size
        self.stride = stride
        self.offset = offset
        self.borders = borders
        self.ROI = ROI
        self.ROIp = ROIp
        
        self.balance = balance
        self.include_gray_matter = include_gray_matter
        self.tolerance = tolerance
        self.random_pos = random_pos
        self.random = random
        
        self._show_coord = False
        self.load = load 
        self.aug = False
        
        self.make()
        self.set_path()
        
        
        
    def make(self):
        """gets all the informations about slides, level and position to get the required region"""
        
        #list of regions to be present in the dataset
        self.region_indices = list()
        
        from library.SlidesHandling import DataSetSlides
        D = DataSetSlides(path_slides=self.path_slides,path_labels=self.path_labels,path_infos=self.path_infos)
        
        #go through all slides to be included in the dataset
        for i,key in enumerate(self.slides) :          
            slide = D[key]
            
            #go through all magnification levels to be included in the dataset
            for i,zoom in enumerate(self.magnification):
                level_slide = slide[zoom]
                
                #makes a sampler for each magnification level of each slide, and sets it as an attribute of the class to be re-used afterwards
                level_slide.update_sampler(size=self.size, stride=self.stride, offset=self.offset, borders=self.borders, ROI=self.ROI, ROIp=self.ROIp,balance=self.balance,include_gray_matter=self.include_gray_matter, tolerance=self.tolerance, random_pos=self.random_pos, random=self.random)
                setattr(self,'level_slide_{}_{}'.format(key,zoom), level_slide)
                
                #appends all regions of the computed sampler to the regions of the dataset
                for index,sample in enumerate(level_slide.sampler): #the information given by the sampler is stored
                    self.region_indices.append([key,zoom,index])
     
    def save(self,path=None,rm=True):
        
        """saves all the regions of the data set in the indicated folder
        
        Inputs :
            path : str, path to the folder were the data has to be stored. If None, is set to be default path of the class. Default : None.
            rm : bool, if True remove all the files in the targeted folder before saving dataset in it. Default : True.
        
        """
        
        if path is None:
            path = self.path
            
        folder = Path(path)
        
        if not folder.is_dir():
            os.mkdir(self.path)
            
        self.regions = list()
        
        #removes previous dataset if there was any
        if rm :
            for file in list(filter(lambda x: 'region' in x, os.listdir(self.path))):
                os.remove(self.path + '/' + file)
            try :
                os.remove(self.path +'/Dataset.p')
            except :
                pass

        slides = np.unique([str(x[0]) for x in self.region_indices])
        #creates a dataset composed of several slides
        for i,key in enumerate(slides):
            key = str(key)
            zooms = np.unique([str(x[1]) for x in list(filter(lambda x : x[0] == key,self.region_indices))])
            
            #and several zooms (maybe it can be useful ?)
            for i,zoom in enumerate(zooms):
                zoom = str(zoom)
                
                #uses the sampler to generate the dataset
                level_slide = getattr(self, 'level_slide_{}_{}'.format(key,zoom))
                regions = np.unique([x[2] for x in list(filter(lambda x : (x[0] == key) and (x[1] == zoom),self.region_indices))])
                for i,ind in enumerate(regions):
                    
                    
                    to_display = 'Fraction of the data already generated : ' + str(i) + '/' + str(len(regions))
                    print(to_display, end = '', flush = True)
                    
                    #saves each region generated by the sampler
                    region = level_slide[ind]
                    region.save(self.path)
                    self.regions.append('region_{}_{}_{}_{}_{}_{}.p'.format(key, level_slide.magnification, *region.coordinates))
                    
                delattr(self, 'level_slide_{}_{}'.format(key,zoom))
        
        #sets data set in "already saved" mode
        self.load = True
        
        #save the dataset obj in the folder
        sf.save(self.path + '/' + 'Dataset.p', self)

            
    def set_path(self,path=None):
        
        """defines the path where the data is to be stored/accessed
        
        Inputs :
            path : str, the path to set. If None, is set as '../data/datasets/'. Default : None.
        
        """
        
        if path is None:
            self.path = '../data/datasets/'
        elif '/' in path :
            self.path = path
        else :
            self.path = '../data/datasets/' + path
      
    def set_show_coord(self, value):
        
        """if the real-world coordinates of each region displayed have to be used
        
        Inputs :
            value : bool. If True, the real world coordinates of each region are returned when calling getitem
        """
        
        self._show_coord = value
    
    def __getitem__(self,index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        #if it has to return real-world coordinates of the region sample
        if self._show_coord :
            image, mask, coord = self._get_data(index)
            return {'Images' : torch.from_numpy(image), 'Labels' : torch.from_numpy(mask), 'Coords' : coord}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(image), 'Labels' : torch.from_numpy(mask)}

    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self.load : #if the dataset is already computed and stored, retrieves the corresponding file
            
            #retrieves informations about the piece of data
            infos = sf.load(self.path +'/'+ self.regions[index])
            
            #if it has to return coordinates of the sample
            if self._show_coord :
                #retrieves coordinates in name of the stored region
                coordinates = self.regions[index].split('_')[3:]
                coordinates[-1] = coordinates[-1][:-2]
                coordinates = [float(val) for val in coordinates]
                return infos['image'],infos['mask'], coordinates
            else :
                return infos['image'], infos['mask']
            
        #if the dataset is not already stored
        else :
            
            #retrieve information about the desired sample
            key,zoom,ind = self.region_indices[index]
            level_slide = getattr(self, 'level_slide_{}_{}'.format(key,zoom))
            region = level_slide[ind]
            
            #if it has to return coordinates of the sample
            if self._show_coord :
                coords = level_slide.sampler[ind]
                return region.array_region, region.mask, coords
            else :
                return region.array_region, region.mask
                
        
    def __len__(self):
        """returns the number of samples in the dataset"""
        
        if self.load :
            return len(self.regions)
        else :
            return len(self.region_indices)


################################################################################################################################# 

class Normalization(DataSetGenerator):
    
    """Performs Khan & al. color normalization (2014) on the whole dataset, based on MATLAB code.
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        already_normed : list of int, the indices of the regions which have already been normalized thus stored.
        
        eng : matlab session used to compute the normalization.
        matlab : matlab library used to define some variables
        classifier : matlab object, the classifier used to perform pixel color classification during Khan normalization
        
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset
            
        Special :
            close : shuts down the matlab session and deletes the already normed files

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
            _load : retrieves sample if it already has been normalized
    
    """
    
    def __init__(self, input_dataset):
        
        """Initialises the class
        
        Inputs :
            input_dataset : DataSetGenerator object, the dataset to be normalised
            
        """
        
        self.input_dataset = input_dataset
        
        self.path = self.input_dataset.path + '/normalised_data'
        
        try :
            #tries to create the folder containing all the regions
            os.mkdir(self.path)
        except :
            #if it fails, considers the folders already exits
            pass
        
        self._show_coord = self.input_dataset._show_coord
        self.load = False
        self.aug = self.input_dataset.aug
        self.regions = self.input_dataset.regions
        
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
        
    def _get_data(self, index, eng=None, matlab=None, target=None, path_classifier=None):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
            
            Other inputs are only necesseray when not in load mode :
                eng : matlab.engine object, the matlab engine used to compute color normalization
                matlab : matlab object, the matlab interface with python
                target : Region object, the target on which images are to be normalized
                path_classifier : str, the path to the matlab object (.mat) containing the color classifier
        
        """
        
        #if the dataset already is saved in a folder, directly loads it
        if self.load :
            
            #information to be retrieved
            infos = sf.load('/'.join((self.path, self.regions[index])))
            
            #if it has to return coordinates of the sample
            if self._show_coord :
                #retrieves coordinates in name of the stored region
                coordinates = self.regions[index].split('_')[3:]
                coordinates[-1] = coordinates[-1][:-2]
                coordinates = [float(val) for val in coordinates]
                return infos[0],infos[1], coordinates
            else :
                return infos[0],infos[1]
            if self._show_coord :
                image, mask, coords = self.input_dataset._get_data(index)
                return sf.load(self.path + '/region_{}.p'.format(index))
        
        else :
            if self._show_coord :
                image, mask, coords = self.input_dataset._get_data(index)
                
                #performs normalization via matlab code
                normalized_image = eng.image_normalization({'source' : matlab.uint8(np.array(image).tolist())}, {'target' : matlab.uint8(self.target.tolist())}, {'classifier' : classifier})
            
                #returns the normalised image, corresponding mask and coordinates
                return normalized_image, mask, coords
            
            else :
                image, mask = self.input_dataset._get_data(index)
                
                #performs normalization via matlab code
                normalized_image = self.eng.image_normalization({'source' : self.matlab.uint8(np.array(image).tolist())}, {'target' : self.matlab.uint8(self.target.tolist())}, {'classifier' : self.classifier})
            
                #returns the normalised image, corresponding mask
                return normalized_image, mask
            
            
    def save(self, eng, matlab, target, path_classifier):
        
        """Performs normalization of the whole input dataset on the same target, with the same color classifier and saves it to be computed faster
        
        Inputs :
            eng : matlab.engine object, the matlab engine used to compute color normalization
            matlab : matlab object, the matlab interface with python
            target : Region object, the target on which images are to be normalized
            path_classifier : str, the path to the matlab object (.mat) containing the color classifier
        
        """
        
        self.path = '/'.join((self.path, 'normalized_on_region_{}_{}_{}_{}_{}_{}'.format(target.id, target.magnification, *target.coordinates)))
        
        try :
            #tries to create the folder containing all the regions
            os.mkdir(self.path)
        except :
            #if it fails, considers the folders already exits, thus it removes it and creates an empty one
            rmtree(self.path)
            os.mkdir(self.path)
        
        #goes through the whole dataset and performs normalization, then saves each sample normalized this way
        for index in range(len(self)):
            to_display = 'Fraction of the data already generated : ' + str(index) + '/' + str(len(self))
            print(to_display, end = '', flush = True)
            
            #retrieves positional information about the region to normalize (id, magnification, coordinates)
            region_name_items = self.regions[index].split('_')[1:]
            region_name_items[-1] = region_name_items[-1][:-2]
            id_slide = region_name_items[0]
            magnification = region_name_items[1]
            coords = [float(data) for data in region_name_items[2:]]
            
            #retrieves content information about the region (image, mask)
            infos = self.input_dataset._get_data(index)
            image, mask = infos[0], infos[1]

            #performs normalization via matlab code
            normalized_image = eng.image_normalization({'source' : matlab.uint8(np.array(image).tolist())}, {'target' : matlab.uint8(target.array_region.tolist())}, {'classifier' : path_classifier})

            #saves the 
            sf.save('/'.join((self.path, 'region_{}_{}_{}_{}_{}_{}.p'.format(id_slide, magnification, *coords))), (normalized_image, mask))
                
        #puts the dataset in load mode and saves it
        self.load = True
        sf.save('/'.join((self.path, 'Dataset.p')), self)

    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)   
        
#################################################################################################################################
        
class DataSplitter(DataSetGenerator):
    
    """This class allows to split the input dataset into several one, with chosen proportions for each piece of data created
    
    Attributes :
    
        datasets : list of DataSetGenerator objects, resulting from the splitting of the input dataset
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
    
    """
    
    def __init__(self,dataset,props=[0.8,0.2]):
        
        """initialises the class
        
        Inputs :
            dataset : DataSetGenerator object, the dataset to be splitted
            props : list of floats, the list containing the proportions of the original dataset to be sent to the splitted ones
        
        """
        
        #props is the element which determines all : the number of datasets created from the input one and the proportions of each dataset created
        if len(props)==2: #for this special case, the 2 datasets are named 'train' and 'test' as it is a common use
            dataset_1 = copy.copy(dataset)
            dataset_1.regions = list(dataset_1.regions[:int(props[0]*len(dataset_1.regions))])
            
            dataset_2 = copy.copy(dataset)
            dataset_2.regions = list(dataset_2.regions[int(props[0]*len(dataset_2.regions)):])
            
            self.datasets = {'train': dataset_1, 'test' : dataset_2}
            
        elif len(props)==3:#for this special case, the 3 datasets are named 'train', 'valid' and 'test' as it is a common use
            dataset_1 = dataset
            dataset_1.regions = dataset_1.regions[:int(props[0]*len(dataset_1.regions))]
            
            dataset_2 = dataset
            dataset_2.regions = dataset_2.regions[int(props[0]*len(dataset_2.regions)):int((props[0]+props[1])*len(dataset_2.regions))]
            
            dataset_3 = dataset
            dataset_3.regions = dataset_3.regions[int((props[0]+props[1])*len(dataset_3.regions)):]
            
            self.datasets = {'train': dataset_1, 'valid' : dataset_2, 'test' : dataset_3}
            
        else : #for more than 3 datasets created - can be used for cross-validaton purposes
            self.datasets = {i:0 for i in range(len(props))}
        
            for i,prop in enumerate(props) :
                dataset_i = dataset
                dataset_i.regions = dataset_i.regions[int(sum(props[:i])*len(dataset_i.regions)):int(sum(props[:i+1])*len(dataset_i.regions))]
                self.datasets[i] = dataset_i
                
                
##################################################################################################################################                
                
class Fusion(DataSetGenerator):
    
    """A class to concatenate a list of DataSetGenerator objects, in order to make a unique dataset
    
    Attributes :
        
        datasets : list of DataSetGenerator objects, the list of datasets to be concatenated.
        path : the path samples from these datasets are to be stored
        
        load : bool. If True, the dataset is considered to be in "already saved" mode. To get its data, it is going to search directly on a folder where samples are supposed to be stored.
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.

        regions : list of str, name of the files containing the samples of the dataset when it has been stored.
        indices : 2D indexation of the multiple datasets
        
    
    Methods :
        
         Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
        
    """
    
    def __init__(self, *datasets):
        
        """initialises the class
        
        Inputs :
            datasets : the list of DataSetGenerator objects to be concatenated
        
        """
        
        self.datasets = datasets
        
        self.path = datasets[0].path
        self._show_coord = datasets[0]._show_coord
        self.load = datasets[0].load
        self.aug = datasets[0].aug
        self.regions = [element for data in self.datasets for element in data.regions]
        
        #placing all datasets on same basis
        for data_set in self.datasets:
            data_set._show_coord = self._show_coord
            data_set.load = self.load
            data_set.aug = self.aug
        
        #creating a useful list of indexing tuples
        self.indices = [[[i,j] for j in range(l)] for i,l in enumerate([len(data_set) for data_set in self.datasets])]#)#.reshape(2, -1)
        
        indices = list()
        for idx in self.indices :
            indices += idx
        self.indices = indices
        
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """

        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
        
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        #gets data according to the 2D index of each sample
        pair_idx = self.indices[index]
        if self._show_coord :
            image, mask, coords = self.datasets[pair_idx[0]]._get_data(pair_idx[1])
        else :
            image, mask = self.datasets[pair_idx[0]]._get_data(pair_idx[1])
            
        if self._show_coord :
            return image, mask, coords
        else :
            return image, mask
           
    def __len__(self):
        """returns the number of samples in the dataset"""
        return sum([len(data_set) for data_set in self.datasets])
    
##################################################################################################################################

class MultiDataSplitter(DataSetGenerator):
    """This class allows to split the input list of datasets into several ones, with chosen proportions for each piece of data created. It uses a combination of DataSplitter and Fusion classes.
    
    Attributes :
        datasets : list of DataSetGenerator objects, resulting from the splitting of the input dataset
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
    
    """
    
    def __init__(self,datasets,props=[0.8,0.2]):
        
        """initialises the class
        
        Inputs :
            datasets : list of DataSetGenerator objects, the datasets to be splitted
            props : list of floats, the list containing the proportions of the original dataset to be sent to the splitted ones
        
        """
        
        #splits each dataset
        data_splits = list(map(list, zip(*[list(DataSplitter(data).datasets.values()) for data in datasets])))
        
        #fusion each corresponding split of the datasets
        self.datasets = dict()
        self.datasets['train'] = Fusion(*data_splits[0])
        self.datasets['test'] = Fusion(*data_splits[1])
        
        #props is the element which determines all : the number of datasets created from the input one and the proportions of each dataset created
        if len(props)==2: #for this special case, the 2 datasets are named 'train' and 'test' as it is a common use
            self.datasets = {'train': Fusion(*data_splits[0]), 'test' : Fusion(*data_splits[1])}
            
        elif len(props)==3:#for this special case, the 3 datasets are named 'train', 'valid' and 'test' as it is a common use
            self.datasets = {'train': Fusion(*data_splits[0]), 'valid' : Fusion(*data_splits[1]), 'test' : Fusion(*data_splits[2])}
            
        else : #for more than 3 datasets created - can be used for cross-validaton purposes
            self.datasets[i] = Fusion(*data_splits[i])

                                              
##################################################################################################################################                                             
                                              
class DataAugmentation(DataSetGenerator):
    
    """Performs data augmentation on the input dataset. The tasks to be performed are :
    
    - Rotation at 90, 180 and 270° (set rotation90/180/270 = True as input)
    - Flip (set flip = True as input)
    
    Each of these operation are appended to the dataset such that the resulting dataset resemble to that :
    concatenation(INPUTDATA, ROTATION90 (same lenght as INPUTDATA), ROTATION180 (same lenght as INPUTDATA), FLIPPED (same lenght as INPUTDATA), FLIPPED + ROTATION90 (same lenght as INPUTDATA), ...)
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        lenght : size of the input dataset
        indexation : list of 2-tuple containing information about which transformation bring to every sample
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
            
    """
    
    def __init__(self, input_dataset, rotation90 = False, rotation180 = False, rotation270 = False, flip = False):
        
        """initialises the class
        
        Inputs :
            input_dataset : the dataset to be transformed by the class
            rotation90 : bool, if True performs rotation by 90° data augmentation. Default : False.
            rotation180 : bool, if True performs rotation by 180° data augmentation. Default : False.
            rotation270 : bool, if True performs rotation by 270° data augmentation. Default : False.
            flip : bool, if True performs flip left to right data augmentation. Default : False.
            
        """
        
        self.input_dataset = input_dataset
        self.lenght = len(self.input_dataset)
        self.path = self.input_dataset.path
        self.regions = self.input_dataset.regions
        self._show_coord = self.input_dataset._show_coord
        self.aug = True
        
        # makes a list of the data augmentation to be processed
        self.indexation = [(0,0)]*len(self.input_dataset)
        
        if rotation90 :
            self.indexation += [(1,0)]*self.lenght
        if rotation180 :
            self.indexation += [(2,0)]*self.lenght
        if rotation270 :
            self.indexation += [(3,0)]*self.lenght
            
        if flip :
            self.indexation += [(0,1)]*len(self.input_dataset)
            if rotation90 :
                self.indexation += [(1,1)]*self.lenght
            if rotation180 :
                self.indexation += [(2,1)]*self.lenght
            if rotation270 :
                self.indexation += [(3,1)]*self.lenght

        self.regions = self.regions * int(1 + rotation90 + rotation180 + rotation270) * int(1 + flip)
                
    def __getitem__(self,index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
    
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        index_input = index % self.lenght
        
        if self._show_coord :
            image, mask, coords = self.input_dataset._get_data(index_input)
        else :
            image, mask = self.input_dataset._get_data(index_input)
        
        #performs the data augmentation operation in function of the index
        if self.indexation[index][0] == 1 : #rotation by 90°
            image = np.rot90(image)
            mask = np.rot90(mask)
        elif self.indexation[index][0] == 2 :#rotation by 180°
            image = np.rot90(np.rot90(image))
            mask = np.rot90(np.rot90(mask))
        elif self.indexation[index][0] == 3 :#rotation by 270°
            image = np.rot90(np.rot90(np.rot90(image)))
            mask = np.rot90(np.rot90(np.rot90(mask)))
        if self.indexation[index][1] == 1 : #flip
            image = np.fliplr(image)
            mask = np.fliplr(mask)
            
        if self._show_coord :
            return image, mask, coords
        else :
            return image, mask
        
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)
    
#################################################################################################################################
    
class DataCropping(DataSetGenerator):
    
    """Square-crops every label of the dataset, in the center of the image.
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        crop_size : int, size of crop square.
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
    
    """
    
    def __init__(self, input_dataset, crop_size = None):
        
        """initialises the class
        
        Inputs :
        
            input_dataset : the dataset to be transformed by the class
            crop_size : int, size of crop square. If None, does not performs cropping. Default : None.
        
        """
        
        self.input_dataset = input_dataset
        self.crop_size = crop_size
        self.path = self.input_dataset.path
        self.regions = self.input_dataset.regions
        self._show_coord = self.input_dataset._show_coord
        self.aug = self.input_dataset.aug
                
    def __getitem__(self,index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """

        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
    
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self.input_dataset._get_data(index)
        else :
            image, mask = self.input_dataset._get_data(index)
        
        if self.crop_size is None :
            pass
        else : #performs crop operation on the mask
            mask = mask[(mask.shape[0]-self.crop_size)//2:(mask.shape[0]-self.crop_size)//2 + self.crop_size, (mask.shape[1]-self.crop_size)//2:(mask.shape[1]-self.crop_size)//2 + self.crop_size]
        
        if self._show_coord :
            return image, mask, coords
        else :
            return image, mask
            
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)

#################################################################################################################################
    
class SideDeletion(DataSetGenerator):
    """Creates 2 new classes : one for tangles on the side of the image and one plaques on the side of the image. The goal is to differentiate them from regular objects to avoid side effects (especially surface bias due to the side dividing objects). The 2 new class created maybe bypassed when computing the loss for the neural network not to take them into account.
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
    
    """
    
    def __init__(self, input_dataset):
        
        """initialises the class
        
        Inputs :
        
            input_dataset : the dataset to be transformed by the class
            
        """
        
        self.input_dataset = input_dataset
        self.path = self.input_dataset.path
        self.regions = self.input_dataset.regions
        self._show_coord = self.input_dataset._show_coord
        self.aug = self.input_dataset.aug
        
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
        
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self.input_dataset._get_data(index)
        else :
            image, mask = self.input_dataset._get_data(index)
        
        #suppression of side tangles (putting in another class)
        nc, cc = cv2.connectedComponents((mask==2).astype(np.uint8))
        for i in range(1,nc):
            positions_cc = np.argwhere(cc==i)
            #checks if a connected component is on the side of the image
            if (np.min(positions_cc[:,0])<= 0) or (np.max(positions_cc[:,0])>= mask.shape[0]-1) or (np.min(positions_cc[:,1])<= 0) or (np.max(positions_cc[:,1])>= mask.shape[1]-1) :
                mask[cc==i] = 4 #creates a fifth class of side tangles
        
        #suppression of side plaques (putting in another class)
        nc, cc = cv2.connectedComponents((mask==3).astype(np.uint8))
        for i in range(1,nc):
            positions_cc = np.argwhere(cc==i)
            #checks if a connected component is on the side of the image
            if (np.min(positions_cc[:,0])<= 0) or (np.max(positions_cc[:,0])>= mask.shape[0]-1) or (np.min(positions_cc[:,1])<= 0) or (np.max(positions_cc[:,1])>= mask.shape[1]-1) :
                mask[cc==i] = 5 #creates a sixth class of side plaques
        
        if self._show_coord :
            return image, mask, coords
        else :
            return image, mask
        
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)
    
#################################################################################################################################
        
class RemoveDuplicate(DataSetGenerator):
    """This class removes from the dataset regions which present multiple objects of same nature on them
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        label : int, label of objects to be removed if multiple are present on the region.
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
    
    """    
    
    def __init__(self, input_dataset, label=2):
        
        """initialises the class
        
        Inputs :
        
            input_dataset : the dataset to be transformed by the class
            label : int, label of objects to be removed if multiple are present on the region.
        
        """
        
        self.input_dataset = input_dataset
        self.path = self.input_dataset.path
        self._show_coord = self.input_dataset._show_coord
        self.load = self.input_dataset.load
        self.label = label
        self.aug = self.input_dataset.aug
        
        #performs the removal of samples showing multiple objects
        self.regions = list()
        for i in range(len(self.input_dataset)):
            if cv2.connectedComponents(np.array(self.input_dataset[i]['Labels']==self.label).astype(np.uint8))[0] <= 2 :
                self.regions.append(self.input_dataset.regions[i])
            
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
        
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self.load : #if the dataset is already computd and stored, retrievs the corresponding file
            if self._show_coord :
                infos = sf.load(self.path +'/'+ self.regions[index])
                coordinates = self.regions[index].split('_')[3:]
                coordinates[-1] = coordinates[-1][:-2]
                coordinates = [float(val) for val in coordinates]
                return infos['image'],infos['mask'], coordinates
            else :
                infos = sf.load(self.path +'/'+ self.regions[index])
                return infos['image'], infos['mask']
        else :
            slide,zoom,ind = self.region_ind[index]
            level_slide = DataSetSlides(path_slides=self.path_slides,path_labels=self.path_labels,path_infos=self.path_infos)[slide][zoom]
            level_slide.update_sampler(size=self.size, stride=self.stride, offset=self.offset, borders=self.borders, ROI=self.ROI, ROIp=self.ROIp,balance=self.balance,include_gray_matter=self.include_gray_matter, tolerance=self.tolerance, random_pos=self.random_pos, random=self.random)
            region = level_slide[ind]
            return region.array_region, region.mask
        
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)
    
#################################################################################################################################
    
class LabelSelect(DataSetGenerator):
    
    """This selects only labels which are wanted on the mask
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        labels : list of int, labels of objects to be kept as objects of interest.
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
    
    """    
    
    def __init__(self, input_dataset, labels=[2]):
        
        """initialises the class
        
        Inputs :
        
            input_dataset : the dataset to be transformed by the class
            labels : list of int, labels of objects to be kept as objects of interest.
        
        """
        
        self.input_dataset = input_dataset
        self.path = self.input_dataset.path
        self.regions = self.input_dataset.regions
        self._show_coord = self.input_dataset._show_coord
        self.aug = self.input_dataset.aug
        self.labels = labels
                
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
    
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self.input_dataset._get_data(index)
        else :
            image, mask = self.input_dataset._get_data(index)
        
        #selecting only labels of interest
        for label in np.unique(mask):
            if label not in self.labels :
                mask[mask==label] = 0
            else :
                mask[mask==label] = self.labels.index(label) + 1

        if self._show_coord :
            return image, mask, coords
        else :
            return image, mask
           
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)

#################################################################################################################################
    
class DetectionOnly(DataSetGenerator):
    """creates a dataset for which we only know if there is a tangle/plaque on the image or not
    
    Attributes :
        input_dataset : the dataset to be transformed by the class
        path : path to folder used to store the samples of the dataset
        regions : the list of sampled regions in the dataset
        _show_coord : bool. If True, samples returned provide information about their real-world coordinates.
        aug : bool. If True, the data is to be augmented. Default : False.
        
        labels : list of int, labels of object to be detected.
    
    Methods : 
    
        Built-in :
            __init__ : initialises the class
            __getitem__ : returns the data sample corresponding to the index passed as input
            __len__ : returns the number of samples in the dataset

        Backend :
            _get_data : __getitem__ backend function, performs operations to retrieve a given data sample
    
    """
    
    def __init__(self, input_dataset, label=2):
        
        """initialises the class
        
        Inputs :
        
            input_dataset : the dataset to be transformed by the class
            label : list of int, labels of objects to be detected.
        
        """
        
        self.input_dataset = input_dataset
        self.path = self.input_dataset.path
        self.regions = self.input_dataset.regions
        self._show_coord = self.input_dataset._show_coord
        self.aug = self.input_dataset.aug
        self.label = label
                
    def __getitem__(self, index):
        
        """Returns a data sample
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask)), 'Coords' : coords}
        else :
            image, mask = self._get_data(index)
            return {'Images' : torch.from_numpy(np.copy(image)), 'Labels' : torch.from_numpy(np.copy(mask))}
    
    def _get_data(self, index):
        
        """retrieves data pieces from the dataset for a given index, and compute transformation to be brought to it
        
        Inputs :
            index : int, index of the data sample in the dataset
        
        """
        
        if self._show_coord :
            image, mask, coords = self.input_dataset._get_data(index)
        else :
            image, mask = self.input_dataset._get_data(index)
        
        #converts the mask into a simple boolean of presence of the object on the region
        if self.label in np.unique(mask):
            labels = np.array(1)
        else:
            labels = np.array(0)

        if self._show_coord :
            return image, labels, coords
        else :
            return image, labels
           
    def __len__(self):
        """returns the number of samples in the dataset"""
        return len(self.regions)
    
#################################################################################################################################
