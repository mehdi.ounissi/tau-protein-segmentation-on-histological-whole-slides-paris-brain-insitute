#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import torch
import torch.nn.functional as F
from torch import nn,Tensor,LongTensor
import numpy as np
import torchvision.models as models
from torch.autograd import Variable

##################################################################################################################################

class UNetDown(nn.Module):
    
    """Class defining a downsampling layer of the UNet
    
    Attributes :
        model : nn.Module object, the downsampling layer Pytorch architecture
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self, in_size, out_size, dropout=0., kernel_size=3, padding=1):
        
        """Initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            dropout : float, dropout to be applied to the layer
            kernel_size : int, size of the square convolution kernel
            padding : int, padding when performing convolution
        
        """
        
        super(UNetDown, self).__init__()
        self.model = nn.Sequential(
            nn.Conv2d(in_size, out_size, kernel_size=kernel_size, stride=2, padding=padding),
            nn.InstanceNorm2d(out_size),
            nn.LeakyReLU(0.2),
            nn.Dropout(dropout)
          )

    def forward(self, x):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
        """
        
        return self.model(x)
    
##################################################################################################################################

class UNetUp(nn.Module):
    
    """Class defining a upsampling layer of the UNet
    
    Attributes :
        model : nn.Module object, the upsampling layer Pytorch architecture
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self, in_size, out_size, dropout=0., kernel_size=4, padding=1):
        
        """Initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            dropout : float, dropout to be applied to the layer
            kernel_size : int, size of the square upconvolution kernel
            padding : int, padding when performing upconvolution
        
        """
        
        super(UNetUp, self).__init__()
        self.model = nn.Sequential(
            nn.ConvTranspose2d(in_size, out_size, kernel_size=kernel_size, stride=2, padding=padding),
            nn.InstanceNorm2d(out_size),
            nn.ReLU(inplace=True),
            nn.Dropout(dropout)
        )
        
    def forward(self, x, skip_input=None):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
            skip_input : torch tensor, second input to be concatenated to x, used to make skip connections in the network
        """
        
        if skip_input is not None:
            x = torch.cat((x, skip_input), 1) # add the skip connection
        x = self.model(x)
        return x

##################################################################################################################################

class FinalLayer(nn.Module):
    
    """Class defining a output layer of the UNet (without ReLu function)
    
    Attributes :
        model : nn.Module object, the output layer Pytorch architecture
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self, in_size, out_size, dropout = 0., kernel_size=3, padding=1):
        
        """Initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            dropout : float, dropout to be applied to the layer
            kernel_size : int, size of the square upconvolution kernel
            padding : int, padding when performing upconvolution
        
        """
        
        super(FinalLayer, self).__init__()
        self.model = nn.Sequential(
            nn.Upsample(scale_factor=2),
            nn.Conv2d(in_size, out_size, kernel_size=kernel_size, padding=padding),
            nn.Dropout(dropout)
        )
        
    def forward(self, x, skip_input=None):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
            skip_input : torch tensor, second input to be concatenated to x, used to make skip connections in the network
        """
        
        if skip_input is not None:
            x = torch.cat((x, skip_input), 1) # add the skip connection
        x = self.model(x)
        return x

##################################################################################################################################
    
class UNet(nn.Module):
    
    """Class defining a the UNet model
    
    Attributes :
        dropout_conv : float, dropout to be applied to every downsampling layers
        dropout_upconv : float, dropout to be applied to every upsampling layers
        depth : int, the depth of the network (number of downsampling layers)
        
        input_layer : UNetDown object, layer taking the input of the network
        final : FinalLayer object, layer giving the output of the network
        
        layer_down_[i] : UNetDown object, indexed by [i] being an int, are each downsampling layer
        layer_up_[i] : UNetUp object, indexed by [i] being an int, are each upsampling layer
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self,in_size,out_size, depth=4, n_features_in=64, dropout_conv = 0., dropout_upconv = 0., kernel_conv=3, kernel_upconv=4, padding_conv=1, padding_upconv=1):
        
        """initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            depth : int, the depth of the network (number of downsampling layers)
            n_features : int, number of channels after the input layer
            dropout_conv : float, dropout to be applied to every downsampling layers
            dropout_upconv : float, dropout to be applied to every upsampling layers
            kernel_conv : int, size of the every downsampling kernel
            padding_conv : int, padding when performing downsampling
            kernel_upconv : int, size of the every upsampling kernel
            padding_upconv : int, padding when performing upsampling
        
        """
        
        super(UNet, self).__init__()
        
        self.dropout_conv = dropout_conv
        self.dropout_upconv = dropout_upconv
        
        self.depth = depth
        
        #defining input layer
        self.input_layer = UNetDown(in_size,n_features_in)
        
        #defining dynamically the downsampling hidden layers
        for i in range(depth-1):
            setattr(self,'layer_down_{}'.format(i), UNetDown(n_features_in*2**i,n_features_in*2**(i+1), dropout = dropout_conv, kernel_size=kernel_conv,padding=padding_conv))
        setattr(self,'layer_down_{}'.format(depth-1),UNetDown(n_features_in*2**(depth-1),n_features_in*2**(depth-1), dropout = dropout_conv, kernel_size=kernel_conv,padding=padding_conv))

        #defining dynamically the upsampling layers
        setattr(self, 'layer_up_{}'.format(0), UNetUp(n_features_in*2**(depth-1),n_features_in*2**(depth-1), dropout = dropout_upconv, kernel_size=kernel_upconv,padding=padding_upconv))
        for i in range(depth-1):
            setattr(self,'layer_up_{}'.format(i+1), UNetUp(n_features_in*2**(depth-i),n_features_in*2**(depth-i-2), dropout = dropout_upconv, kernel_size=kernel_upconv,padding=padding_upconv))

        #defining output layer
        self.final = FinalLayer(2*n_features_in,out_size, kernel_size=kernel_conv,padding=padding_conv)

    def forward(self, x):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
        """
        
        #computes the downsampling forward pass
        x_down = list()
        x_down.append(self.input_layer(x))
        for i in range(self.depth):
            
            #retrieves the layer to be used
            layer_down = getattr(self, 'layer_down_{}'.format(i))
            
            #stores each feature vector computed by each downsampling pass
            x_down.append(layer_down(x_down[i]))
        
        #computes the upsampling forward pass
        layer_up = getattr(self, 'layer_up_{}'.format(0))
        x = layer_up(x_down[-1])
        for i in range(self.depth-1):
            
            #retrieves the layer to be used
            layer_up = getattr(self, 'layer_up_{}'.format(i+1))
            
            #gets the feature computed from downsampling, crops it to fit with the input size of the current upsampling layer, uses it as skip input
            x_crop = x_down[-(i+2)]
            x_crop = x_crop[:,:,(x_crop.size(2)-x.size(2))//2:(x_crop.size(2)-x.size(2))//2 + x.size(2),(x_crop.size(3)-x.size(3))//2:(x_crop.size(3)-x.size(3))//2 + x.size(3)]
            
            #computes upsampling output
            x = layer_up(x,skip_input = x_crop)

        #gets the feature computed from downsampling, crops it to fit with the input size of the final layer, uses it as skip input
        x_crop = x_down[0]
        x_crop = x_crop[:,:,(x_crop.size(2)-x.size(2))//2:(x_crop.size(2)-x.size(2))//2 + x.size(2),(x_crop.size(3)-x.size(3))//2:(x_crop.size(3)-x.size(3))//2 + x.size(3)]
        
        #computes output
        x = self.final(x,skip_input = x_crop)
        
        return x
    
##################################################################################################################################

class CNNlayer(nn.Module):
    
    """Class defining a convolution layer of the CNN classifier
    
    Attributes :
        model : nn.Module object, the downsampling layer Pytorch architecture
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self, in_size, out_size, kernel_size=3, padding=1):
        
        """Initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            kernel_size : int, size of the square convolution kernel
            padding : int, padding when performing convolution
        
        """
        
        super(CNNlayer, self).__init__()
        self.model = nn.Sequential(
            nn.BatchNorm2d(in_size),
            nn.ReLU(),
            nn.Conv2d(in_size, out_size, kernel_size=kernel_size, stride=1, padding=padding),
            nn.MaxPool2d(2, stride=2)
          )
        
    def forward(self, x):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
        """
        
        return self.model(x)
    
##################################################################################################################################

class DenseLayer(nn.Module):
    
    """Class defining a convolution layer of the CNN classifier
    
    Attributes :
        model : nn.Module object, the downsampling layer Pytorch architecture
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self, in_shape, out_shape): 
        
        """Initialises the class
        
        Inputs :
            in_size : tuple of ints, the input shape
            out_size : tuple of ints, the desired output shape
        
        """
        
        super(DenseLayer, self).__init__()
        
        self.out_shape = out_shape
        
        self.model = nn.Sequential(
            nn.BatchNorm1d(in_shape[0]),
            nn.Linear(np.prod(in_shape), np.prod(out_shape))
          )
        
    def forward(self, x):
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
        """
        
        return torch.reshape(self.model(x), [-1] + self.out_shape)

##################################################################################################################################
    
class DetectionCNN(nn.Module):
    
    """Class defining a the UNet model
    
    Attributes :
        dropout_conv : float, dropout to be applied to every convolutional layers
        dropout_fully : float, dropout to be applied to every fully-connected layers
        depth : int, the depth of the network (number of downsampling layers)

        cnn_[i] : CNNLayer object, indexed by [i] being an int, are each convolutional layer
        dense_in : DenseLayer object, first fully-connected layer of the classifier part
        dense_hidden : DenseLayer object, first fully-connected layer of the classifier part
        dense_out : DenseLayer object, first fully-connected layer of the classifier part
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
        
        Special :
            forward : computes the forward pass
    """
    
    def __init__(self,in_size,out_size, depth = 4, n_features_in=64, kernel=3, padding=1, dropout_conv = 0.1, dropout_fully = 0.5):
        
        """initialises the class
        
        Inputs :
            in_size : int, number of input channels
            out_size : int, number of output channels
            depth : int, the depth of the network (number of downsampling layers)
            n_features : int, number of channels after the input layer
            dropout_conv : float, dropout to be applied to every convolutional layers
            dropout_fully : float, dropout to be applied to every fully-connected layers
            kernel : int, size of the every convolutional kernel
            padding : int, padding when performing convolution
        
        """
        
        super(DetectionCNN, self).__init__()
        
        self.depth = depth
        self.dropout_conv = dropout_conv
        self.dropout_fully = dropout_fully
        
        #defining dynamically the convolution layers
        setattr(self,'cnn_{}'.format(0), CNNlayer(in_size, n_features_in, kernel_size=kernel, padding=padding))
        for i in range(1, depth):
            setattr(self,'cnn_{}'.format(i), CNNlayer(n_features_in*2**(i-1), n_features_in*2**i, kernel_size=kernel, padding=padding))
        
        #defines fully-connected layers
        self.dense_hidden = DenseLayer([512], [100])
        self.dense_out =  DenseLayer([100], [out_size])
        
    def forward(self, x):
        
        """Computes the forward pass through the layer
        
        Inputs :
            x : torch tensor, the vector passed as input
        """
        
        #computes vector features from CNN layers
        for i in range(self.depth):
            x = getattr(self,'cnn_{}'.format(i))(x)
        
        #applies dropout
        x = F.dropout(x, p = self.dropout_conv, training = self.training)
        
        x = nn.Flatten()(x)#.reshape([x.shape[0]] + [1] + [np.prod(x.shape[1:])])
        
        #defines properly the first fully connected layer
        if hasattr(self, 'dense_in') :
            x = self.dense_in(x)
        else :
            self.dense_in = DenseLayer([x.shape[-1]], [512])
            x = self.dense_in(x)
        
        x = self.dense_hidden(x)
        
        #applies dropout
        x = F.dropout(x, p = self.dropout_fully, training = self.training)
        
        x = self.dense_out(x)

        return x

##################################################################################################################################
    
class FocalLoss(nn.Module):
    
    """This class is used to compute the Focal Loss
    
    Attributes :
    
        gamma : float, the ponderation of hard elements
        weights : list of floats, the ponderation of each class in the computation of the loss
        size_average : bool, performs the average on a batch if True, else the Sum
        crop_size : int, if only the center part of the image has to be taken into account in the computation of the loss. Choose the size.
        skip_class : list of int, if some classes are to be skipped in the computation of the loss, input the class which are to be skipped
    
    Methods :
    
        Built-in :
            __init__ : initialises the class
            __repr__ : displays information about the class
        
        Special :
            forward : used to compute the forward pass
    
    """
    
    def __init__(self, gamma=0, weights=None, size_average=True, crop_size=None, skip_class = None):
        
        """initialises the class
        
        Inputs : 
            gamma : float, the decay factor of the loss. Default : 0.
            weights : float or list of floats, the importance of each class in the computation of the loss. If None give uniform weightening. Default : None.
            size_average : bool, if True the loss is averaged over the batch, else summed. Default : True.
            crop_size : int, if only the center part of the image has to be taken into account in the computation of the loss. Choose the size.
            skip_class : list of int, if some classes are to be skipped in the computation of the loss, input the class which are to be skipped
        
        """
        
        super(FocalLoss, self).__init__()
        
        self.gamma = gamma
        
        #weights of the focal loss
        if isinstance(weights,(float,int)): self.weights = torch.Tensor([weights,1-weights])
        if isinstance(weights,list): self.weights = torch.Tensor(weights)
        else : self.weights = weights
        
        self.size_average = size_average
        
        self.crop_size = crop_size
        
        if isinstance(skip_class,(float,int)): self.skip_class = [skip_class]
        elif isinstance(skip_class,list): self.skip_class = skip_class
        elif skip_class is None : self.skip_class = skip_class
           
        #modifies weights if some classes are to be skept
        if skip_class is not None :
            if self.weights is not None :
                self.weights[np.array(skip_class).astype(bool)] = 0.
                self.weights = self.weights / self.weights.sum()
            else :
                self.weights = 1 - torch.Tensor(self.skip_class)
                self.weights = self.weights / self.weights.sum()
            
    def forward(self, input, target):
        
        """Forward pass through the loss
        
        Inputs :
            input : the predicted vector to be compared to the target
            target : the reference to compute the loss
        """
        
        #print(input.shape, flush = True)
        #print(target.shape, flush = True)
        
        #reshaping input and target
        if input.dim()>2:
            
            #performs cropping
            if self.crop_size is not None :
                input = input[:,:, (input.size(2)-self.crop_size)//2:(input.size(2)-self.crop_size)//2+self.crop_size, (input.size(3)-self.crop_size)//2:(input.size(3)-self.crop_size)//2+self.crop_size]
            input = input.reshape(input.size(0),input.size(1),-1)  # N,C,H,W => N,C,H*W
            input = input.transpose(1,2)    # N,C,H*W => N,H*W,C
            input = input.contiguous().view(-1,input.size(2))   # N,H*W,C => N*H*W,C
        
        #performs cropping
        if self.crop_size is not None :
            target = target[:, (target.size(1)-self.crop_size)//2:(target.size(1)-self.crop_size)//2+self.crop_size, (target.size(2)-self.crop_size)//2:(target.size(2)-self.crop_size)//2+self.crop_size]
        target = target.reshape(-1, 1)
        

        #computes the output log-probabilities
        logpt = F.log_softmax(input)
        
        #print(logpt.shape)
        #print(target.shape)

        #makes the product between labels and outputs
        logpt = logpt.gather(1,target)
        
        logpt = logpt.view(-1)
        pt = Variable(logpt.data.exp())

        #makes the weights
        if self.weights is not None:
            
            #checks cuda !
            if self.weights.type()!=input.data.type():
                self.weights = self.weights.type_as(input.data)
                
            #assigns each weight to the corresponding value
            at = self.weights.gather(0,target.data.view(-1))
            logpt = logpt * Variable(at)

        loss = -1 * (1-pt)**self.gamma * logpt
        
        #averaging the loss along classes
        if self.size_average: return loss.mean()
        else: return loss.sum()
        
    def __repr__(self):
        """Displays information about the class"""
        return 'focal'
    
##################################################################################################################################

#changing the way to display CrossEntropy
CrossEntropyLoss = nn.CrossEntropyLoss
CrossEntropyLoss.__repr__ = lambda _ : 'crossentropy'

##################################################################################################################################