#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:27:24 2020

@author: valentinabadie
"""

import torch
import os
from torch import nn,Tensor,LongTensor
#from library.Metrics import JaccardIndex, FB_Score, ROC_curve, F1_thresh
from torch.autograd import Variable
import numpy as np
#import cv2
#import matplotlib.pyplot as plt
from tqdm import tqdm
from library.utils import SaveFiles as sf
#from library.DataSetSlides import DataSetSlides
#from mpl_toolkits.axes_grid1 import make_axes_locatable

def train(model, train_loader, test_loader, path_save, criterion=torch.nn.CrossEntropyLoss(), optimizer=torch.optim.Adam, lr=0.01, betas=(0.9, 0.999), epochs = 10, valid=True, classes = [0, 2], aug = True):
    
    """Function designed to train a neural network model for segmentation task"""
    
    #with open('../log/'+ path_save.split('/')[-1] + '_model_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, model.dropout_conv, model.dropout_fully), 'w') as file:
                #file.write('Starting point.')
    print('../log/'+ path_save.split('/')[-1] + '_model_batch_{}_loss_{}_dropconv_{}_dropupconv_{}.p'.format(train_loader.batch_size, criterion, model.dropout_conv, model.dropout_upconv))
    print(os.listdir('../log/'))
    
    # displays availability of GPU
    cuda = True if torch.cuda.is_available() else False
    print("cuda %s"%(cuda))  
    
    #optimizer is chosen according to inputs parameters
    optimizer = optimizer(model.parameters(), lr=lr, betas=betas)
    
    #initializes directories where to store models and performances if necessary
    try :
        os.mkdir(path_save)
    except :
        pass
    try :
        os.mkdir(path_save + '/unets')
    except :
        pass
    
    #initializes the trainer
    trainer = Trainer(model, path_save, criterion, optimizer)
    
    #initializes the list of performances of the models along epochs
    list_loss_train = list()
    list_loss_test = list()
    
    #with open('../log/'+ path_save.split('/')[-1] + '_model_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, model.dropout_conv, model.dropout_fully), 'w') as file:
                #file.write('Performing first evaluation of loss.')
    print('Performing first evaluation of loss.')
    #computes performances of the model before training to have ground basis for it to be compared once trained
    list_loss_train.append(trainer.mean_loss(train_loader)) #performances on loss
    list_loss_test.append(trainer.mean_loss(test_loader))
    
    for epoch in range(epochs):
        
        #displays progression of the training
        #with open('../log/'+ path_save.split('/')[-1] + '_unet_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p'.format(train_loader.batch_size, criterion, model.dropout_conv, model.dropout_fully), 'w') as file:
                #file.write('\n'.join(('Epoch : {}'.format(epoch+1), 'Best loss on train: {}, Best loss on val: {}, Best F1 on val: {}'.format(min(list_loss_train),min(list_loss_test), max(list_f1_test)))))
        print('\n'.join(('Epoch : {}'.format(epoch+1), 'Best loss on train: {}, Best loss on val: {}'.format(min(list_loss_train),min(list_loss_test)))))
        
        #performs training and evaluation of the loss on train set and validation set
        list_loss_train.append(trainer.train(train_loader)) #performances on loss + train
        list_loss_test.append(trainer.mean_loss(test_loader))
        
        #displaying model performances at epoch
        print('Epoch: {}, Loss on train: {}, Loss on val: {}'.format(epoch,list_loss_train[-1], list_loss_test[-1]))
        
        #saving model at concerned epoch
        torch.save(trainer.model, path_save + '/unets' + '/unet_epoch={}_batch={}_loss_{}_dropconv_{}_dropupconv_{}.pt'.format(epoch, train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv))
        
        #saving the losses computed from the beginning (on train and validation)
        sf.save(path_save + '/unets' + '/loss_train_batch_{}_loss_{}_dropconv_{}_dropupconv_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv), list_loss_train)
        sf.save(path_save + '/unets' + '/loss_valid_batch_{}_loss_{}_dropconv_{}_dropupconv_{}.p'.format(train_loader.batch_size, criterion, trainer.model.dropout_conv, trainer.model.dropout_upconv), list_loss_test)
        
    return list_loss_train,list_loss_test

def test(model, data_loader, classes = [0, 2, 3], jaccard=True, F1 = True, ROC = True, threshold = 0, n_points_ROC= 100, n_points_F1 = 100, crop_size = None):
    
    """Function designed to test performances of a neural network model for segmentation task"""

    cuda = True if torch.cuda.is_available() else False
    print("cuda %s"%(cuda))  # check if GPU is used 

    # Tensor type (put everything on GPU if possible)
    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
    LongTensor = torch.cuda.LongTensor if cuda else torch.LongTensor
    
    model.eval()
    
    for i, batch in tqdm(enumerate(data_loader)):
        #preparing inputs
        images = Variable(batch["Images"].type(Tensor),requires_grad=False)
        
        labels = batch["Labels"]
            
        #selecting only labels of interest
        for label in np.unique(labels):
            if label not in classes :
                labels[labels==label] = 0
            else :
                labels[labels==label] = classes.index(label)

        labels = Variable(labels.type(LongTensor),requires_grad=False)
            
        images = images.permute(0,3,1,2)

        #computes forward pass
        outputs = model(images)

        #reshaping output
        if crop_size is not None :
            outputs = outputs[:,:, (outputs.size(2)-crop_size)//2:(outputs.size(2)-crop_size)//2+crop_size, (outputs.size(3)-crop_size)//2:(outputs.size(3)-crop_size)//2+crop_size]
            labels = labels[:, (labels.size(1)-crop_size)//2:(labels.size(1)-crop_size)//2+crop_size, (labels.size(2)-crop_size)//2:(labels.size(2)-crop_size)//2+crop_size]
        outputs = outputs.permute(0, 2, 3, 1)
        outputs = nn.Softmax(dim = -1)(outputs)

        #computing Jaccard Index
        for j, (output,label) in enumerate(zip(outputs,labels)):
            
            with torch.no_grad():
                output = np.array(output.cpu().detach())
                label = np.array(label.cpu().detach())
                
                if jaccard :
                    if i == 0 and j == 0 :
                        jacc = JaccardIndex(output,label,labels=[i for i in range(len(classes))])
                    else :
                        jacc += JaccardIndex(output,label,labels=[i for i in range(len(classes))])

                if F1 :
                    if i == 0 and j == 0 :
                        fb = FB_Score(output,label,labels=[i for i in range(len(classes))], threshold = threshold)
                    else :
                        fb += FB_Score(output,label,labels=[i for i in range(len(classes))], threshold = threshold)
                    
                if ROC :    
                    if i == 0 and j == 0 :
                        roc = ROC_curve(output,label,labels=[i for i in range(len(classes))], threshold = threshold, n_points = n_points_ROC)
                    else :
                        roc += ROC_curve(output,label,labels=[i for i in range(len(classes))], threshold = threshold, n_points = n_points_ROC)
                if F1 :    
                    if i == 0 and j == 0 :
                        f1 = F1_thresh(output,label,labels=[i for i in range(len(classes))], threshold = threshold, n_points = n_points_F1)
                    else :
                        f1 += F1_thresh(output,label,labels=[i for i in range(len(classes))], threshold = threshold, n_points = n_points_F1)
    output = dict()
    if jaccard :
        jacc_d = ['T:{0:.2f}'.format(jacc.confusion_matrix[1,1]),'P:{0:.2f}'.format(jacc.confusion_matrix[2,2]),'O:{0:.2f}'.format(jacc.confusion_matrix[0,0])]
        output['Jaccard'] = jacc_d
        
    if F1 :
        fb_d = ['T:{0:.2f}'.format(fb.confusion_matrix_fb[1,1]),'P:{0:.2f}'.format(fb.confusion_matrix_fb[2,2]),'O:{0:.2f}'.format(fb.confusion_matrix_fb[0,0])]
        output['Fb-score'] = fb_d
        
        pre_d = ['T:{0:.2f}'.format(fb.confusion_matrix_p[1,1]),'P:{0:.2f}'.format(fb.confusion_matrix_p[2,2]),'O:{0:.2f}'.format(fb.confusion_matrix_p[0,0])]
        output['Precision'] = pre_d
        
        rec_d = ['T:{0:.2f}'.format(fb.confusion_matrix_r[1,1]),'P:{0:.2f}'.format(fb.confusion_matrix_r[2,2]),'O:{0:.2f}'.format(fb.confusion_matrix_r[0,0])]
        output['Recall'] = rec_d
        
    if ROC :
        AUC = ['T:{0:.2f}'.format(roc.AUC[1]),'P:{0:.2f}'.format(roc.AUC[2]),'O:{0:.2f}'.format(roc.AUC[0])]
        output['AUC'] = AUC
        print(roc)
    
    if F1 :
        output['f1'] = f1.__repr__()
        print(f1)

    return output

def displayResult(model, image, label, slide, magnification, coordinates, thresh_1 = 0.5, thresh_2 = 0.5, classes = [0,2,3], crop_size = None):
    
    # Tensor type (put everything on GPU if possible)
    cuda = True if torch.cuda.is_available() else False
    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
    image = Variable(image.type(Tensor),requires_grad=False)
    
    #computing model prediction
    image = image.reshape(tuple([1] + list(image.shape)))
    image = image.permute(0, 3, 1, 2)
    output = model(image)
    image = image.permute(0, 2, 3, 1)
    output = output.permute(0, 2, 3, 1)
    output = output.reshape(output.shape[1:])
    image = image.reshape(image.shape[1:])
    
    #selecting only labels of interest
    for lab in np.unique(label):
        if lab not in classes :
            label[label==lab] = 0
        else :
            label[label==lab] = classes.index(lab)
            
    with torch.no_grad():
        label = np.array(label.cpu())

        #reshaping prediction made
        output = nn.Softmax(dim = -1)(output)
        output = np.array(output.cpu())
        
    #cropping
    if crop_size is not None :
        label_crop = label[(label.shape[0]-crop_size)//2:(label.shape[0]-crop_size)//2+crop_size,(label.shape[1]-crop_size)//2:(label.shape[1]-crop_size)//2+crop_size]
        output_crop = output[(output.shape[0]-crop_size)//2:(output.shape[0]-crop_size)//2+crop_size,(output.shape[1]-crop_size)//2:(output.shape[1]-crop_size)//2+crop_size,:]
   
    pad_x = (image.size(0) - output_crop.shape[0])//2
    pad_y = (image.size(1) - output_crop.shape[1])//2
    
    output_crop = np.pad(output_crop, ((pad_x, pad_x), (pad_y, pad_y), (0,0)), 'constant')
    label_crop = np.pad(label_crop, ((pad_x, pad_x), (pad_y, pad_y)), 'constant')
    #output_thresholded = np.argmax(output, -1)
    
    output_thresholded = np.dstack((output_crop[:,:,1], output_crop[:,:,2]>=thresh_1, output_crop[:,:,3]>=thresh_2))

    #retrieving output display
    plots_out = []
    for lab in [1,2] :
        plots_out.append(cv2.findContours((output_thresholded[:,:,lab]).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0])
    
    #retrieving labels display
    plots_lab = []
    for lab in [1,2] :
        plots_lab.append(cv2.findContours((label_crop==lab).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0])
    
    
    #displays the result
    fig, ax = plt.subplots(2, 2, figsize = (18, 18))
    
    #display the image
    with torch.no_grad():
        image = np.array(image.cpu()).astype(int)
        image_to_display = DataSetSlides()[slide][magnification](*coordinates, read_mask = False, read_labels = False)
        ax[0,0].imshow(image_to_display.array_region, extent = [0,image.shape[1]-1,image.shape[0]-1,0])
        ax[0,0].plot([(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2+crop_size,(label.shape[0]-crop_size)//2 + crop_size,(label.shape[0]-crop_size)//2],[(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2,],color='black')
    
    #displays output
    colors_out = ['blue', 'red']
    legends = ['Predicted Tangles', 'Predicted Plaques']
    for i, list_plot in enumerate(plots_out):
        for j, plot in enumerate(list_plot):
            plot = plot.reshape(-1, 2)
            plot = np.concatenate((plot, plot[0].reshape(-1, 2)), 0)
            ax[0,0].plot(plot[:,0], plot[:,1], color = colors_out[i], label = legends[i] + ' ' + str(j+1))
            
    #displays target
    colors_label = ['green', 'orange']
    legends = ['Ground-Truth Tangles', 'Ground-Truth Plaques']
    for i, list_plot in enumerate(plots_lab):
        for j, plot in enumerate(list_plot):
            plot = plot.reshape(-1, 2)
            plot = np.concatenate((plot, plot[0].reshape(-1, 2)), 0)
            ax[0,0].plot(plot[:,0], plot[:,1], color = colors_label[i], label = legends[i] + ' ' + str(j+1))
    
    im = ax[0,1].imshow(output[:,:,2], cmap = 'RdYlGn', vmin = 0., vmax = 1.)
    ax[0,1].plot([(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2+crop_size,(label.shape[0]-crop_size)//2 + crop_size,(label.shape[0]-crop_size)//2],[(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2,],color='black')
    divider = make_axes_locatable(ax[0,1])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig.colorbar(im, cax=cax, orientation='vertical')
    cbar.set_label('Probability of being a tangle', rotation=270)
    ax[0,1].set_xlabel('x')
    ax[0,1].set_ylabel('y')
        
    im = ax[1,1].imshow(output[:,:,3], cmap = 'RdYlGn', vmin = 0., vmax = 1.)
    ax[1,1].plot([(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2+crop_size,(label.shape[0]-crop_size)//2 + crop_size,(label.shape[0]-crop_size)//2],[(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2,],color='black')
    divider = make_axes_locatable(ax[1,1])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig.colorbar(im, cax=cax, orientation='vertical')
    cbar.set_label('Probability of being background', rotation=270)
    ax[0,1].set_xlabel('x')
    ax[0,1].set_ylabel('y')
    
    im = ax[1,0].imshow(output[:,:,1], cmap = 'RdYlGn', vmin = 0., vmax = 1.)
    ax[1,0].plot([(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2,(label.shape[0]-crop_size)//2+crop_size,(label.shape[0]-crop_size)//2 + crop_size,(label.shape[0]-crop_size)//2],[(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2 + crop_size,(label.shape[1]-crop_size)//2,(label.shape[1]-crop_size)//2,],color='black')
    divider = make_axes_locatable(ax[1,0])
    cax = divider.append_axes('right', size='5%', pad=0.05)
    cbar = fig.colorbar(im, cax=cax, orientation='vertical')
    cbar.set_label('Probability of being a plaque', rotation=270)
    ax[0,1].set_xlabel('x')
    ax[0,1].set_ylabel('y')
    
    fig.legend(loc="center right")
    plt.subplots_adjust(bottom = 0.15, right=0.85)
    
    return image, output
    
    
def displayResultList(model, dataloader, classes = [0,2,3], plot_tangles = True, plot_plaques = False, number_examples = 10, padding = 5, threshold_size = 15, thresh_1=0.5, thresh_2=0.5):
    
    cuda = True if torch.cuda.is_available() else False
    Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor
    
    displaying_tangles_list = list()
    displaying_plaques_list = list()
    
    for i, batch in tqdm(enumerate(dataloader)):
        # Tensor type (put everything on GPU if possible)
        images = Variable(batch["Images"].type(Tensor),requires_grad=False)
        
        labels = batch["Labels"]
        
        #computing model prediction
        images = images.permute(0, 3, 1, 2)
        output = model(images)
        images = images.permute(0, 2, 3, 1)
        output = output.permute(0, 2, 3, 1)
        output = output.reshape(output.shape[1:])

        #selecting only labels of interest
        for lab in np.unique(labels):
            if lab not in classes :
                labels[labels==lab] = 0
            else :
                labels[labels==lab] = classes.index(lab)

        with torch.no_grad():
            labels = np.array(labels.cpu().detach()).reshape(labels.shape[1:])
            images = np.array(images.cpu().detach())
            output = np.array(output.cpu().detach())
        images = images.reshape(images.shape[1:])
            
        #output = np.argmax(output, -1)
        pad_x = (images.shape[0] - output.shape[0])//2
        pad_y = (images.shape[1] - output.shape[1])//2

        output = np.pad(output, ((pad_x, pad_x), (pad_y, pad_y), (0,0)), 'constant')
        labels = np.pad(labels, ((pad_x, pad_x), (pad_y, pad_y)), 'constant')

        output = np.dstack((output[:,:,0], output[:,:,1]>=thresh_1, output[:,:,2]>=thresh_2))

        #retrieving labels display
        plots_lab = []
        for lab in [1,2] :
            if lab == 1:
                color = [0,255,0,255]
            else :
                color = [255,127,80,255]
            
            contours = cv2.findContours((labels==lab).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
            cv2.drawContours(images, contours, -1, color, 1)

        #retrieving output display
        plots_out = []
        for lab in [1,2] :
            if lab == 1:
                color = [0,0,255,255]
            else :
                color = [255,0,0,255]
            
            contours = cv2.findContours((output[:,:,lab]).astype(np.uint8), cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)[0]
            cv2.drawContours(images, contours, -1, color, 1)
            
            #we only want to display relevant parts of the image
            nb_cc, cc = cv2.connectedComponents((output[:,:,lab]).astype(np.uint8))
            for i in range(1,nb_cc):
                obj = (cc==i)
                
                line = np.any(obj, axis = 0)
                col = np.any(obj, axis = 1)
                min_x, max_x = min(np.where(line)[0]), max(np.where(line)[0])
                min_y, max_y = min(np.where(col)[0]), max(np.where(col)[0])
                
                if lab == 1:
                    im_to_append = images[min_y-padding:max_y+padding, min_x-padding:max_x+padding, :].astype(int)
                    if min(im_to_append.shape[:2]) >= threshold_size :
                        displaying_tangles_list.append(im_to_append)
                if lab == 2:
                    im_to_append = images[min_y-padding:max_y+padding, min_x-padding:max_x+padding, :].astype(int)
                    if min(im_to_append.shape[:2]) >= threshold_size :
                        displaying_plaques_list.append(im_to_append)
                
    
    if plot_tangles :
        nb_plots = len(displaying_tangles_list[:number_examples])
        square_size = int(np.ceil(np.sqrt(nb_plots)))
        
        #displays the result
        fig, ax = plt.subplots(square_size, square_size, figsize = (18, 18))

        for l in range(nb_plots):

            #displays the image
            ax[l//square_size, l%square_size].imshow(displaying_tangles_list[l])

        plt.show()
    if plot_plaques :
        
        nb_plots = len(displaying_plaques_list[:number_examples])
        square_size = int(np.ceil(np.sqrt(nb_plots)))
        
        #displays the result
        fig, ax = plt.subplots(square_size, square_size, figsize = (18, 18))

        for l in range(nb_plots):

            #displays the image
            ax[l//square_size, l%square_size].imshow(displaying_plaques_list[l])
        plt.show()
    
    return displaying_tangles_list, displaying_plaques_list
    
class Util():
    
    @classmethod
    def forward_pass(self, images, model, probability = False):
        
        #reshaping image in the right size if necessary
        if len(images.shape) < 4:
            images = images.reshape(1, *images.shape).type(Tensor)

        #computes forward pass
        outputs = model(images.permute(0,3,1,2))
        
        #if a probability is wanted, ensures to give it
        outputs = nn.Softmax(dim = -1)(outputs) if probability else outputs

        return outputs

    @classmethod
    def shape_sample(self, sample):
        # checks if GPU is available
        cuda = True if torch.cuda.is_available() else False

        # Input tensors are given a suitable type, and put on the GPU if possible
        Tensor = torch.cuda.FloatTensor if cuda else torch.FloatTensor 
        LongTensor = torch.cuda.LongTensor if cuda else torch.LongTensor
        
        #giving the right type to the sample
        images = Variable(sample["Images"].type(Tensor))
        labels = Variable(sample["Labels"].type(LongTensor))
        
        return images, labels

class Trainer():
    
    def __init__(self, model, path_save, criterion, optimizer):
        
        cuda = True if torch.cuda.is_available() else False
        
        self.model = model.cuda() if cuda else model.cpu()
        self.path_save = path_save
        self.criterion = criterion.cuda() if cuda else criterion.cpu()
        self.optimizer = optimizer
        
    def train(self, data_loader):
        
        #puts model in training mode
        self.model.train()
        
        #list of losses to be computed
        losses = list()
        
        #passes all data through the model
        for i,batch in enumerate(data_loader) :
            
            #preparing inputs
            images, labels = Util.shape_sample(batch)
           
            #zeroes gradients in the optimizer
            self.optimizer.zero_grad()
            
            #computes forward pass
            outputs = Util.forward_pass(images, self.model)
            
            #computing loss
            loss = self.criterion(outputs, labels)
            
            #memorises the loss updates
            losses.append(loss.cpu().detach())
            
            #backward pass
            loss.backward()
            self.optimizer.step()
            
            #clears memory
            torch.cuda.empty_cache()
            
        return sum(losses)/len(losses)
        
    def mean_loss(self, data_loader):

        #puts model in evaluation mode
        self.model.eval()
        
        #prevents any gradient to be computed
        with torch.no_grad():

            #list of losses to be computed
            losses = list()
            for i,batch in enumerate(data_loader) :
                
                #preparing inputs
                images, labels = Util.shape_sample(batch)
                
                #computes forward pass
                outputs = Util.forward_pass(images, self.model)

                #computing loss
                loss = self.criterion(outputs, labels)

                #memorises the loss updates
                losses.append(loss.cpu().detach())

                #clears memory
                torch.cuda.empty_cache()
               
        #returns average loss
        return sum(losses)/len(losses) 