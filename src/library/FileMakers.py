#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  4 15:06:22 2020

@author: valentinabadie
"""

#import scipy.io
import os

class FileMakers(object):
    
    """A class to generate .py files to be exploited via slurm, these are examples of use of the rest of the library.
    
    Methods :
        dataset : writes a .py file to be used to generate datasets
        learning : writes a .py file to be used to run a learning for region classification
        learning_segmentation : writes a .py file to be used to run a learning for region segmentation
    
    """
    
    @classmethod
    def dataset(self, slide, magnification, size, path_to_data, ROI = 2, stride = None, random = True, balance = 0.5, tolerance = None, random_pos = True, path_slides = '../data/slides/', path_labels = '../data/labels/', path_infos = '../data/info_slides/'):
        
        """writes a .py file to be used to generate datasets
        
        Inputs :
            slide : str, key of the slide to be used for computing the dataset
            magnification : str, magnification level wanted
            size : int, the size of the images in the dataset to be generated
            path_to_data : str, the path to the folder were the dataset is to be saved
            stride : 2-tuple, the stride between 2 consecutive regions
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
            
            path_slides, path_labels, path_infos : paths to the slides information
        
        """
        
        if not isinstance(slide, list):
            slide = [slide]
            
        if not isinstance(magnification, list):
            magnification = [magnification]
        
        if tolerance is None :
            tolerance = size
       
        if stride is None:
            stride = size
        
        
        #this list is going to be contain the text to be written in the .py file
        list_lines = list()
        
        #loads useful modules
        list_lines.append('from library.DataSetGenerator import DataSetGenerator')
        
        #initialises the dataset
        list_lines.append('data_set = DataSetGenerator(path_slides="{}",path_labels="{}",path_infos="{}",slides = {}, magnification={}, size = {}, stride = {}, ROI={}, tolerance = {}, random_pos = {}, balance = {}, random = {})'.format(path_slides, path_labels, path_infos, slide, magnification, size, stride, ROI, tolerance, random_pos, balance, random))
        list_lines.append('data_set.set_path("{}")'.format(path_to_data))
        
        #saves it
        list_lines.append('data_set.save()')
        
        #writes the code and put it in the file dataset.py
        code = '\n'.join(list_lines)
        file = open('dataset.py','w')
        file.write(code)
        file.close()
        
    @classmethod
    def data_normalisation(self, path_to_data, target_id, target_magnification, target_coords, path_classifier):
        
        """writes a .py file to be used to generate datasets
        
        Inputs :
            path_to_data : str, the path to the folder were the dataset is to be loaded and then saved in subfolders
            target_id : str/int, chooses on which slide(s) the target region for normalisation is selected
            target_magnification str/int, chooses at which magnification this region should be
            target_coords : 4-tuple of floats, sets the coordinates of the target region
            path_classifier : str, the path to the matlab object (.mat) containing the color classifier
        
        """
        
        #this list is going to be contain the text to be written in the .py file
        list_lines = list()
        
        list_lines.append('import matlab.engine')
        list_lines.append('import matlab')    
        list_lines.append('from library.SlidesHandling import DataSetSlides')
        list_lines.append('from library.DataSetGenerator import Normalization')
        list_lines.append('from library.utils import SaveFiles as sf')
        
        #booting matlab engine
        list_lines.append('eng = matlab.engine.start_matlab()')
        list_lines.append('eng.addpath(eng.genpath("matlab_scripts"), nargout = 0)')
        
        #computing the region on which to perform normalization
        list_lines.append("target = DataSetSlides('../data/slides/', path_labels = '../data/labels/', path_infos = '../data/info_slides/')['{}']['{}']({},{},{},{},read_labels=False)".format(target_id, target_magnification, *target_coords))
        
        #retrieving the dataset
        list_lines.append("data_set = sf.load('/'.join(('{}','Dataset.p')))".format(path_to_data))
        
        #normalises the data
        list_lines.append("data_set_norm = Normalization(data_set)")
        list_lines.append("data_set_norm.save(eng, matlab, target, '{}')".format(path_classifier))
        
        #closes the MATLAB environment
        list_lines.append("eng.quit()")

        #writes the code and put it in the file dataset.py
        code = '\n'.join(list_lines)
        file = open('dataset_normalisation.py','w')
        file.write(code)
        file.close()
        
    @classmethod
    def learning(self, slide, magnification, ROI, size, paths_to_data, path_to_model, in_channels=4, out_channels=4, depth=5, dropout_conv=0., dropout_fully=0., epochs=10, criterion='nn.CrossEntropyLoss()', tolerance = None, random_pos = True, rotation90 = False, rotation180 = False, rotation270 = False, flip = False, batch_size = 10):
        
        
        """writes a .py file to be used to train classifier models
        
        Inputs :
            slide : str, key of the slide to be used for computing the dataset
            magnification : str, magnification level wanted
            size : int, the size of the images in the dataset to be generated
            path_to_data : str, the path to the folder were the dataset is to be saved
            stride : 2-tuple, the stride between 2 consecutive regions
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
            
            paths_to_data : paths to the folders where data to be used is stored
            path_to_model : path to the folder where the model is to be stored
            
            in_channels : int, number of input channels
            out_channels : int, number of output channels
            depth : int, the depth of the network (number of downsampling layers)
            dropout_conv : float, dropout to be applied to every convolutional layers
            dropout_fully : float, dropout to be applied to every fully-connected layers
            
            criterion : nn.Module object, the loss to be used to train the model
            
            rotation90 : bool, if True performs rotation by 90° data augmentation. Default : False.
            rotation180 : bool, if True performs rotation by 180° data augmentation. Default : False.
            rotation270 : bool, if True performs rotation by 270° data augmentation. Default : False.
            flip : bool, if True performs flip left to right data augmentation. Default : False.
            
            epochs : int, number of epochs along which the model is to be trained
            batch_size : int, the size of batchs of data to be passed altogether to the model
        
        """
        
        if tolerance is None :
            tolerance = size
            
        #this list is going to be contain the text to be written in the .py file
        list_lines = list()
        
        #importing modules
        list_lines.append('import matlab.engine')
        list_lines.append('import matlab')    
        list_lines.append('import os')
        list_lines.append('import numpy as np')
        list_lines.append('from torch.utils.data import DataLoader')
        list_lines.append('import torch')
        list_lines.append('from torch import nn')
        list_lines.append('from library.Training import train_classifier')
        list_lines.append('from library.Models import DetectionCNN, FocalLoss, CrossEntropyLoss')
        list_lines.append('from library.DataSetGenerator import *')
        list_lines.append('from library.utils import SaveFiles as sf')
        
        #booting matlab engine
        list_lines.append('eng = matlab.engine.start_matlab()')
        list_lines.append('eng.addpath(eng.genpath("matlab_scripts"), nargout = 0)')
        
        #defining loss to be used
        list_lines.append("criterion = {}".format(criterion))
        
        #datasets initialisation and normalisation
        list_lines.append("data_sets = list()")
        list_lines.append("for path_to_data in {} :".format(paths_to_data))
        list_lines.append("    data_sets.append(sf.load(''.join((path_to_data,'/Dataset.p'))))")
        list_lines.append("    data_sets[-1].set_path(path_to_data)")
        list_lines.append("    data_sets[-1]._show_coord = False")
        list_lines.append("    data_sets[-1].load = True #specifies that the dataset has been loaded")
        list_lines.append("    data_sets[-1].aug = False")
        list_lines.append("    data_sets[-1] = Normalization(data_sets[-1], eng, matlab, target = np.array(data_sets[0][0]['Images']))")

        #fusionning then splitting the datasets
        list_lines.append('data_set_train, data_set_test = MultiDataSplitter(data_sets).datasets["train"],MultiDataSplitter(data_sets).datasets["test"]')   

        #performing data augmentation and converting dataset to regular classification
        list_lines.append("train_data_aug = DataAugmentation(data_set_train, rotation90 = {}, rotation180 = {}, rotation270 = {}, flip = {})".format(rotation90, rotation180, rotation270, flip))
        list_lines.append("train_data_detec = DetectionOnly(train_data_aug, label = {})".format(ROI))
        list_lines.append("test_data_detec = DetectionOnly(data_set_test, label = {})".format(ROI))

        #converting in DataLoader format
        list_lines.append("inputs_train = DataLoader(train_data_detec,batch_size={},shuffle=True)".format(batch_size))
        list_lines.append("inputs_test = DataLoader(test_data_detec,batch_size=1,shuffle=False)")

        list_lines.append("print('Model : Magnification = {}, Size = {}, ROI = {}, Tolerance = {}, Depth = {}')".format(magnification, size, ROI, tolerance, depth))

        #defining model
        list_lines.append("model = DetectionCNN(in_size = {}, out_size = {}, depth = {}, dropout_conv={}, dropout_fully={})".format(in_channels, out_channels, depth, dropout_conv, dropout_fully))

        #performing training
        list_lines.append("_,_,list_loss_test, list_f1 = train_classifier(model, inputs_train, inputs_test, epochs = {}, path_save='{}', criterion = criterion)".format(epochs, path_to_model))

        list_lines.append("print('Model : Magnification = {}, Size = {}, ROI = {}, Depth = {}, Tolerance = {}')".format(magnification, size, ROI, depth, tolerance))

        #retrieves at which epoch the model has performed the best performances, according to loss on validation or f1 score
        list_lines.append("best_epoch_val = np.argmin(list_loss_test)")
        list_lines.append("best_epoch_f1 = np.argmax(list_f1)")

        #deletes all other epochs
        list_lines.append("for epoch in range(epochs):")
        list_lines.append("    if epoch != best_epoch_val and epoch != best_epoch_f1:")
        list_lines.append("        os.remove('{}' + '/models_detect' + '/cnn_epoch={}_batch={}_loss_{}_dropconv_{}_dropfully_{}.pt'.format(criterion))".format(path_to_model, epochs, batch_size, '{}', dropout_conv, dropout_fully))
        list_lines.append("    else :")
        list_lines.append("        path_save = '{}' + '/models_detect' + '/cnn_epoch={}_batch={}_loss_{}_dropconv_{}_dropfully_{}.pt'.format(criterion)".format(path_to_model, epochs, batch_size, '{}', dropout_conv, dropout_fully))
        list_lines.append("        torch.save(torch.load(path_save).cpu(), path_save)")

        #writes the code and puts it in the file dataset.py
        code = '\n'.join(list_lines)
        file = open('learning.py','w')
        file.write(code)
        file.close()
        
    @classmethod
    def learning_segmentation(self, slide, magnification, ROI, size, paths_to_data, path_to_model, in_channels=4, out_channels=2, depth=5, dropout_conv=0., dropout_upconv=0., epochs=10, criterion='nn.CrossEntropyLoss()', tolerance = None, random_pos = True, rotation90 = False, rotation180 = False, rotation270 = False, flip = False, batch_size = 10):
        
        """writes a .py file to be used to train classifier models
        
        Inputs :
            slide : str, key of the slide to be used for computing the dataset
            magnification : str, magnification level wanted
            size : int, the size of the images in the dataset to be generated
            path_to_data : str, the path to the folder were the dataset is to be saved
            stride : 2-tuple, the stride between 2 consecutive regions
            ROI : int, if >0 then only triggers the regions which intersects the label corresping to the interger passed.                     Default 1
            tolerance : int, in case of negative sample generation (i.e. no objects in it), say at which point the negative sample is needed to be negative, i.e. at what distance (in terms of infinite norm) the nearest object has to be away from the center of the sample.
            random_pos : bool. If true, positive samples are generated with the object of interest at a random position on the region, else in the center of it. For more details, see report.
            balance : float. The balance between the number of positive and negative samples to be generated.
            
            paths_to_data : paths to the folders where data to be used is stored
            path_to_model : path to the folder where the model is to be stored
            
            in_channels : int, number of input channels
            out_channels : int, number of output channels
            depth : int, the depth of the network (number of downsampling layers)
            dropout_conv : float, dropout to be applied to every downsampling layers
            dropout_upconv : float, dropout to be applied to every upsampling layers
            
            criterion : nn.Module object, the loss to be used to train the model
            
            rotation90 : bool, if True performs rotation by 90° data augmentation. Default : False.
            rotation180 : bool, if True performs rotation by 180° data augmentation. Default : False.
            rotation270 : bool, if True performs rotation by 270° data augmentation. Default : False.
            flip : bool, if True performs flip left to right data augmentation. Default : False.
            
            epochs : int, number of epochs along which the model is to be trained
            batch_size : int, the size of batchs of data to be passed altogether to the model
        
        """
        
        if tolerance is None :
            tolerance = size

        #this list is going to be contain the text to be written in the .py file
        list_lines = list()
        
        #importing modules
        list_lines.append('import matlab.engine')
        list_lines.append('import matlab') 
        list_lines.append('import os')
        list_lines.append('import numpy as np')
        list_lines.append('from torch.utils.data import DataLoader')
        list_lines.append('import torch')
        list_lines.append('from torch import nn')
        list_lines.append('from library.Training import train_segmenter, test')
        list_lines.append('from library.Models import UNet, FocalLoss, CrossEntropyLoss')
        list_lines.append('from library.DataSetGenerator import *')
        list_lines.append('from library.utils import SaveFiles as sf')

        #defining loss to be used
        list_lines.append("criterion = {}".format(criterion))
        
        #booting matlab engine
        list_lines.append('eng = matlab.engine.start_matlab()')
        list_lines.append('eng.addpath(eng.genpath("matlab_scripts"), nargout = 0)')
        
        #datasets initialisation and normalisation
        list_lines.append("data_sets = list()")
        list_lines.append("for path_to_data in {} :".format(paths_to_data))
        list_lines.append("    data_sets.append(sf.load(''.join((path_to_data,'/Dataset.p'))))")
        list_lines.append("    data_sets[-1].set_path(path_to_data)")
        list_lines.append("    data_sets[-1]._show_coord = False")
        list_lines.append("    data_sets[-1].load = True #specifies that the dataset has been loaded")
        list_lines.append("    data_sets[-1].aug = False")
        list_lines.append("    data_sets[-1] = Normalization(data_sets[-1], eng, matlab, target = np.array(data_sets[0][0]['Images']))")

         #fusionning then splitting the datasets
        list_lines.append('data_set_train, data_set_test = MultiDataSplitter(data_sets).datasets["train"],MultiDataSplitter(data_sets).datasets["test"]')   

        #performing data augmentation and converting dataset to regular classification
        list_lines.append("train_data_aug = DataAugmentation(data_set_train, rotation90 = {}, rotation180 = {}, rotation270 = {}, flip = {})".format(rotation90, rotation180, rotation270, flip))
        list_lines.append("train_data_sel = LabelSelect(train_data_aug, labels = {})".format(ROI))
        list_lines.append("test_data_sel = LabelSelect(data_set_test, labels = {})".format(ROI))

        #formatting dataset
        list_lines.append("inputs_train = DataLoader(train_data_sel,batch_size={},shuffle=True)".format(batch_size))
        list_lines.append("inputs_test = DataLoader(test_data_sel,batch_size=1,shuffle=False)")

        list_lines.append("print('Model : Magnification = {}, Size = {}, ROI = {}, Tolerance = {}, Depth = {}')".format(magnification, size, ROI, tolerance, depth))
        
        #defining model
        list_lines.append("model = UNet(in_size = {}, out_size = {}, depth = {}, dropout_conv={}, dropout_upconv={})".format(in_channels, out_channels, depth, dropout_conv, dropout_upconv))

        #performing training
        list_lines.append("_,list_loss_test = train_segmenter(model, inputs_train, inputs_test, epochs = {}, path_save='{}', criterion = criterion, classes = {})".format(epochs, path_to_model, [0] + ROI))

        list_lines.append("print('Model : Magnification = {}, Size = {}, ROI = {}, Depth = {}, Tolerance = {}')".format(magnification, size, ROI, depth, tolerance))

        #retrieving best model according to validation loss
        list_lines.append("best_epoch_val = np.argmin(list_loss_test)")

        #deleting all other epochs
        list_lines.append("for epoch in range(epochs):")
        list_lines.append("    if epoch != best_epoch_val :") # and epoch != best_epoch_f1:")
        list_lines.append("        os.remove('{}' + '/models_detect' + '/unet_epoch={}_batch={}_loss_{}_dropconv_{}_dropupconv_{}.pt'.format(criterion))".format(path_to_model, epochs, batch_size, '{}', dropout_conv, dropout_upconv))
        list_lines.append("    else :")
        list_lines.append("        path_save = '{}' + '/models_detect' + '/unet_epoch={}_batch={}_loss_{}_dropconv_{}_dropupconv_{}.pt'.format(criterion)".format(path_to_model, epochs, batch_size, '{}', dropout_conv, dropout_upconv))
        list_lines.append("        torch.save(torch.load(path_save).cpu(), path_save)")

        #writes the code and puts it in the file dataset.py
        code = '\n'.join(list_lines)
        file = open('learning_segmentation.py','w')
        file.write(code)
        file.close()
        
        
    @classmethod
    def real_implementation(self, slide, magnification, ROI, size, path_to_data_train, path_to_data_test, dropout_conv=0., dropout_fully=0., criterion='nn.CrossEntropyLoss()', batch_size = 10, choice_metric=0): 
        
        list_lines = list()
        
        list_lines.append("import numpy as np")
        list_lines.append("import torch")
        list_lines.append("from torch import nn")
        list_lines.append("from library.DataSetLearning import *")
        list_lines.append("from library.utils import SaveFiles as sf")
        list_lines.append("from library.DeepLearningDetection import test_real_implementation")
        
        #loading the dataset
        list_lines.append("data_set = sf.load('{}/Dataset.p')".format(path_to_data_test))
        list_lines.append("data_set.set_path('{}')".format(path_to_data_test))
        list_lines.append("data_set.load = True")
        list_lines.append("data_set._show_coord = True")
        list_lines.append('data_set.aug = False')
        list_lines.append("data_set = DetectionOnly(data_set, label = {})".format(ROI))

        #loads the best model
        list_lines.append("loss_on_valid = sf.load('{}/models_detect/loss_valid_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p')".format(path_to_data_train, batch_size, criterion, dropout_conv, dropout_fully))
        list_lines.append("f1_on_valid = sf.load('{}/models_detect/f1_valid_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p')".format(path_to_data_train, batch_size, criterion, dropout_conv, dropout_fully))

        #printing best epoch performed on validation loss
        list_lines.append("if {} == 0:".format(choice_metric))
        list_lines.append("    best_epoch = np.argmin(loss_on_valid[1])")
        list_lines.append("else :")
        list_lines.append("    best_epoch = np.argmax(f1_on_valid[1])")

        #loading the model corresponding to this epoch
        list_lines.append("best_model = torch.load('{}/models_detect/cnn_epoch={}_batch={}_loss_{}_dropconv_{}_dropfully_{}.pt'.format(best_epoch))".format(path_to_data_train, '{}', batch_size, criterion, dropout_conv, dropout_fully))
        list_lines.append("best_model.eval()")
        
        list_lines.append("output_data, labels_data, coords_data = test_real_implementation(data_set, best_model, '{}')".format(path_to_data_test))
        

        list_lines.append("sf.save('{}/models_detect/outputs_{}_batch_{}_loss_{}_dropconv_{}_dropfully_{}.p', output_data)".format(path_to_data_test, path_to_data_train.split('/')[-1], batch_size, criterion, dropout_conv, dropout_fully))
        list_lines.append("sf.save('{}/models_detect/labels.p', labels_data)".format(path_to_data_test))
        list_lines.append("sf.save('{}/models_detect/coords.p', coords_data)".format(path_to_data_test))

        code = '\n'.join(list_lines)

        file = open('real_implementation.py','w')

        file.write(code)
        
        file.close()
        