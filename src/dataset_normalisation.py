"""This file is used to perform color normalisation of a dataset on a target"""

import matlab.engine
import matlab
from library.SlidesHandling import DataSetSlides
from library.DataSetGenerator import Normalization
from library.utils import SaveFiles as sf

#this code uses matlab, so it opens a MATLAB terminal
eng = matlab.engine.start_matlab()
eng.addpath(eng.genpath("matlab_scripts"), nargout = 0) 

#defining the target image on which to perform normalization, here it is an arbitrarily chosen region of the slide A1702121
target = DataSetSlides('../data/slides/', path_labels = '../data/labels/', path_infos = '../data/info_slides/')['A1702121']['x20'](3400,-3800,3540,-3660,read_labels=False)

#load the dataset to be normalized
data_set = sf.load('/'.join(('../data/datasets/DataSet_Slide_A1702121_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_0_Bal_0.5','Dataset.p')))

#initializes normalization
data_set_norm = Normalization(data_set)

#performs normalization and saves the data
data_set_norm.save(eng, matlab, target, 'example_images/trained_classifier.mat')

#quitting matlab
eng.quit()
