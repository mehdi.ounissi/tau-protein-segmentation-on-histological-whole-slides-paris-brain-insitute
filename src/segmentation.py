"""This file performs segmentation with a trained model"""

from library.RealImplementation import SegmentationImplementation
from library.utils import SaveFiles as sf
from library.DataSetGenerator import LabelSelect, DetectionOnly
import numpy as np
import torch
from library.SlidesHandling import DataSetSlides
import os
from library.Morphology import MorphologyStudy

#slide on which model is to be tested
slide = DataSetSlides('../data/slides/', path_labels = '../data/labels/', path_infos = '../data/info_slides/')['A1702114']

#dataset on which model is to be tested
dataset = sf.load(''.join(('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_0_Bal_0.5','/Dataset.p')))

#retrieves region coordinates
region_coords = list()
for region_name in list(filter(lambda x:x[:6] == 'region', os.listdir('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_0_Bal_0.5'))):
    coords = region_name.split('_')[3:]
    coords[-1] = coords[-1][:-2]
    for j,coord in enumerate(coords) :
         coords[j] = float(coord)
    region_coords.append(coords)

#selects region where there already are ROI on it
dataset_det = DetectionOnly(dataset, label = 2)
region_ind = list()
for i, sample in enumerate(dataset_det):
    if sample['Labels'] == 1: 
        region_ind.append(i)

#selects model to evaluate
best_model = torch.load('../models/Segmenter_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_0_Bal_0.5/unets/unet_epoch=9_batch=100_loss_focal_dropconv_0.0_dropupconv_0.0.pt')

#performs segmentation, with postprocessing
segmenter = SegmentationImplementation(region_ind, region_coords, dataset, best_model, labels = slide.labels, ROI = 2)
segmenter.segmentation() 
segmenter.postprocessing(threshold =0.15)#suppress object whose surface is smaller than 15% of the biggest one
segmenter.evaluation() #to be avoided if there is no labels available for the slide - new images segmentation

#displays performances
print(segmenter.precision)
print(segmenter.recall)
print(segmenter.f1_score)

#computes morphological variables out of segmented objects
morphology = MorphologyStudy(segmenter.segmentations ,slide['x20'], object_group = 2)
morphology.output_all_variables(path_to_save = '../table_out.xls')

