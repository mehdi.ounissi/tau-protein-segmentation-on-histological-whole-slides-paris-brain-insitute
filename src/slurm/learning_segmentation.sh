#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=100:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=../
#SBATCH --gres=gpu:1
#SBATCH --output=outputs/learning_segmentation_job_%j.log
#SBATCH --error=outputs/learning_segmentation_job_%j.log
#SBATCH --job-name=learning_segmentation

module load python/3.6
module load MATLAB/R2019b
python learning_segmentation.py
