#!/bin/bash
#SBATCH --job-name=matlab_normalisation_im
#SBATCH --partition=normal
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=matlab_normalisation_im.txt
#SBATCH --error=matlab_error_im.txt

module load MATLAB
matlab -nodesktop -softwareopengl -nosplash -nodisplay < matlab_scripts/image_normalization.m
sleep 5;
