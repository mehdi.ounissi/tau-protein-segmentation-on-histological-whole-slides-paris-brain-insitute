#!/bin/bash
#SBATCH --job-name=matlab_normalisation
#SBATCH --partition=normal
#SBATCH --mem=1G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=matlab_normalisation.txt
#SBATCH --error=matlab_error.txt

module load MATLAB
matlab -nodesktop -softwareopengl -nosplash -nodisplay < ../matlab_scripts/trainingClassifier.m
sleep 5;
