import numpy as np
import torch
from torch import nn
from library.DataSetLearning import *
from library.utils import SaveFiles as sf
from library.DeepLearningDetection import test_real_implementation
data_set = sf.load('../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64/Dataset.p')
data_set.set_path('../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64')
data_set.load = True
data_set._show_coord = True
data_set.aug = False
data_set = DetectionOnly(data_set, label = 2)
loss_on_valid = sf.load('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False/models_detect/loss_valid_batch_100_loss_CrossEntropyLoss()_dropconv_0.0_dropfully_0.0_aug_False.p')
f1_on_valid = sf.load('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False/models_detect/f1_valid_batch_100_loss_CrossEntropyLoss()_dropconv_0.0_dropfully_0.0_aug_False.p')
if 1 == 0:
    best_epoch = np.argmin(loss_on_valid[1])
else :
    best_epoch = np.argmax(f1_on_valid[1])
best_model = torch.load('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False/models_detect/cnn_epoch={}_batch=100_loss_CrossEntropyLoss()_dropconv_0.0_dropfully_0.0_aug_False.pt'.format(best_epoch))
best_model.eval()
output_data, labels_data, coords_data = test_real_implementation(data_set, best_model, '../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64')
sf.save('../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64/models_detect/outputs_DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False_batch_100_loss_CrossEntropyLoss()_dropconv_0.0_dropfully_0.0_aug_False.p', output_data)
sf.save('../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64/models_detect/labels.p', labels_data)
sf.save('../data/datasets/DataSet_Slide_A1702114_Magni_x20_Size_128_Stride_64/models_detect/coords.p', coords_data)