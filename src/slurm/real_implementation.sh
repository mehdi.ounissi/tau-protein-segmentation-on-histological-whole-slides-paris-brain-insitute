#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=10:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --gres=gpu:1
#SBATCH --output=outs/real_time_implementation_job_%j.log
#SBATCH --error=outs/real_time_implementation_job_%j.log
#SBATCH --job-name=real_time_implementation

module load openslide/1.1.1
python real_implementation.py