#!/bin/bash
#SBATCH --partition=normal
#SBATCH --time=300:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=.
#SBATCH --output=outputs/make_dataset_job_%j.log
#SBATCH --error=outputs/make_dataset_job_%j.log
#SBATCH --job-name=make_dataset

module load proxy
module load python/3.6
module load openslide
python dataset.py
