#!/bin/bash
#SBATCH --partition=gpu
#SBATCH --time=1000:00:00
#SBATCH --mem=20G
#SBATCH --cpus-per-task=1
#SBATCH --chdir=../
#SBATCH --gres=gpu:1
#SBATCH --output=outputs/learning_job_%j.log
#SBATCH --error=outputs/learning_job_%j.log
#SBATCH --job-name=learning

module load python/3.6
module load MATLAB/R2019b
python learning.py
