import os
import numpy as np
from torch.utils.data import DataLoader
import torch
from torch import nn
from library.DeepLearningTasks import train, test
from library.Models import UNet
from library.DataSetLearning import *
from library.utils import SaveFiles as sf
from library.Losses import FocalLoss
criterion = FocalLoss(gamma = 2, weights = torch.tensor([2.0, 2.0]))
data_set = sf.load('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False' + '/Dataset.p')
data_set.set_path("../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False")
data_set.load = True
data_set._show_coord = False
data_set.aug = False
data_splitter = DataSplitter(data_set)
train_data = data_splitter.datasets['train']
test_data = data_splitter.datasets['test']
train_data_aug = DataAugmentation(train_data, rotation90 = False, rotation180 = False, rotation270 = False, flip = False)
inputs_train = DataLoader(train_data_aug,batch_size=100,shuffle=True)
inputs_test = DataLoader(test_data,batch_size=1,shuffle=False)
print('Model : Magnification = x20, Size = 128, ROI = 2, Tolerance = 128, Depth = 4')
model = UNet(in_size = 4, out_size = 2, depth = 4, dropout_conv=0.0, dropout_upconv=0.0)
_,_,list_loss_test = train(model, inputs_train, inputs_test, epochs = 50, path_save='../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False', criterion = criterion, classes = [0, 2], aug=False)
print('Model : Magnification = x20, Size = 128, ROI = 2, Depth = 4, Tolerance = 128')
best_epoch_val = np.argmin(list_loss_test)
for epoch in range(epochs):
    if epoch != best_epoch_val :
        os.remove('../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False' + '/models_detect' + '/unet_epoch=50_batch=100_loss_{}_dropconv_0.0_dropupconv_0.0_aug_False.pt'.format(criterion))
    else :
        path_save = '../data/datasets/DataSet_Slide_A1702114_Magni_x20_ROI_2_Size_128_Tolerance_128_Aug_1_Bal_False' + '/models_detect' + '/unet_epoch=50_batch=100_loss_{}_dropconv_0.0_dropupconv_0.0_aug_False.pt'.format(criterion)
        torch.save(torch.load(path_save).cpu(), path_save)