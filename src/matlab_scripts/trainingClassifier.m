function trainingClassifier(path_images, name)

% Clear all previous data
clc, clear all, close all;


%% Loads Train images and performs training

addpath(genpath('../matlab_scripts'))
chdir path_save %to be changed to the folder where data is stored

a = 1

% retrieves the folder where data is stored
listdir_train = dir('*.mat'); %%dir to train
nb_train = length(listdir_train);

% makes 2 cell arrays containing stuff for training
TrainingImages = cell(nb_train,1);
TrainingLabels = cell(nb_train,1);
for i=1:nb_train
    %checks if file is effectively a training sample
    file = listdir_train(i).name;
    load(file);
    %increments sample and labels cell arrays
    TrainingImages{i,1} = sample.image(:,:,1:3);
    TrainingLabels{i,1} = sample.label;
end



%performs training
[ TrainingStruct ] = MakeClassifier( TrainingImages, TrainingLabels);

save(name, "TrainingStruct");