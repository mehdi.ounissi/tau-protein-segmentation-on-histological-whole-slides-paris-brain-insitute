addpath(genpath('../matlab_scripts'))

slide_s = 'A1702114';
magnification_s = 'x20';
index_s = '1';

slide_t = 'A1702121';
magnification_t = 'x20';
index_t = '5';
%["../example_images/example_",slide_s,"_",magnification_s,"_",index_s,".mat"]
image_s = load(['../example_images/example_',slide_s,'_',magnification_s,'_',index_s,'.mat']);
image_s = image_s.sample.image;
%image_s = sample; %importing image to normalize

image_t = load(['../example_images/example_',slide_t,'_',magnification_t,'_',index_t,'.mat']);
image_t = image_t.sample.image;
%image_t = sample; %importing image on which normalization has to be performed

TrainingStruct = load('../example_images/trained_classifier.mat'); %importing the trained classifier
TrainingStruct.TrainingStruct
[ Norm ] = NormSCD(image_s(:,:,1:3), image_t(:,:,1:3), TrainingStruct.TrainingStruct, 0); %normalises the image
save(['../example_images/example_normed_',slide_s,'_',magnification_s,'_',index_s,'.mat'], 'Norm'); %saves it