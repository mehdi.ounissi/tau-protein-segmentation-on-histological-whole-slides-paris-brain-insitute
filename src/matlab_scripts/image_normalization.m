function [Norm] = image_normalization(source, target, classifier)

TrainingStruct = load(classifier.classifier);

Norm = NormSCD(source.source(:,:,1:3), target.target(:,:,1:3), TrainingStruct.TrainingStruct, 0);

end