"""This file is dedicated to generate a dataset with parameters of user's choice, then store it in hard disk"""

from library.DataSetGenerator import DataSetGenerator

#initialisaing the dataset - ROI = 2 for tangles and 3 for plaques - for magnification x20 for tangles and x10 for plaques
data_set = DataSetGenerator(path_slides="../data/slides/",path_labels="../data/labels/",path_infos="../data/info_slides/",slides = ['A1702114'], magnification=['x10'], size = 128, stride = 64, ROI=3, tolerance = 128, random_pos = True, balance = 0.5, random = False)

#setting the path where to save the dataset  - ROI = 2 for tangles and 3 for plaques
data_set.set_path("../data/datasets/DataSet_Slide_A1702114_Magni_x10_ROI_3_Size_128_Tolerance_128_Aug_1_Bal_0.5")

#assessing that coordinates have to be contained in the dataset
data_set._show_coord = True

#saving the dataset
data_set.save()
