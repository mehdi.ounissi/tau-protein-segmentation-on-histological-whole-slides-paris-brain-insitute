This project is about detecting and segmenting Tau proteins appearing in the brain with Alzheimer's disease. The Tau proteins can be found in 3 different morphologies : neurites which are present sparsely in the whole brain, tangles which are very dense agglomerations of proteins and plaques which are diffuse stacks of Tau proteins.

The goal of the project is to perform segmentation of both tangles and plaques structures (neurites being detected by simple colorimetry), in order to study their morphology and correlate it to the advancement of the Amzheimer's disease.

This task is to be performed on histological whole slides, which are about one gigapixel.

For this purpose, the architecture of the code proposed is built as follows :

    1 - A module to handle easily the histological whole slides and their labels (e. g. delimitation of tangles and plaques).
    
    2 - A module to generate properly datasets to be used for machine learning purposes.
    
    3 - A module to actually perform the deep learning tasks.
    
    
## 1 - Module to easily handle the histological whole slides

The slides are huge images of with different levels of magnification - here the magnification goes from x0.3125 to x40. Hence, the code is built to make it easy to jump into different levels as such.

For the highest resolution level, the resulting images are too huge to be processed as single piece, and hence a way to access to a given region is provided.

Important thing to notice : since there are different magnification levels, all x and y positions on images are given in meter - i.e. corresponding to the real measurements of the studied slice.

The UML diagram is as follows :

![alt text](https://gitlab.com/icm-institute/aramislab/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/-/blob/development/Images/Capture_d_e%CC%81cran_2020-07-29_a%CC%80_19.24.19.png)

This code architecture is stored in the src/SlidesHandling.py file.

The class DataSetSlides is an overall tools to handle the dataset of slides at the file level. It provides an acces to the class Slide which is used to manage a slide file, and especially to acces to the different resolution levels of the slide, whose are handled by the class LevelSlide.

From the class LevelSlide allows to access to small rectangle regions of the slide by specifying its boundaries in real world (given in micrometers). This is handled by the class Region, which dispose of tools to retrieve labels matching whith them, and create masks, which are very useful for deep learning purposes.

An example of use is as follows :
`datasetslide = DataSetSlides('your/path/to/folder/containing/slides', path_labels = 'your/path/to/folder/containing/slides', path_infos = 'your/path/to/folder/containing/infos')`

`region = datasetslide['A1702114']['x10'](-3000,0,1000,3000)`

`print(region)`

This code chooses the rectangular region of which the left inferior corner is at x = -3000 and y = 0, left inferior corner is at x = 1000 and y = 3000, and which belongs to the slide 'A1702114' of the dataset provided, at the magnification level x10.

## 2 - Generating the data

### 2 - 1) Samplers

The LevelSlide class is equipped with 2 objects called samplers, which makes it easy to sample a wide range of regions of slide for a certain magnification level. 

First object is associated with the class DeterministSampler which sample regions witj a constant spatial pace, with parameters such as size, stride, necessity of containing certain ROI and other ones (it is detailed in code commentaries).

On the other hand, the class RandomSamplers allows to randomly sample regions, with certain constraints as having an ROI in them - or not, being in the gray matter, and certain parameters such their size, their number and other ones (it is detailed in code commentaries).

### 2 - 2) Dataset generator

The file src/DataSetGenerator.py file contains a class DataSetGenerator inheriting from the class DataSet from Pytorch which makes it possible use the RegionSampler in order to make a dataset available for deep learning methods. It uses the LevelSlide samplers to proceed, and takes as much parameters as both of the samplers have (it is detailed in code commentaries).

Other classes are also available : DataSplitter, which allows to divide a dataset in several subdatasets (e.g. train, validation and test), Fusion which makes concatenation datasets, MultiDataSplitter which concatenate and splits as many datasets as wanted. Normalization, which performs color normalization through all the dataset based on the target image passed as argument, using Khan & al. method. DataAugmentation, which makes it easy to apply several data augmentation tools such as rotation or flipping. Other functions are available which are detailed in code commentaries.

Code architechture is as follows :
![alt text](https://gitlab.com/icm-institute/aramislab/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/-/blob/development/Images/Capture_d_e%CC%81cran_2020-08-03_a%CC%80_18.24.08.png)

## 3 - Deep Learning

The deep learnings models, losses and metrics are defined respectively in the file src/Models.py. The code architecture is the following :

![alt text](https://gitlab.com/icm-institute/aramislab/tau-protein-segmentation-on-histological-whole-slides-paris-brain-insitute/-/blob/development/Images/Capture_d_e%CC%81cran_2020-08-03_a%CC%80_18.24.08.png)

This file makes it possible to quickly design convolutional network for classification or segmentation purposes, and to rapidly load the cost

They can be used via the functions in the file src/Training.py :

    - train_classifier makes it possible to train a model towards a speficied loss and other parameters

    - train_segmenter
    
    - test is used to score a trained model with the several metrics

## 4 - Usecase example :

    1 - generate a dataset with the file src/make_dataset.py : choose the the slide, the magnification level of the slide, size of the images, the sampling stride. You may also have to chose a directory for storing the data generated this way. To modify these parameters, open the files and follow the guidelines. Then, execute the file.
    
    2 - train a model on the dataset via the file src/learning.py. Open this file and modifiy the parameters concerning data augmentation, structure of the network, batch size, epoch... You may have to choose a location for the trained models to be stored. Then, execute the file.
    
    3 - open the file src/main.ipynb to use the datasets and models generated by the previous operations to conduct your study.
    
## 5 - Results

An exhaustive table of results of the experiments are provided at : https://docs.google.com/spreadsheets/d/1Xj-BybdoAjZ05DuiBNCxJ4jmEE0wKKixpWmpxA6DI7k/edit?usp=sharing.

I may add some synthetics results soon enough.

